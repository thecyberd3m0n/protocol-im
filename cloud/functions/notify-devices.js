const functions = require("firebase-functions");
const admin = require("firebase-admin");
const cors = require("cors");
const express = require("express");

const app = express();
app.use(cors({ origin: true }));

admin.initializeApp();
app.post("/", async (req, res) => {
  const { deviceTokens, payload } = req.body.data;
  try {
    // Check if deviceTokens and payload are provided
    if (!deviceTokens || !payload) {
      return res
        .status(400)
        .json({ error: "deviceTokens and payload are required" });
    }

    // Construct the message
    await admin.messaging().sendEachForMulticast({
      tokens: deviceTokens,
      notification: {
        title: payload.title,
        body: payload.body,
      },
      data: payload.data || {},
    });
    return res
      .status(200)
      .json({ data: { message: "Notifications sent successfully" } });
  } catch (error) {
    console.error("Error sending notifications:", error);
    return res.status(500).json({ error: "Internal server error" });
  }
});
exports.notifyDevices = functions.https.onRequest(app);
