import { AfterContentChecked, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, HostListener, Input, OnInit, Renderer2, ViewChild } from '@angular/core';

@Component({
  selector: 'desktop-logo-wrapper',
  templateUrl: './desktop-logo-wrapper.component.html',
  styleUrls: ['./desktop-logo-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DesktopLogoWrapperComponent implements AfterViewInit {
  @ViewChild('logoWrapper', { static: false }) logoWrapper: ElementRef;
  width = null;
  constructor(private changeDetectorRef: ChangeDetectorRef) {}

  ngAfterViewInit(): void {
    this.getWidth();
  }

  getWidth(): void {
    if (this.logoWrapper) {
      this.width = this.logoWrapper.nativeElement.offsetWidth;
      this.changeDetectorRef.detectChanges();
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.getWidth();
  }
  @HostListener('document:keydown.escape', ['$event'])
  onKeydownHandler(event: KeyboardEvent) {
    event.preventDefault();
    event.stopPropagation();
  }
}
