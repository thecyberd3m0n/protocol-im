import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectDeviceLoadStepComponent } from './connect-device-load-step.component';

describe('ConnectDeviceLoadStepComponent', () => {
  let component: ConnectDeviceLoadStepComponent;
  let fixture: ComponentFixture<ConnectDeviceLoadStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectDeviceLoadStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectDeviceLoadStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
