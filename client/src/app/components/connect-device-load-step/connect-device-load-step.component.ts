import {
  ChangeDetectionStrategy,
  Component,
  HostListener,
  Input,
  OnInit,
  Provider,
  Type,
  ViewEncapsulation,
} from "@angular/core";
import { StepperComponent } from "../../modules/ux/components/stepper/stepper.component";
import { CreatorStepLifecycle } from "../../modules/ux/interfaces/creator-step-lifecycle.interface";
import { BehaviorSubject, Observable } from "rxjs";
import { SecureSyncSelectors } from "../../modules/secure-chat/services/secure-sync.selectors";
import { SecureSyncDispatchers } from "../../modules/secure-chat/services/secure-sync.dispatchers";

@Component({
  selector: "app-connect-device-load-step",
  templateUrl: "./connect-device-load-step.component.html",
  styleUrls: ["./connect-device-load-step.component.scss"],
})
export class ConnectDeviceLoadStepComponent
  implements OnInit, CreatorStepLifecycle
{
  passphrase: string;
  nextStepSubject = new BehaviorSubject<boolean>(true);
  prevStepSubject = new BehaviorSubject<boolean>(true);
  canGoNext = this.nextStepSubject.asObservable();
  canGoBack = this.prevStepSubject.asObservable();
  constructor(
    private secureSyncDispatchers: SecureSyncDispatchers,
    private secureSyncSelectors: SecureSyncSelectors
  ) {}

  @Input() parentStepper: StepperComponent;

  ngOnInit(): void {}

  @HostListener("document:keydown.enter", ["$event"])
  onEnterPressed(event: KeyboardEvent): void {
    event.preventDefault();
    if (this.parentStepper) {
      setTimeout(() => {
        this.parentStepper.goForward();
      });
    }
  }

  onNext(): Promise<void> {
    return new Promise<void>((resolve) => {
      if (this.nextStepSubject.getValue()) {
        this.nextStepSubject.next(false);
        this.secureSyncDispatchers.unlockImport(this.passphrase);
      }

      resolve();
    });
  }

  getStepData() {
    return true;
  }
}
