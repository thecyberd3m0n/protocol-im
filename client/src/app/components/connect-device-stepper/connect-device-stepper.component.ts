import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { filter, first, map, switchMap } from "rxjs/operators";
import { SecureChatMode, SecureChatSelectors } from "../../modules/secure-chat";
import { SecureSyncDispatchers } from "../../modules/secure-chat/services/secure-sync.dispatchers";
import { SecureSyncSelectors } from "../../modules/secure-chat/services/secure-sync.selectors";
import { SyncState } from "../../modules/secure-chat/store/reducers/secure-sync.reducer";
import { SnackbarComponent, SnackbarState } from "../../modules/ux/components/snackbar/snackbar.component";
import { StepperComponent } from "../../modules/ux/components/stepper/stepper.component";
import { ConnectDeviceFirstStepComponent } from "../connect-device-first-step/connect-device-first-step.component";
import { ConnectDeviceLoadStepComponent } from "../connect-device-load-step/connect-device-load-step.component";

@Component({
  selector: "connect-device-stepper",
  templateUrl: "./connect-device-stepper.component.html"
})
export class ConnectDeviceStepperComponent implements OnInit, OnDestroy {
  ConnectDeviceFirstStepComponent = ConnectDeviceFirstStepComponent;
  ConnectDeviceLoadStepComponent = ConnectDeviceLoadStepComponent;

  @ViewChild(StepperComponent)
  parentStepper: StepperComponent;

  @ViewChild(ConnectDeviceFirstStepComponent)
  firstComponent: ConnectDeviceFirstStepComponent;
  @ViewChild(ConnectDeviceLoadStepComponent)
  secondComponent: ConnectDeviceFirstStepComponent;

  subscriptions: Subscription[] = [];
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private secureSyncDispatchers: SecureSyncDispatchers,
    private secureSyncSelectors: SecureSyncSelectors,
    private secureChatSelectors: SecureChatSelectors,
    private matSnackbar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.secureSyncSelectors.global$.subscribe((syncState) => {
      if (syncState.lastLoadReport && syncState.lastLoadReport.success) {
        this.showDeviceEnrolledSnackbar();
        this.router.navigate(["/", "conversations"]);
      }
    });
    this.activatedRoute.params
      .pipe(
        first(),
        filter((x) => !!x),
      )
      .pipe(
        switchMap(({ syncId }) => {
          return this.secureChatSelectors.mode$.pipe(map((mode) => ({
            syncId,
            mode
          })))
        }),
        filter(({ mode }) => mode === SecureChatMode.GUEST)
      )
      .subscribe(({ syncId }) => {
        this.secureSyncDispatchers.connectDevice(syncId);
        this.setUnlockEvent();
      });
  }

  setUnlockEvent() {
    // select sync state, react when SyncState.AWAIT_PASSPHRASE
    this.subscriptions.push(
      this.secureSyncSelectors.state$.subscribe((state) => {
        if (state === SyncState.AWAIT_PASSPHRASE) {
          this.parentStepper.goForward();
        }
      })
    )
  }

  showDeviceEnrolledSnackbar() {
    const snackbarComponent = this.matSnackbar.openFromComponent(
      SnackbarComponent,
      {
        duration: 3000,
        horizontalPosition: "end",
        verticalPosition: "bottom",
      }
    );
    snackbarComponent.instance.message = `Device enrolled successfully and will be synced with Account`;
    snackbarComponent.instance.snackbarState = SnackbarState.Success;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
