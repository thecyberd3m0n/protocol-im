import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectDeviceStepperComponent } from './connect-device-stepper.component';

describe('ConnectDeviceStepperComponent', () => {
  let component: ConnectDeviceStepperComponent;
  let fixture: ComponentFixture<ConnectDeviceStepperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectDeviceStepperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectDeviceStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
