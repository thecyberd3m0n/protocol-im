import { Component, EventEmitter, Output, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { SecureChatSelectors } from "../../modules/secure-chat";
import { StepperComponent } from "../../modules/ux/components/stepper/stepper.component";
import { CreateProfileFirstStepComponent } from "../create-profile-first-step/create-profile-first-step.component";
import { CreateProfileSecondStepComponent } from "../create-profile-second-step/create-profile-second-step.component";
import { CreateProfileThirdStepComponent } from "../create-profile-third-step/create-profile-third-step.component";

@Component({
  selector: "store-session-stepper",
  templateUrl: "./store-session-stepper.component.html",
  styleUrls: ["./store-session-stepper.component.scss"],
})
export class StoreSessionStepperComponent {
  @ViewChild(StepperComponent)
  parentStepper: StepperComponent;
  @ViewChild("firstComponent", {
    read: CreateProfileFirstStepComponent,
    static: false,
  })
  firstComponent: CreateProfileFirstStepComponent;
  @ViewChild("secondComponent", {
    read: CreateProfileSecondStepComponent,
    static: false,
  })
  secondComponent: CreateProfileSecondStepComponent;
  @Output()
  finished = new EventEmitter<void>();
  CreateProfileFirstStepComponent = CreateProfileFirstStepComponent;
  CreateProfileSecondStepComponent = CreateProfileSecondStepComponent;
  CreateProfileThirdStepComponent = CreateProfileThirdStepComponent;
  data = null;
  constructor(
    public secureChatSelectors: SecureChatSelectors,
    private router: Router
  ) {}

  onFinished(_$event) {
    if (!this.router.url.includes("conversations")) {
      this.finished.emit();
      this.router.navigate(["conversations"]);
    }
  }

  onNextStep({ step, data }: { step: number; data: any }) {
    this.data = data;
  }
}
