import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreSessionStepperComponent } from './store-session-stepper.component';

describe('StoreSessionStepperComponent', () => {
  let component: StoreSessionStepperComponent;
  let fixture: ComponentFixture<StoreSessionStepperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreSessionStepperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreSessionStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
