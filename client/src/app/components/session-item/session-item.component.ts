import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { session } from '@wireapp/proteus';
@Component({
  selector: 'session-item',
  templateUrl: './session-item.component.html',
  styleUrls: ['./session-item.component.scss']
})
export class SessionItemComponent {
  @Input()
  name: string;
@Input()
active: boolean;
  sessionId: string;
  @Input()
  avatarLabel: string;

  constructor(
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {

  }
}
