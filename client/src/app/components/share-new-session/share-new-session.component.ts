import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SecureChatDispatchers, SecureChatSelectors } from '../../modules/secure-chat';
import { SerializedInviteSession } from '../../modules/secure-chat/models/inviteSession.model';
import { SnackbarComponent, SnackbarState } from '../../modules/ux/components/snackbar/snackbar.component';

@Component({
  selector: 'share-new-session',
  templateUrl: './share-new-session.component.html',
  styleUrls: ['./share-new-session.component.scss']
})
export class ShareNewSessionComponent implements OnInit {
  PIN?: number = null;
  sessionLink = '';
  subscriptions: Subscription[] = [];
  constructor(
    public secureChatSelectors: SecureChatSelectors,
    private secureChatDispatchers: SecureChatDispatchers,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private matSnackbar: MatSnackBar
    ) {
  }
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.secureChatSelectors.pendingSessions$.subscribe((inviteSessions: SerializedInviteSession[]) => {
          // detecting app installation path
          let paths = window.location.href.split('/');
          this.activatedRoute.snapshot.url.forEach((part) => {
            // remove parts from current route, and that remove 'conversations'
            const idx = paths.findIndex((x) => x === part.path);
            paths.splice(idx, 1);

          });
          const idx = paths.findIndex((x) => x === 'conversations');
          paths.splice(idx, 1);
          const originArr = window.location.origin.split('/');
          // check if they're similar
          let diff = [];
          if (paths.length !== originArr.length) {
            diff = paths.filter((path) => {
              return !(originArr.find(x => x === path)) && path !== '';
            });

          }
          const inviteSession = inviteSessions.find(session => session.id === params.id); 
          if (inviteSession) {
            this.sessionLink = `${window.location.origin}/${diff.join('/')}/conversations/session/join/${params.id}`.replace(new RegExp('//', 'g'), '/');
            if (inviteSession && inviteSession.activeSessionId) {
              // this.showPeerJoinedSnackbar(inviteSession.activeSessionId);
              this.router.navigate(['conversations', 'session', 'active', inviteSession.activeSessionId]);
            }
          }

        });
      } else {
        this.secureChatDispatchers.startSession({ manualApprove: false, groupMode: false, oneTime: true });
      }
    });
  }

  showPeerJoinedSnackbar(sessionId: string): void {
    const snackbarComponent = this.matSnackbar.openFromComponent(
      SnackbarComponent,
      {
        duration: 3000,
        horizontalPosition: "end",
        verticalPosition: "bottom",
      }
    );
    snackbarComponent.instance.message = `Peer connected to session ${sessionId}`;
    snackbarComponent.instance.snackbarState = SnackbarState.Success;
  }

}

