import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from "@angular/core";
import { MatAccordion } from "@angular/material/expansion";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription, combineLatest } from "rxjs";
import {
  distinctUntilChanged,
  filter,
  first,
  map,
  mergeMap,
  switchMap,
  tap,
  withLatestFrom,
} from "rxjs/operators";
import {
  SecureChatDispatchers,
  SecureChatMode,
  SecureChatSelectors,
} from "src/app/modules/secure-chat";
import { SerializedInviteSession } from "../../modules/secure-chat/models/inviteSession.model";
import { MetadataMap } from "../../modules/secure-chat/models/metadataMap.model";
import { SecureSyncSelectors } from "../../modules/secure-chat/services/secure-sync.selectors";
import { ProfileMessageService } from "../../services/profile-message.service";
import { SidenavService } from "../../modules/ux/layouts/sidenav-layout/sidenav-layout.service";

@Component({
  templateUrl: "./sessions-list.component.html",
  styleUrls: ["./sessions-list.component.scss"],
  selector: "session-list",
})
export class SessionsListComponent implements OnInit, OnDestroy {
  @ViewChild("accordion", { read: MatAccordion, static: true })
  accordion: MatAccordion;

  @Input()
  currentSessionId: number;
  isMobile$ = this.breakpointObserver
    .observe([Breakpoints.Small, Breakpoints.Handset])
    .pipe(distinctUntilChanged());
  sessions: { id: string; name?: string | MetadataMap }[];
  inviteSessions: SerializedInviteSession;
  syncId: string;
  subscriptions: Subscription[] = [];
  personalExpanded = true;
  buddiesExpanded = true;
  syncSession$ = this.secureSyncSelectors.syncActiveSessionId$.pipe(
    filter((x) => !!x)
  );
  
  @Output()
  close = new EventEmitter();
  profile$ = this.profileService.profile$;
  syncSession: {
    id: string;
    profile: MetadataMap;
  } | null = null;
  constructor(
    private breakpointObserver: BreakpointObserver,
    public secureChatSelectors: SecureChatSelectors,
    private secureSyncSelectors: SecureSyncSelectors,
    private secureChatDispatchers: SecureChatDispatchers,
    public profileService: ProfileMessageService,
    private router: Router,
    public activeRoute: ActivatedRoute,
    private sidenavService: SidenavService
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.secureChatSelectors.mode$
        .pipe(
          filter((mode) => mode === SecureChatMode.READY),
          switchMap(() => this.secureChatSelectors.activeSessions$),
          filter((x) => !!x),
          switchMap((activeSessions) =>
            this.secureSyncSelectors.syncActiveSessionId$.pipe(
              map((syncId) => ({ syncId, activeSessions }))
            )
          ),
          map(({ activeSessions, syncId }) => {
            return {
              activeSessions: activeSessions.filter((x) => x.id !== syncId),
              syncId,
            };
          }),
          mergeMap(({ activeSessions, syncId }) => {
            // TODO: is this is getting updated?
            return this.profile$.pipe(
              map((profile) => ({
                profile,
                activeSessions,
                syncId,
              }))
            );
          }),
          mergeMap(({ activeSessions, syncId, profile }) => {
            return this.secureSyncSelectors.approvedDevices$.pipe(
              map((approvedDevices) => ({
                approvedDevices,
                activeSessions,
                syncId,
                profile,
              }))
            );
          }),
          tap(({ activeSessions, syncId, profile, approvedDevices }) => {
            console.log(
              "sessionlist sessions",
              activeSessions,
              syncId,
              profile,
              approvedDevices
            );
            if (syncId && approvedDevices.length > 0) {
              this.syncSession = {
                id: syncId,
                profile,
              };
            }
            this.sessions = activeSessions.map((as) => ({
              id: as.id,
              name:
                as.meta.myprofile?.name ?? as.meta?.myprofile?.name ?? as.id,
            }));
          }),
          mergeMap(({ activeSessions, syncId, profile }) => {
            const profileObservables = activeSessions.map((session) =>
              this.profileService.getProfileForSession$(session.id).pipe(
                filter((profile) => !!profile?.name),
                map((profile) => ({
                  id: session.id,
                  name:
                    profile?.name ??
                    session.meta?.myprofile?.name ??
                    session.id,
                }))
              )
            );
            return combineLatest(profileObservables);
          })
        )
        .subscribe((sessions: any) => {
          this.sessions = sessions;
        })
    );

    this.subscriptions.push(
      this.secureChatSelectors.pendingSessions$
        .pipe(
          first(),
          withLatestFrom(
            this.secureSyncSelectors.syncActiveSessionId$.pipe(
              filter((x) => !!x)
            )
          ),
          map(([pendingSessions, syncId]) =>
            pendingSessions.filter((x) => x.id !== syncId)
          ),
          map((x) => x[x.length - 1])
        )
        .subscribe((pendingSession) => {
          if (pendingSession) {
            this.inviteSessions = pendingSession;
          }
        })
    );
    this.accordion.openAll();
  }

  newSession(): void {
    // get "free" session
    // find all active and pending, reduce pending by active
    combineLatest([
      this.secureChatSelectors.pendingSessions$,
      this.secureChatSelectors.activeSessions$,
      this.secureSyncSelectors.syncInviteSessionId$,
    ]).subscribe(([pendingSessions, activeSessions, syncPendingId]) => {
      // reduce pending by active
      let newSession = null;
      // find pendingSession that has no activeId

      newSession = pendingSessions
        .filter((x) => x.id !== syncPendingId)
        .filter((x) => !x.activeSessionId);
      if (!newSession || newSession.length === 0) {
        this.secureChatDispatchers.startSession({
          groupMode: false,
          manualApprove: false,
          oneTime: true,
          metadata: {},
        });
      } else if (newSession.length === 1) {
        this.router.navigate([
          "conversations",
          "session",
          "new",
          newSession[0].id,
        ]);
      } else if (newSession.length > 1) {
        this.router.navigate([
          "conversations",
          "session",
          "new",
          newSession[newSession.length - 1].id,
        ]);
      }
      this.close.next();
    });
  }


  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
