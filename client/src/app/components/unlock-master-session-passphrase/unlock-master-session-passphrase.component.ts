import { Component, HostListener, Input, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { BehaviorSubject, Subscription } from "rxjs";
import { CreatorStepLifecycle } from "../../modules/ux/interfaces/creator-step-lifecycle.interface";
import { StepperComponent } from "../../modules/ux/components/stepper/stepper.component";
import {
  SecureChatDispatchers,
  SecureChatSelectors,
} from "../../modules/secure-chat";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'unlock-session-passphrase',
  styleUrls: ["./unlock-master-session-passphrase.component.scss"],
  templateUrl: "./unlock-master-session-passphrase.component.html",
})
export class UnlockMasterSessionPassphraseComponent
  implements OnInit, CreatorStepLifecycle, OnDestroy
{
  join: string;
  nextStepSubject = new BehaviorSubject<boolean>(true);
  prevStepSubject = new BehaviorSubject<boolean>(false);
  canGoNext = this.nextStepSubject.asObservable();
  canGoBack = this.prevStepSubject.asObservable();
  passphrase: string;
  @Input() parentStepper: StepperComponent;
  unlockError$ = this.secureChatSelectors.error$;
  disabled = false;
  formGroup: FormGroup = this.builder.group({
    passphrase: ["", Validators.required],
  });
  
  subscriptions: Subscription[] = [];
  error: string;
  constructor(
    private route: ActivatedRoute,
    private secureChatDispatchers: SecureChatDispatchers,
    private secureChatSelectors: SecureChatSelectors,
    private builder: FormBuilder
  ) {}
  ngOnInit() {
    this.subscriptions.push(
      this.route.queryParams.subscribe((params) => {
        this.join = params.join;
      })
    );
    this.subscriptions.push(
      this.unlockError$.subscribe((err) => {
        if (err) {
          console.error('[UnlockMasterSessionError] Unlock error', err);
          this.error = "Password is not correct";
          this.formGroup.get('passphrase').setErrors({ 'badpassphrase': true });
          if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
          }
          this.disabled = true;
          setTimeout(() => {
            this.disabled = false;
            this.formGroup.get('passphrase').setValue('');
            this.formGroup.get('passphrase').setErrors({ 'badpassphrase': false });
            if (document.activeElement instanceof HTMLElement) {
              document.activeElement.blur();
            }
          }, 2000);
        }
      })
    );
  }

  getStepData() {
    return this.formGroup.get('passphrase').value;
  }

  onNext() {
    return new Promise((resolve, reject) => {
      if (this.formGroup.get('passphrase').valid) {
        resolve(this.formGroup.get('passphrase').value);
      } else {
        reject('Invalid form');
      }
    })
  }

  @HostListener("document:keydown.enter", ["$event"])
  onEnterPressed(event: KeyboardEvent): void {
    event.preventDefault();
    if (this.parentStepper) {
      setTimeout(() => {
        this.parentStepper.goForward();
      });
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((s) => s.unsubscribe());
  }
}
