import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectDeviceDeviceApproveStepComponent } from './connect-device-device-approve-step.component';

describe('ConnectDeviceDeviceApproveStepComponent', () => {
  let component: ConnectDeviceDeviceApproveStepComponent;
  let fixture: ComponentFixture<ConnectDeviceDeviceApproveStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectDeviceDeviceApproveStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectDeviceDeviceApproveStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
