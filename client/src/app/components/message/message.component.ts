import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { MatBottomSheet } from "@angular/material/bottom-sheet";
import { Observable, Subject, Subscription } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { MessageContextMenuComponent } from "src/app/modules/ux/components/message-context-menu/message-context-menu/message-context-menu.component";
import {
  ReactionMessage,
  ReactionMessageService,
} from "src/app/services/reaction-message.service";
import { DecryptedOlmEnvelope } from "../../modules/secure-chat/models/decryptedOlmEnvelope.model";

@Component({
  selector: "message",
  templateUrl: "./message.component.html",
  styleUrls: ["./message.component.scss"],
})
export class MessageComponent implements OnInit, OnDestroy {
  @Input()
  id: string;
  @Input()
  content: DecryptedOlmEnvelope;
  @Input()
  date: Date;
  @Input()
  isRead?: boolean;
  subscriptions: Subscription[] = [];
  isSet: boolean = false;
  reactions$: Observable<ReactionMessage[]>;
  JSON = JSON;
  private destroy$: Subject<boolean> = new Subject<boolean>();
  constructor(
    public reactionMessageService: ReactionMessageService,
    private meessageContextMenu: MatBottomSheet
  ) {}

  ngOnInit(): void {
    this.reactions$ = this.reactionMessageService.reactions$(this.id);
  }

  messageLongPress($event) {
    $event.preventDefault();
    this.isSet = true;
    const messageContextMenuRef = this.meessageContextMenu.open(
      MessageContextMenuComponent,
      { data: { messageId: this.id } }
    );
    messageContextMenuRef
      .afterDismissed()
      .pipe(takeUntil(this.destroy$))
      .subscribe((reaction) => {
        if (reaction)
          this.reactionMessageService.sendReactionMessage(
            this.content.sessionId,
            reaction
          );
        this.isSet = false;
      });
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
