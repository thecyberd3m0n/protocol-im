import {
  Component,
  Input,
  OnInit,
  ElementRef,
  ViewChild,
  OnDestroy,
} from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidatorFn,
  Validators,
} from "@angular/forms";
import { BehaviorSubject, Subscription } from "rxjs";
import {
  SecureChatDispatchers,
  SecureChatMode,
  SecureChatSelectors,
} from "src/app/modules/secure-chat";
import { UxStepperService } from "../../modules/ux/components/stepper/stepper.service";
import { CreatorStepLifecycle } from "../../modules/ux/interfaces/creator-step-lifecycle.interface";
import { ProfileMessageService } from "../../services/profile-message.service";
import { StepperComponent } from "../../modules/ux/components/stepper/stepper.component";
import { SecureSyncDispatchers } from "../../modules/secure-chat/services/secure-sync.dispatchers";
import { MatCheckbox } from "@angular/material/checkbox";
import { filter, first, switchMap } from "rxjs/operators";
import { SecureSyncSelectors } from "../../modules/secure-chat/services/secure-sync.selectors";
import { ConnectDeviceService } from "../../services/connect-device.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import {
  SnackbarComponent,
  SnackbarState,
} from "../../modules/ux/components/snackbar/snackbar.component";

@Component({
  selector: "start-new-session-second-step",
  templateUrl: "./create-profile-second-step.component.html",
  styleUrls: ["./create-profile-second-step.component.scss"],
})
export class CreateProfileSecondStepComponent
  implements OnInit, CreatorStepLifecycle, OnDestroy
{
  nextStepSubject = new BehaviorSubject<boolean>(true);
  prevStepSubject = new BehaviorSubject<boolean>(true);
  canGoNext = this.nextStepSubject.asObservable();
  canGoBack = this.prevStepSubject.asObservable();
  error: string = null;
  formSub: Subscription;
  @Input()
  username: string;
  formGroup: FormGroup;
  @ViewChild("password", { static: false }) password: ElementRef;
  @ViewChild("repeatPassword", { static: false }) repeatPassword: ElementRef;
  @ViewChild("agreeToTerms", { static: false }) agreeToTerms: MatCheckbox;
  @Input() parentStepper: StepperComponent;
  subscriptions: Subscription[] = [];
  constructor(
    public secureChatSelectors: SecureChatSelectors,
    private secureChatDispatchers: SecureChatDispatchers,
    private secureSyncDispatchers: SecureSyncDispatchers,
    private secureSyncSelectors: SecureSyncSelectors,
    private builder: FormBuilder,
    private profileService: ProfileMessageService,
    private uxStepperService: UxStepperService,
    private connectDeviceService: ConnectDeviceService,
    private matSnackbar: MatSnackBar
  ) {
    this.formGroup = this.builder.group({
      password: ["", [Validators.required, this.passwordStrengthValidator()]],
      repeatPassword: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          this.repeatPasswordValidator(),
        ],
      ],
      agreeToTerms: [false, Validators.requiredTrue],
    });
  }

  ngOnInit() {
    // check if there is already password in state

    this.subscriptions.push(
      this.formGroup.valueChanges.subscribe((value) => {
        this.nextStepSubject.next(this.formGroup.valid);
      })
    );
  }

  passwordStrengthValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const value = control.value;
      if (value.length === 0) {
        return Promise.resolve(null);
      }
      const hasNumber = /\d/.test(value);
      const hasSymbol = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(value);
      const hasUpperCase = /[A-Z]/.test(value);
      const hasLowerCase = /[a-z]/.test(value);
      const hasNonAlphanumeric = /[^A-Za-z0-9]/.test(value);
      const isNumeric = /^[0-9]+$/.test(value);
      const isAlphabetic = /^[A-Za-z]+$/.test(value);
      const isValid =
        !isNumeric &&
        !isAlphabetic &&
        hasNumber &&
        hasSymbol &&
        hasNonAlphanumeric;

      if (!isValid) {
        return { passwordStrength: true };
      }
      return null;
    };
  }

  repeatPasswordValidator(): ValidatorFn {
    let passwordControl: AbstractControl;

    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!passwordControl) {
        if (this.formGroup) {
          passwordControl = this.formGroup.get("password");
        } else {
          return null;
        }
      }

      const password = passwordControl.value;
      const confirmPassword = control.value;
      const isValid = password === confirmPassword;

      if (!isValid) {
        return { passwordMismatch: true };
      }
      return null;
    };
  }

  getStepData() {
    return this.formGroup.value.password;
  }

  onKeydown(event: KeyboardEvent, nextElementName: string): void {
    if (event.key === "Enter" || event.keyCode === 13) {
      event.preventDefault(); // Prevent default behavior, like form submission
      this.nextStepSubject.next(this.formGroup.valid);
    }
  }

  onNext() {
    return new Promise<void>((resolve) => {
      this.secureChatDispatchers.setupProfile({
        passphrase: this.formGroup.value.password,
        name: this.uxStepperService.dataSteps[0].username,
      });

      this.subscriptions.push(this.secureChatSelectors.mode$
        .pipe(
          filter((mode) => mode === SecureChatMode.READY),
          switchMap(() => {
            this.secureSyncDispatchers.createSyncSession();
            return this.secureSyncSelectors.syncInviteSessionId$.pipe(
              filter((syncId) => !!syncId)
            );
          }),
          first()
        )
        .subscribe(() => {
          this.connectDeviceService.init(this.formGroup.value.password);

          // TODO: check if break sync scheme?
          this.profileService.changeProfile({
            username: this.uxStepperService.dataSteps[0].username,
            bio: this.uxStepperService.dataSteps[0].bio,
          });
          this.showAccountCreatedSnackbar();
          resolve(this.formGroup.value.password);
        }));
    });
  }

  showAccountCreatedSnackbar(): void {
    const snackbarComponent = this.matSnackbar.openFromComponent(
      SnackbarComponent,
      {
        duration: 3000,
        horizontalPosition: "end",
        verticalPosition: "bottom",
      }
    );
    snackbarComponent.instance.message = "Your account has been created";
    snackbarComponent.instance.snackbarState = SnackbarState.Success;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
