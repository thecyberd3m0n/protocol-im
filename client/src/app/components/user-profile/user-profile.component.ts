import { Component, OnDestroy, OnInit } from "@angular/core";
import {
  Profile,
  ProfileMessageService,
} from "../../services/profile-message.service";
import { SidenavService } from "../../modules/ux/layouts/sidenav-layout/sidenav-layout.service";
import { EditUserProfileComponent } from "../edit-user-profile/edit-user-profile.component";
import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { distinctUntilChanged } from "rxjs/operators";

@Component({
  selector: "user-profile",
  templateUrl: "./user-profile.component.html",
  styleUrls: ["./user-profile.component.scss"],
})
export class UserProfileComponent implements OnInit {
  name = "";
  bio = "";
  isMobile$ = this.breakpointObserver
  .observe([Breakpoints.Small, Breakpoints.Handset])
  .pipe(
    distinctUntilChanged()
  );
  constructor(
    private profileService: ProfileMessageService,
    private sidenavService: SidenavService,
    private breakpointObserver: BreakpointObserver
  ) {}
  
  ngOnInit(): void {
    this.profileService.profile$.subscribe((profile: Profile) => {
      console.log('profile', profile);
      this.name = profile.username;
      this.bio = profile.bio;
    });
  }
  cancel(): void {
    this.sidenavService.unloadSidenav();
  }
  editSidenav(): void {
    this.sidenavService.loadAndOpenChildView(EditUserProfileComponent, {});
  }
}
