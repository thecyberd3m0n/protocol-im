import {
  Component,
  HostListener,
  Input,
  OnDestroy,
  OnInit
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BehaviorSubject, Subscription } from "rxjs";
import { SecureChatSelectors } from "../../modules/secure-chat";
import { StepperComponent } from "../../modules/ux/components/stepper/stepper.component";
import { CreatorStepLifecycle } from "../../modules/ux/interfaces/creator-step-lifecycle.interface";

@Component({
  selector: "create-profile-first-step",
  templateUrl: "./create-profile-first-step.component.html",
  styleUrls: ["./create-profile-first-step.component.scss"],
})
export class CreateProfileFirstStepComponent
  implements OnInit, CreatorStepLifecycle, OnDestroy
{
  sessionID: string;
  pendingSessionsSubscription: Subscription;
  activeSessionsSubscription: Subscription;
  nextStepSubject = new BehaviorSubject<boolean>(false);
  prevStepSubject = new BehaviorSubject<boolean>(true);
  canGoNext = this.nextStepSubject.asObservable();
  canGoBack = this.prevStepSubject.asObservable();
  previousButtonLabelSubject = new BehaviorSubject<string>("Back");
  nextButtonLabelSubject = new BehaviorSubject<string>("Next");
  previousButtonLabel = this.previousButtonLabelSubject.asObservable();
  nextButtonLabel = this.nextButtonLabelSubject.asObservable();
  error: string = null;
  imageDataUrl: string | null = null;
  formGroup: FormGroup = this.formBuiled.group({
    username: ["", [Validators.required, Validators.minLength(3)]],
    bio: ["", [Validators.maxLength(300)]],
  });
  subscriptions: Subscription[] = [];

  constructor(
    public secureChatSelectors: SecureChatSelectors,
    private formBuiled: FormBuilder,
  ) {}
  @Input() parentStepper: StepperComponent;
  ngOnInit() {
    this.subscriptions.push(this.formGroup.valueChanges.subscribe(() => {
      this.nextStepSubject.next(this.formGroup.valid);
    }));
  }

  getStepData() {
    return {
      username: this.formGroup.value.username,
      bio: this.formGroup.value.bio,
    };
  }

  onFileDropped(files: File[]) {
    const file = files[0];
    const reader = new FileReader();
    reader.onload = () => {
      const dataUrl = reader.result as string;
      const isImage = file.type.startsWith("image/");
      this.imageDataUrl = isImage ? dataUrl : null;
    };
    reader.readAsDataURL(file);
  }


  @HostListener("document:keydown.enter", ["$event"])
  onEnterPressed(event: KeyboardEvent): void {
    event.preventDefault();
    if (this.parentStepper) {
      setTimeout(() => {
        this.parentStepper.goForward();
      });
    }
  }

  onNext() {
    return new Promise((resolve) => resolve(this.getStepData()));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
