import { Component, OnInit } from "@angular/core";
import {
  Profile,
  ProfileMessageService,
} from "../../services/profile-message.service";
import { MatDialog } from "@angular/material/dialog";
import { DialogComponent } from "../dialog/dialog.component";
import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { distinctUntilChanged } from "rxjs/operators";
import { SidenavService } from "../../modules/ux/layouts/sidenav-layout/sidenav-layout.service";
import { FormBuilder, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { SnackbarComponent, SnackbarState } from "../../modules/ux/components/snackbar/snackbar.component";

@Component({
  selector: "edit-user-profile",
  templateUrl: "./edit-user-profile.component.html",
  styleUrls: ["./edit-user-profile.component.scss"],
})
export class EditUserProfileComponent implements OnInit {
  isMobile$ = this.breakpointObserver
    .observe([Breakpoints.Small, Breakpoints.Handset])
    .pipe(distinctUntilChanged());
  saved = false;

  formGroup = this.formBuilder.group({
    username: ["", [Validators.required, Validators.maxLength(16)]],
    bio: ["", [Validators.maxLength(1000)]],
  });
  constructor(
    private profileService: ProfileMessageService,
    public dialog: MatDialog,
    private breakpointObserver: BreakpointObserver,
    private sidenavService: SidenavService,
    private formBuilder: FormBuilder,
    private matSnackbar: MatSnackBar
  ) {}
  ngOnInit(): void {
    this.profileService.profile$.subscribe((profile: Profile) => {
      this.formGroup.get('username').setValue(profile.username); 
      this.formGroup.get('bio').setValue(profile.bio);
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: "40%",
      backdropClass: "backdrop",
      height: "20vh",
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
    });
  }

  cancel(): void {
    this.sidenavService.closeChildSidenav();
  }

  saveChanges() {
    this.saved = !this.saved;
    if (this.formGroup.valid) {
      this.profileService.changeProfile({
        username: this.formGroup.get('username').value,
        bio: this.formGroup.get('bio').value
      });
      this.sidenavService.closeChildSidenav();
      this.showProfileSavedSnackbar();
  
    } else {
      // TODO: error snackbar 
    }
    
  }

  showProfileSavedSnackbar() {
    const snackbarComponent = this.matSnackbar.openFromComponent(
      SnackbarComponent,
      {
        duration: 3000,
        horizontalPosition: "end",
        verticalPosition: "bottom",
      }
    );
    snackbarComponent.instance.message = `User profile changed`;
    snackbarComponent.instance.snackbarState = SnackbarState.Success;
  }
}
