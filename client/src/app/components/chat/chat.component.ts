import { CdkTextareaAutosize } from "@angular/cdk/text-field";
import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild,
} from "@angular/core";
import { Subject, Subscription, fromEvent } from "rxjs";
import {
  debounceTime,
  filter,
  map,
  switchMap,
  takeUntil,
  tap,
} from "rxjs/operators";
import {
  SecureChatDispatchers,
  SecureChatMode,
  SecureChatSelectors,
} from "../../modules/secure-chat";
import { DecryptedOlmEnvelope } from "../../modules/secure-chat/models/decryptedOlmEnvelope.model";
import { TextMessageService } from "../../services/text-message.service";
import { WasReadMessageService } from "../../services/was-read-message.service";
import { WritingMessageService } from "../../services/writing-message.service";
import { session } from "@wireapp/proteus";
import { SerializedActiveSession } from "../../modules/secure-chat/models/activeSession.model";
import { NotifyBarAction, NotifyBarColor } from "src/app/modules/ux/components/notify-bar/notify-bar.component";
import { NotificationsDispatchers } from "../../modules/secure-chat/services/notifications.dispatchers";
import { NotificationsSelectors } from "src/app/modules/secure-chat/services/notifications-selectors.service";

@Component({
  selector: "app-chat",
  templateUrl: "./chat.component.html",
  styleUrls: ["./chat.component.scss"],
})
export class ChatComponent
  implements OnInit, OnChanges, AfterViewInit, OnDestroy
{
  @Input() session: SerializedActiveSession;
  ready: boolean;
  message: string = "";
  isFocus: boolean;
  messagesCount: number = 0;
  lasIncomingMessage: DecryptedOlmEnvelope | null = null;
  messagesList: Map<string, DecryptedOlmEnvelope>;
  private destroy$: Subject<boolean> = new Subject<boolean>();
  lastIncomingMessage: DecryptedOlmEnvelope | null = null;
  subscriptions: Subscription[] = [];
  messages: DecryptedOlmEnvelope[];
  notifyBarEnabled = false;
  notifyBarActions = [
    {
      action: () => {
        this.notifyBarEnabled = false;
      },
      color: NotifyBarColor.PRIMARY,
      label: "Disable",
    },
    {
      action: () => {
        this.notificationsDispatcher.enable();
      },
      color: NotifyBarColor.WARN,
      label: "Enable",
    },
  ] as NotifyBarAction[];
  constructor(
    public secureChatSelectors: SecureChatSelectors,
    public secureChatDispatchers: SecureChatDispatchers,
    public textMessageService: TextMessageService,
    public wasReadMessageService: WasReadMessageService,
    public writingMessageService: WritingMessageService,
    private notificationsDispatcher: NotificationsDispatchers,
    private notificationsSelectors: NotificationsSelectors
  ) {}

  @ViewChild("scrollMe") private myScrollContainer: ElementRef;
  @ViewChild("autosize") autosize: CdkTextareaAutosize;
  @ViewChild("writing") writing: ElementRef;

  ngOnInit(): void {
    this.subscriptions.push(
      this.notificationsSelectors.global$.subscribe((notificationState) => {
        if (notificationState.permissions === 'default') {
          setTimeout(() => {
            this.notifyBarEnabled = true;
          }, 2000);
        } else {
          this.notifyBarEnabled = false;
        }

      })
    );
    this.subscriptions.push(
      this.secureChatSelectors.mode$.subscribe((mode) => {
        if (mode === SecureChatMode.DISABLED) {
          this.secureChatDispatchers.init();
          // this.wasDisabled = true;
        } else if (
          mode === SecureChatMode.READY ||
          mode === SecureChatMode.GUEST
        ) {
          // this.subscribeToMessages();
          this.ready = true;
          this.textMessageService.loadMessagesForSession(this.session.id, 50, 0);
        }
      })
    );

    this.subscriptions.push(
      this.textMessageService.messages$.subscribe((messages) => {
        this.messages = messages;
        if (messages.length !== this.messagesCount) {
          this.messagesCount = messages.length;
          this.writingMessageService.writingSub.next(false);
        }
        if (messages.length > 0) {
          setTimeout(() => {
            this.scrollToBottom();
          });
        }
      })
    );
    this.subscriptions.push(
      this.textMessageService.messages$
        .pipe(
          filter((x) => x.length > 0),
          map((x) => x.filter((y) => !y.own)),
          map((x) => x[x.length - 1]),
          takeUntil(this.destroy$)
        )
        .subscribe((x) => {
          this.lastIncomingMessage = x;
          if (this.isFocus) {
            this.wasReadMessageService.sendWasReadMessage(
              this.session.id,
              this.lastIncomingMessage.id
            );
            this.lastIncomingMessage = null;
          }
        })
    );
  }
  ngAfterViewInit() {
    //Detecting when user is typing.
    this.writingEvent().pipe(takeUntil(this.destroy$)).subscribe();
  }
  writingEvent() {
    return fromEvent(this.writing.nativeElement, "input").pipe(
      tap(() => this.writingMessageService.typingSub.next(this.session.id))
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.ready && changes.session) {
      this.textMessageService.loadMessagesForSession(this.session.id, 50, 0);
    }
  }

  @HostListener("window:focus", ["$event"])
  onFocus(): void {
    if (this.lastIncomingMessage) {
      this.isFocus = true;
      this.wasReadMessageService.sendWasReadMessage(
        this.session.id,
        this.lastIncomingMessage.id
      );
      this.lastIncomingMessage = null;
    }
  }

  @HostListener("window:blur", ["$event"])
  onBlur(): void {
    this.isFocus = false;
  }
  @HostListener("document:keydown.enter", ["$event"])
  onKeydownHandler(event: KeyboardEvent) {
    this.sendMessage();
    event.preventDefault();
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop =
        this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) {}
  }

  sendMessage() {
    if (!this.message || this.message.trim().length === 0) {
      return;
    }
    this.textMessageService.sendMessage(
      this.session,
      {
        sessionId: this.session.id,
        content: this.message,
        type: "text",
        noStore: false,
      }
    )
    this.message = "";
  }

  onEmoji($event: any) {
    if ($event) {
      this.message += $event.emoji.native;
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
    this.destroy$.next();
    this.destroy$.complete();
  }
}
