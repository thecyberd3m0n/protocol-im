import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeerProfileComponent } from './peer-profile.component';

describe('PeerProfileComponent', () => {
  let component: PeerProfileComponent;
  let fixture: ComponentFixture<PeerProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeerProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeerProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
