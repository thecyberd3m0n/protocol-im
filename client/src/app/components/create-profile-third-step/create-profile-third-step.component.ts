import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute } from "@angular/router";
import { BehaviorSubject, Subscription } from "rxjs";
import { distinctUntilChanged, first } from "rxjs/operators";
import {
  SecureChatDispatchers,
  SecureChatSelectors,
} from "src/app/modules/secure-chat";
import { environment } from "../../../environments/environment";
import { SecureSyncSelectors } from "../../modules/secure-chat/services/secure-sync.selectors";
import { BackupData } from "../../modules/secure-chat/store/reducers/secure-chat.reducer";
import { StepperComponent } from "../../modules/ux/components/stepper/stepper.component";
import { CreatorStepLifecycle } from "../../modules/ux/interfaces/creator-step-lifecycle.interface";

@Component({
  selector: "create-profile-third-step",
  templateUrl: "./create-profile-third-step.component.html",
  styleUrls: ["./create-profile-third-step.component.scss"],
})
export class CreateProfileThirdStepComponent
  implements OnInit, CreatorStepLifecycle, OnDestroy
{
  pendingSessionsSubscription: Subscription;
  activeSessionsSubscription: Subscription;
  nextStepSubject = new BehaviorSubject<boolean>(true);
  prevStepSubject = new BehaviorSubject<boolean>(false);
  canGoNext = this.nextStepSubject.asObservable();
  canGoBack = this.prevStepSubject.asObservable();
  error: string = null;
  checked = false;
  @Input() parentStepper: StepperComponent;
  @Input()
  profile: any;
  storedForm = null;
  isMobile$ = this.breakpointObserver
  .observe([Breakpoints.Small, Breakpoints.Handset])
  .pipe(
    distinctUntilChanged()
  );
  subscriptions: Subscription[] = [];
  sessionLink = "";
  constructor(
    public secureChatSelectors: SecureChatSelectors,
    public secureSyncSelectors: SecureSyncSelectors,
    public secureChatDispatchers: SecureChatDispatchers,
    public activatedRoute: ActivatedRoute,
    public changeDetectorRef: ChangeDetectorRef,
    private breakpointObserver: BreakpointObserver,
    private matSnackbar: MatSnackBar
  ) {}


  ngOnInit(): void {
    this.secureSyncSelectors.syncInviteSessionId$.subscribe((syncId) => {
      this.sessionLink = `${environment.baseUrl}/device/${syncId}`;
      this.changeDetectorRef.detectChanges();
    });
  }


  download(): void {
    this.secureChatSelectors
      .getSyncData()
      .pipe(first())
      .subscribe((backupData: BackupData) => {
        const backupDataJson = JSON.stringify(backupData);

        const blob = new Blob([backupDataJson], { type: "application/json" });

        const a = document.createElement("a");
        a.setAttribute(
          "download",
          `mastersession-${new Date().getTime()}.json`
        );
        a.setAttribute("href", URL.createObjectURL(blob));

        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
      });
  }

  getStepData() {
    return null;
  }

  onNext() {
    return new Promise((resolve) => resolve(true));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
