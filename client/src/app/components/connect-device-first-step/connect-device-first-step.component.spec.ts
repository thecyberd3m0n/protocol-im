import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectDeviceFirstStepComponent } from './connect-device-first-step.component';

describe('ConnectDeviceFirstStepComponent', () => {
  let component: ConnectDeviceFirstStepComponent;
  let fixture: ComponentFixture<ConnectDeviceFirstStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectDeviceFirstStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectDeviceFirstStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
