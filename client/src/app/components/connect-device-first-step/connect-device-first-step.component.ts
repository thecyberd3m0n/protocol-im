import { Component, OnInit, ViewChild } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { StepperComponent } from "../../modules/ux/components/stepper/stepper.component";
import { CreatorStepLifecycle } from "../../modules/ux/interfaces/interfaces/creator-step-lifecycle.interface";

@Component({
  selector: "connect-device-first-step",
  templateUrl: "./connect-device-first-step.component.html",
  styleUrls: ["./connect-device-first-step.component.scss"],
})
export class ConnectDeviceFirstStepComponent
  implements OnInit, CreatorStepLifecycle
{
  link: string;
  constructor() {}
  nextStepSubject = new BehaviorSubject<boolean>(true);
  prevStepSubject = new BehaviorSubject<boolean>(true);
  canGoNext = this.nextStepSubject.asObservable();
  canGoBack = this.prevStepSubject.asObservable();
  @ViewChild("stepper", { static: true, read: StepperComponent })
  stepperComponent: StepperComponent;
  getStepData() {
    return this.link;
  }
  onNext(): Promise<any> {
    return new Promise((resolve) => resolve(this.link));
  }

  ngOnInit(): void {}
}
