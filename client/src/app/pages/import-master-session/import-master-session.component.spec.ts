import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportMasterSessionComponent } from './import-master-session.component';

describe('ImportMasterSessionComponent', () => {
  let component: ImportMasterSessionComponent;
  let fixture: ComponentFixture<ImportMasterSessionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportMasterSessionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportMasterSessionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
