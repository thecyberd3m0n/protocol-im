import { Component, OnInit } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { FileSystemFileEntry, NgxFileDropEntry } from "ngx-file-drop";
import { SecureChatDispatchers, SecureChatMode, SecureChatSelectors } from "../../modules/secure-chat";

@Component({
  templateUrl: "./import-master-session.component.html",
  styleUrls: ["./import-master-session.component.scss"],
})
export class ImportMasterSessionComponent implements OnInit {
  syncId: string;
  nonce: string;
  passphrase: string;
  enablePassform = false;
  constructor(
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private secureChatDispatchers: SecureChatDispatchers,
    private secureChatSelectors: SecureChatSelectors,
    private router: Router,
  ) {}
  public files: NgxFileDropEntry[] = [];

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.syncId = params["id"];
      this.nonce = params["nonce"];
      if (this.syncId && this.nonce) {
        // Perform download from Cloud Storage
        // ...
        this.askForPassphrase();
      }
    });
  }

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        if (!fileEntry) {
          return;
        }
        // Validate file type
        if (fileEntry.name.endsWith(".json")) {
          fileEntry.file((file: File) => {
            // Read file contents as text
            const fileReader = new FileReader();
            fileReader.onload = (e) => {
              try {
                const jsonContent = JSON.parse(fileReader.result as string);

                // Validate JSON structure
                if (
                  typeof jsonContent.id === "string" &&
                  typeof jsonContent.nonce === "string"
                ) {
                  console.log("Valid JSON:", jsonContent);
                  this.syncId = jsonContent.id;
                  this.nonce = jsonContent.nonce;
                  // Log content of proper file
                  // TODO: dispatch import and move to unlocking master session
                  // this.loadMasterSession(jsonContent.id, jsonContent.nonce);
                  this.askForPassphrase();
                } else {
                  this.snackBar.open("Invalid JSON content", "Dismiss", {
                    duration: 5000,
                    panelClass: "error-toast",
                  });
                }
              } catch (error) {
                this.snackBar.open("Error parsing JSON content", "Dismiss", {
                  duration: 5000,
                  panelClass: "error-toast",
                });
              }
            };
            fileReader.readAsText(file);
          });
        } else {
          this.snackBar.open("Only JSON files are allowed", "Dismiss", {
            duration: 5000,
            panelClass: "error-toast",
          });
        }
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        this.snackBar.open("Directories are not allowed", "Dismiss", {
          duration: 5000,
          panelClass: "error-toast",
        });
      }
    }
  }

  loadMasterSession() {
    // dispatch the procedure here, wait till it ends, and move to unlock screen
    // this.secureChatSelectors.mode$.subscribe((mode) => {
    //   if (mode === SecureChatMode.READY) {
    //     this.router.navigate(['conversations']);
    //   }
    // });
    // this.secureChatDispatchers.importMasterSession(
    //   this.syncId,
    //   this.nonce,
    //   this.passphrase
    // );
  }

  private askForPassphrase() {
    this.enablePassform = true;
  }

  public fileOver(event) {
    console.log(event);
  }

  public fileLeave(event) {
    console.log(event);
  }
}
