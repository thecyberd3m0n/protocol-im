import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SecureChatSelectors } from '../../modules/secure-chat';
import { filter, map, switchMap } from 'rxjs/operators';
import { SerializedActiveSession } from '../../modules/secure-chat/models/activeSession.model';

@Component({
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})
export class SessionComponent implements OnInit {
  activeSession: SerializedActiveSession;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private changeDetector: ChangeDetectorRef,
    private secureChatSelectors: SecureChatSelectors
  ) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      map((params) => params.get('id')),
      filter(x => !!x),
      switchMap(id => this.secureChatSelectors.getActiveSession(id))
    ).subscribe(activeSession => {
      this.activeSession = activeSession;
    });
  }

}
