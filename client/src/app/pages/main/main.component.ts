import { Component } from '@angular/core';
import { environment } from '../../../environments/environment';
@Component({
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {
  getGitlabLogoPath() {
    return `${environment.baseUrl}/assets/gitlab.svg`;
  }
}
