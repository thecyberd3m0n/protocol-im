import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './no-session.component.html',
  styleUrls: ['./no-session.component.scss']
})
export class NoSessionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
