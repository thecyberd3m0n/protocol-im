import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import {
  SecureChatDispatchers,
  SecureChatMode,
  SecureChatSelectors,
} from "../../modules/secure-chat";
import { MatSnackBar } from "@angular/material/snack-bar";
import { SnackbarComponent, SnackbarState } from "../../modules/ux/components/snackbar/snackbar.component";

@Component({
  templateUrl: "./join-session.component.html",
  styleUrls: ["./join-session.component.scss"],
})
export class JoinSessionComponent implements OnInit, OnDestroy {
  subscriptions = [];
  constructor(
    private secureChatDispatchers: SecureChatDispatchers,
    private secureChatSelectors: SecureChatSelectors,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private matSnackbar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.subscriptions.push(
      this.activatedRoute.params.subscribe((params) => {
        const sessionId = params.id;
        this.subscriptions.push(
          this.secureChatSelectors.mode$.subscribe((mode) => {
            if (mode === SecureChatMode.GUEST) {
              this.subscriptions.push(
                this.secureChatSelectors.activeSessions$.subscribe(
                  (activeSessions) => {
                    if (activeSessions.length !== 0) {
                      // joined session
                      this.showJoinedSessionSnackbar(activeSessions[0].id, true);
                      this.router.navigate([
                        "conversations",
                        "session",
                        "active",
                        activeSessions[0].id,
                      ]);
                    }
                  }
                )
              );

              this.secureChatDispatchers.joinSession({ sessionId });
            } else if (mode === SecureChatMode.READY) {
              this.subscriptions.push(
                this.secureChatSelectors.activeSessions$.subscribe(
                  (activeSessions) => {
                    const activeSession = activeSessions.find(
                      (x) => x.inviteSessionId === sessionId
                    );
                    if (activeSession) {
                      // joined session
                      this.showJoinedSessionSnackbar(activeSession.id, false);
                      this.router.navigate([
                        "conversations",
                        "session",
                        "active",
                        activeSession.id,
                      ]);
                    }
                  }
                )
              );

              this.secureChatDispatchers.joinSession({ sessionId });
            }
          })
        );
      })
    );
  }

  showJoinedSessionSnackbar(sessionId: string, asGuest: boolean) {
    const snackbarComponent = this.matSnackbar.openFromComponent(
      SnackbarComponent,
      {
        duration: 3000,
        horizontalPosition: "end",
        verticalPosition: "bottom",
      }
    );
    snackbarComponent.instance.message = `Joined session ${sessionId} ${asGuest ?? 'as Guest'}`;
    snackbarComponent.instance.snackbarState = SnackbarState.Success;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
