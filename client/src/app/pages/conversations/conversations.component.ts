import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { Component, OnDestroy, OnInit } from "@angular/core";
import {
  MatBottomSheet
} from "@angular/material/bottom-sheet";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { distinctUntilChanged, filter } from "rxjs/operators";
import { StoreSessionStepperComponent } from "../../components/store-session-stepper/store-session-stepper.component";
import { UserProfileComponent } from "../../components/user-profile/user-profile.component";
import {
  SecureChatDispatchers,
  SecureChatMode,
  SecureChatSelectors,
} from "../../modules/secure-chat";
import { MetadataMap } from "../../modules/secure-chat/models/metadataMap.model";
import { NotificationsSelectors } from "../../modules/secure-chat/services/notifications-selectors.service";
import { NotificationsDispatchers } from "../../modules/secure-chat/services/notifications.dispatchers";
import {
  NotifyBarAction,
  NotifyBarColor,
} from "../../modules/ux/components/notify-bar/notify-bar.component";
import { SidenavService } from "../../modules/ux/layouts/sidenav-layout/sidenav-layout.service";
import {
  Profile,
  ProfileMessageService,
} from "../../services/profile-message.service";

@Component({
  templateUrl: "./conversations.component.html",
  styleUrls: ["./conversations.component.scss"],
})
export class ConversationsComponent implements OnInit, OnDestroy {
  sessionWasStarted = false;
  leftSideOpened = false;
  sidebarLoaded: any;
  rightSideOpened = false;
  mode: SecureChatMode;
  SecureChatMode = SecureChatMode;
  activeSessions = [];
  sessionId = null;
  profile: Profile | null = null;
  peerProfile: MetadataMap;
  isMobile$ = this.breakpointObserver
    .observe([Breakpoints.Small, Breakpoints.Handset])
    .pipe(distinctUntilChanged());
  startStepper = false;

  peerProfileSub: Subscription;
  subscriptions = [];


  constructor(
    private matBottomSheet: MatBottomSheet,
    public secureChatSelectors: SecureChatSelectors,
    private secureChatDispatchers: SecureChatDispatchers,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private breakpointObserver: BreakpointObserver,
    private profileService: ProfileMessageService,
    private sidenavService: SidenavService,
    private notificationsDispatcher: NotificationsDispatchers,
  ) {}

  ngOnInit() {

    // get sessions. If there are no active sessions - go to new session
    // if there are active sessions - open first
    this.subscriptions.push(
      this.secureChatSelectors.mode$.subscribe((mode: SecureChatMode) => {
        this.mode = mode;

        if (mode === SecureChatMode.GUEST) {
          this.startStepper = true;
          this.sidenavService.load(StoreSessionStepperComponent, {});
        }

        if (mode === SecureChatMode.READY || SecureChatMode.GUEST) {
          this.setNameChangeEvent();
        }
      })
    );
    this.subscriptions.push(
      this.router.events
        .pipe(filter((event) => event instanceof NavigationEnd))
        .subscribe((_event: NavigationEnd) => {
          this.leftSideOpened = false;
        })
    );

    if (this.activatedRoute.children.length === 0) {
      this.subscriptions.push(
        this.secureChatSelectors
          .getPendingSessions()
          .subscribe((pendingSessions) => {
            if (pendingSessions.length === 1) {
              this.sessionWasStarted = true;
              this.secureChatDispatchers.startSession({
                manualApprove: false,
                groupMode: false,
                oneTime: true,
              });
            } else if (pendingSessions.length === 2) {
              this.router.navigate([
                "conversations",
                "session",
                "new",
                pendingSessions[1].id, //TODO: select session that is not "sync" session
              ]);

              this.sessionWasStarted = true;
              this.notificationsDispatcher.init();
            }
          })
      );
      // this.secureChatSelectors.global$.subscribe((secureChatState) => {
      //   if (
      //     (secureChatState.mode === SecureChatMode.READY || SecureChatMode.GUEST) &&
      //     !this.sessionWasStarted &&
      //     secureChatState.activeSessions.ids.length === 0 &&
      //     secureChatState.pendingSessions.ids.length <= 1
      //   ) {
      //     // create pending session

      //   } else if (secureChatState.activeSessions.ids.length === 0 && secureChatState.pendingSessions.ids.length <= 1) {
      //     this.router.navigate([
      //       "conversations",
      //       "session",
      //       "new",
      //       secureChatState.pendingSessions.ids[1], //TODO: select session that is not "sync" session
      //     ]);

      //     this.sessionWasStarted = true;
      //   }
      // });
    }
  }

  toggleSidenav() {
    this.leftSideOpened = !this.leftSideOpened;
  }

  setNameChangeEvent() {
    // TODO: set dynamic title
    this.subscriptions.push(
      this.router.events
        .pipe(filter((event) => event instanceof NavigationEnd))
        .subscribe((event: NavigationEnd) => {
          this.sessionId = event.url.substring(event.url.lastIndexOf("/") + 1);
          const parts = event.url.split("/");

          if (
            parts[2] === "session" &&
            parts[3] === "active" &&
            this.sessionId.length > 1
          ) {
            if (this.peerProfileSub?.unsubscribe) {
              this.peerProfileSub.unsubscribe();
            }
            this.subscriptions.push(
              this.profileService
                .getProfileForSession$(this.sessionId)
                .subscribe((profile) => {
                  this.peerProfile = profile;
                })
            );
          }
        })
    );
  }

  clickAvatar(): void {
    // load user profile
    if (!this.sidebarLoaded) {
      if (this.mode === SecureChatMode.READY) {
        this.sidenavService.unloadSidenav();
        this.sidenavService.load(UserProfileComponent, {});
      } else if (this.mode === SecureChatMode.GUEST || this.startStepper) {
        this.sidenavService.load(StoreSessionStepperComponent, {});
      }
      this.subscriptions.push(
        this.sidenavService.mainSidenav$.subscribe((component) => {
          this.rightSideOpened = !!component;
        })
      );

      this.sidebarLoaded = true;
      this.rightSideOpened = true;
    } else {
      this.rightSideOpened = !this.rightSideOpened;
    }
  }

  stepperFinished(): void {
    this.startStepper = false;
    this.rightSideOpened = false;
  }

  closeLeftPanel(): void {
    this.leftSideOpened = false;
  }

  onActivate($event) {}

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
