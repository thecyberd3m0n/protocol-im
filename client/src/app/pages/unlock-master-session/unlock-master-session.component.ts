import { Component, HostListener, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import {
  SecureChatDispatchers,
  SecureChatMode,
  SecureChatSelectors,
} from "../../modules/secure-chat";
import { Subscription } from "rxjs";
import {
  SnackbarComponent,
  SnackbarState,
} from "src/app/modules/ux/components/snackbar/snackbar.component";
import { MatSnackBar } from "@angular/material/snack-bar";
import { UnlockMasterSessionPassphraseComponent } from "../../components/unlock-master-session-passphrase/unlock-master-session-passphrase.component";
import { StepperComponent } from "../../modules/ux/components/stepper/stepper.component";

@Component({
  styleUrls: ["./unlock-master-session.component.scss"],
  templateUrl: "./unlock-master-session.component.html",
})
export class UnlockMasterSessionComponent implements OnInit, OnDestroy {
  r: string;
  subscription: Subscription;
  @ViewChild(StepperComponent)
  parentStepper: StepperComponent;
  @ViewChild(UnlockMasterSessionPassphraseComponent)
  firstComponent: UnlockMasterSessionPassphraseComponent;
  UnlockMasterSessionPassphraseComponent = UnlockMasterSessionPassphraseComponent;
  constructor(
    private route: ActivatedRoute,
    private secureChatSelectors: SecureChatSelectors,
    private secureChatDispatchers: SecureChatDispatchers,
    private router: Router,
    private matSnackbar: MatSnackBar
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.r = params.r;
    });

    this.subscription = this.secureChatSelectors.mode$.subscribe(
      (mode: SecureChatMode) => {
        if (mode === SecureChatMode.READY) {
          this.showSessionUnlockedSnackbar();
          this.router.navigateByUrl(
            this.r ? decodeURIComponent(this.r) : "conversations"
          );
        }
      }
    );
    
  }

  showSessionUnlockedSnackbar(): void {
    const snackbarComponent = this.matSnackbar.openFromComponent(
      SnackbarComponent,
      {
        duration: 3000,
        horizontalPosition: "end",
        verticalPosition: "bottom",
      }
    );
    snackbarComponent.instance.message = "Account unlocked";
    snackbarComponent.instance.snackbarState = SnackbarState.Success;
  }

  onFinished($event) {
    this.secureChatDispatchers.unlockMasterSession($event[0]);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
