import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import {
  SecureChatDispatchers,
  SecureChatSelectors,
} from "../modules/secure-chat";
import { SerializedActiveSession } from "../modules/secure-chat/models/activeSession.model";
import { DecryptedOlmEnvelope } from "../modules/secure-chat/models/decryptedOlmEnvelope.model";
import { OutgoingMessageModel } from "../modules/secure-chat/models/outgoingMessage.model";
import { MessageInterpreterService } from "./message-interpreter.service";
import { CustomMessageCallbacks } from "./message-service-lifecycle.interface";
import { NotificationsDispatchers } from "../modules/secure-chat/services/notifications.dispatchers";

@Injectable({
  providedIn: "root",
})
export class TextMessageService implements CustomMessageCallbacks {
  TYPE = "text";
  currentSessionId: string;
  messages$ = this.secureChatSelectors
    .getMessages({ type: "text" })
    .pipe(
      map((messages) =>
        messages.sort(
          (a: DecryptedOlmEnvelope, b: DecryptedOlmEnvelope) =>
            a.timestamp.getTime() - b.timestamp.getTime()
        )
      )
    );

  constructor(
    private messageInterpreterService: MessageInterpreterService,
    private secureChatDispatchers: SecureChatDispatchers,
    private secureChatSelectors: SecureChatSelectors,
    private notificationsDispatchers: NotificationsDispatchers
  ) {}

  init() {
    this.messageInterpreterService.registerMessageType("text", this);
  }
  onMessage(
    _sessionId: string,
    _message: DecryptedOlmEnvelope
  ): void | Promise<void> {
    // we can leave it empty if we want to expose just filtered envelopes
    // in other message type you can use it to manipulate metadata in MasterSession, ActiveSessions and other Messages
    console.log("Text message received", _message);
  }

  loadMessagesForSession(sessionId: string, limit: number, skipLast: number) {
    this.secureChatDispatchers.loadMessages(sessionId, limit, skipLast);
  }

  sendMessage(session: SerializedActiveSession, outgoingMessage: OutgoingMessageModel) {
    this.secureChatDispatchers.outgoingMessage(outgoingMessage);

  }
}
