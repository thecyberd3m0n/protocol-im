import { Injectable } from "@angular/core";
import { Subscription } from "rxjs";
import { SecureChatSelectors } from "../modules/secure-chat";
import { DecryptedOlmEnvelope } from "../modules/secure-chat/models/decryptedOlmEnvelope.model";
import { CustomMessageCallbacks } from "./message-service-lifecycle.interface";

type SubscriptionWithCMC = {
  customMessageCallback: CustomMessageCallbacks;
  subscription: Subscription;
};
@Injectable({
  providedIn: "root",
})
export class MessageInterpreterService {
  eventMessagesRegistry = new Map<string, SubscriptionWithCMC>();
  constructor(private secureChatSelectors: SecureChatSelectors) {}

  registerMessageType(type: string, messageCallbacks: CustomMessageCallbacks) {
    // TODO: onmessage triggers on each load. Make it trigger on new message
    const subscription = this.secureChatSelectors
      .getMostRecentMessage({ type, own: false })
      .subscribe((envelope: DecryptedOlmEnvelope) => {
        // TODO: BUG - subscription returns old "myprofile" messages on outgoing message
        if (this.eventMessagesRegistry.get(type)) {
          this.eventMessagesRegistry
            .get(type)
            .customMessageCallback.onMessage(envelope.sessionId, envelope);
        } else {
          console.error(
            `Unsupported message detected in session ${envelope.sessionId}`,
            type,
            envelope
          );
        }
      });

    this.eventMessagesRegistry.set(type, {
      customMessageCallback: messageCallbacks,
      subscription,
    });

    console.log("registered message type", type);
  }
}
