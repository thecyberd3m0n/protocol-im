import { DecryptedOlmEnvelope } from "../modules/secure-chat/models/decryptedOlmEnvelope.model";

export interface CustomMessageCallbacks {
  TYPE: string;

  /**
   * Must define what to do on new message received
   * @param sessionId
   * @param message
   */
  onMessage(sessionId: string, message: DecryptedOlmEnvelope): Promise<void> | void;
}
