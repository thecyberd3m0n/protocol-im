import { Injectable, OnDestroy, OnInit } from "@angular/core";
import { CustomMessageCallbacks } from "./message-service-lifecycle.interface";
import { DecryptedOlmEnvelope } from "../modules/secure-chat/models/decryptedOlmEnvelope.model";
import { SecureChatDispatchers } from "../modules/secure-chat";
import { MessageInterpreterService } from "./message-interpreter.service";
import { BehaviorSubject, Observable, Subject, timer } from "rxjs";
import {
  tap,
  takeUntil,
  startWith,
  switchMapTo,
  auditTime,
} from "rxjs/operators";
import { session } from "@wireapp/proteus";
import { log } from "util";
@Injectable({ providedIn: "root" })
export class WritingMessageService
  implements CustomMessageCallbacks, OnDestroy
{
  TYPE = "wrtitingMessage";
  private isWriting = false;
  //waiting time for next message
  private waitTime: number = 5000;
  //WM sending interval
  private intervalTime: number = this.waitTime - 1000;
  private destroy$: Subject<void> = new Subject<void>();
  public writingSub: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );
  public isWriting$: Observable<boolean> = this.writingSub.asObservable();
  private reset$: Subject<void> = new Subject<void>();
  private timer$: Observable<number> = timer(this.waitTime, this.waitTime);
  public typingSub: Subject<string> = new Subject<string>();
  constructor(
    private messageInterpreterService: MessageInterpreterService,
    private secureChatDispatchers: SecureChatDispatchers
  ) {
    //when timer reach the end emit false on writingSubject
    this.reset$
      .pipe(
        startWith(void 0),
        switchMapTo(this.timer$),
        takeUntil(this.destroy$)
      )
      .subscribe((x) => {
        this.writingSub.next(false);
      });
    this.typingSub
      .pipe(
        tap((sessionId) => {
          if (!this.isWriting) {
            this.sendWritingMessage(sessionId);
          }
        }),
        auditTime(this.intervalTime),
        takeUntil(this.destroy$)
      )
      .subscribe((sessionId) => {
        this.isWriting = false;
      });
  }

  init() {
    this.messageInterpreterService.registerMessageType(this.TYPE, this);
  }
  msgParse(message: DecryptedOlmEnvelope): boolean {
    return JSON.parse(message.content);
  }
  onMessage(
    _sessionId: string,
    message: DecryptedOlmEnvelope
  ): Promise<void> | void {
    //when recived WM emit true on writingSubject and reset timer
    this.writingSub.next(true);
    this.reset$.next();
  }
  sendWritingMessage(sessionId: string) {
    this.isWriting = true;
    this.secureChatDispatchers.outgoingMessage({
      sessionId,
      type: this.TYPE,
      noStore: true,
      content: "",
    });
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
