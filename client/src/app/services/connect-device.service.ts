import { Injectable } from "@angular/core";
import { MatBottomSheet, MatBottomSheetRef } from "@angular/material/bottom-sheet";

import { Subscription } from "rxjs";
import { SecureSyncDispatchers } from "../modules/secure-chat/services/secure-sync.dispatchers";
import { SecureSyncSelectors } from "../modules/secure-chat/services/secure-sync.selectors";
import {
  BottomSnackbarComponent,
  BottomSnackbarConfig
} from "../modules/ux/components/bottom-snackbar/bottom-snackbar.component";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { SnackbarComponent, SnackbarState } from "../modules/ux/components/snackbar/snackbar.component";
@Injectable({
  providedIn: "root",
})
export class ConnectDeviceService {
  subscription: Subscription | null = null;
  bottomSheet: MatBottomSheetRef<
    BottomSnackbarComponent,
    { proceed: boolean }
  > | null;

  passphrase: string;
  inited = false;
  constructor(
    private secureSyncDispatchers: SecureSyncDispatchers,
    private secureSyncSelectors: SecureSyncSelectors,
    private matBottomSheet: MatBottomSheet,
    private matDialog: MatDialog,
    private matSnackbar: MatSnackBar
  ) {}

  init(passphrase: string): void {
    // listen for ReceiveAskForSyncSession action and invoke the Snackbar
    this.passphrase = passphrase;
    if (!this.inited) {
      this.subscription = this.secureSyncSelectors.unapprovedDevices$.subscribe(
        (devices) => {
          if (devices.length > 0) {
            this.bottomSheet = this.matBottomSheet.open(
              BottomSnackbarComponent,
              {
                backdropClass: 'protocol-bottom-sheet',
                hasBackdrop: false,
                data: {
                  panelClass: 'bottom-sheet-container',
                  message: `Device ${devices[0].device.name} wants to import your account. Do you recognize it?`,
                  actions: [
                    {
                      label: "Reject",
                      callback: () =>
                        this.bottomSheet.dismiss({ proceed: false }),
                      color: "warn",
                    },
                    {
                      label: "Approve",
                      callback: () =>
                        this.bottomSheet.dismiss({ proceed: true }),
                      color: "primary",
                    },
                  ],
                } as BottomSnackbarConfig,
              }
            );
            this.bottomSheet.afterDismissed().subscribe(({ proceed }) => {
              // mat dialog to check passphrase
              if (proceed) {
                // allow the device[0]
                // TODO: invoke main passphrase prompt screen
                this.secureSyncDispatchers.approveSyncSession(devices[0].pendingSessionId, passphrase, devices[0].device.fingerprint);
                this.showEnrollingDeviceSnackbar(devices[0].device.fingerprint);
              } else {
                // reject the device[0]
                this.secureSyncDispatchers.rejectSyncSession(devices[0].pendingSessionId);
              }
              this.bottomSheet = null;
            });
          }
        }
      );
      this.inited = true;
    }
  }

  showEnrollingDeviceSnackbar(deviceFingerPrint: string) {
    const snackbarComponent = this.matSnackbar.openFromComponent(
      SnackbarComponent,
      {
        duration: 3000,
        horizontalPosition: "end",
        verticalPosition: "bottom",
      }
    );
    snackbarComponent.instance.message = `Enrolling device ${deviceFingerPrint}`;
    snackbarComponent.instance.snackbarState = SnackbarState.Neutral;
  }
}
