import { Injectable } from "@angular/core";
import { CustomMessageCallbacks } from "./message-service-lifecycle.interface";
import {
  DecryptedMessage,
  SecureChatDispatchers,
  SecureChatSelectors,
} from "../modules/secure-chat";
import { MessageInterpreterService } from "./message-interpreter.service";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";

export interface ReactionMessage {
  messageId: string;
  emoji: string;
  user?: "own" | "opposite"; //TODO change to username when group chats will be implemented
}
@Injectable({
  providedIn: "root",
})
export class ReactionMessageService implements CustomMessageCallbacks {
  TYPE = "reactionMessage";
  private reactionMessagesSubject: BehaviorSubject<ReactionMessage> =
    new BehaviorSubject<ReactionMessage>({ messageId: null, emoji: null });
  constructor(
    private messageInterpreterService: MessageInterpreterService,
    private secureChatDispatchers: SecureChatDispatchers,
    private secureChatSelectors: SecureChatSelectors
  ) { }
  init() {
    this.messageInterpreterService.registerMessageType(this.TYPE, this);
  }
  parseMessage(message: DecryptedMessage): ReactionMessage {
    return JSON.parse(message.content);
  }
  onMessage(
    sessionId: string,
    message: DecryptedMessage
  ): void | Promise<void> {
    this.reactionMessagesSubject.next(this.parseMessage(message));
  }
  sendReactionMessage(sessionId: string, message: ReactionMessage) {
    this.secureChatDispatchers.outgoingMessage({
      sessionId,
      type: this.TYPE,
      noStore: false,
      content: JSON.stringify(message),
    });
  }
  reactions$(messageId: string): Observable<ReactionMessage[]> {
    return this.secureChatSelectors.getMessages({ type: this.TYPE }).pipe(
      map((messages) =>
        messages.map((message) => ({
          ...this.parseMessage(message),
          user: message.own ? "own" : "opposite",
        }))
      ),
      map((reactions) =>
        reactions.filter((reaction) => reaction.messageId === messageId)
      ),
      map((reactions) => [
        ...new Map(
          reactions.map((result) => [result["user"], result])
        ).values(),
      ])
    ) as Observable<ReactionMessage[]>;
  }
}
