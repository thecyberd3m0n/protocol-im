import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import {
  filter,
  first,
  map,
  switchMap,
  take,
  tap,
  withLatestFrom,
} from "rxjs/operators";
import {
  SecureChatDispatchers,
  SecureChatMode,
  SecureChatSelectors
} from "../modules/secure-chat";
import { SerializedActiveSession } from "../modules/secure-chat/models/activeSession.model";
import { DecryptedOlmEnvelope } from "../modules/secure-chat/models/decryptedOlmEnvelope.model";
import { MetadataMap } from "../modules/secure-chat/models/metadataMap.model";
import { SecureSyncSelectors } from "../modules/secure-chat/services/secure-sync.selectors";
import { MessageInterpreterService } from "./message-interpreter.service";
import { CustomMessageCallbacks } from "./message-service-lifecycle.interface";
import { MatSnackBar } from "@angular/material/snack-bar";
import { SnackbarComponent, SnackbarState } from "../modules/ux/components/snackbar/snackbar.component";

export interface Profile {
  username: string;
  bio: string;
}

@Injectable({
  providedIn: "root",
})
export class ProfileMessageService implements CustomMessageCallbacks {
  TYPE = "myprofile";
  // TODO: perhaps we could ditch those
  profileMeta: MetadataMap;
  profile$ = this.secureChatSelectors
    .getMasterSessionMeta(this.TYPE)
    .pipe(filter((x) => !!x));
  private activeSessions: SerializedActiveSession[] = [];
  inited = false;
  constructor(
    private secureChatSelectors: SecureChatSelectors,
    private secureChatDispatchers: SecureChatDispatchers,
    private secureSyncSelectors: SecureSyncSelectors,
    private mis: MessageInterpreterService,
    private matSnackbar: MatSnackBar
  ) {}

  init(): void {
    if (!this.inited) {
      this.inited = true;
      this.mis.registerMessageType(this.TYPE, this);
      this.secureChatSelectors.mode$.subscribe((mode) => {
        if (mode === SecureChatMode.READY) {
          this.secureChatSelectors
            .getMasterSessionMeta(this.TYPE)
            .pipe(
              filter((x) => !!x),
              take(1)
            )
            .subscribe((profile) => {
              this.profileMeta = profile;
            });
        }
      });

      // listen to new activeSessions and send profile
      this.secureChatSelectors.activeSessions$
        .pipe(
          switchMap((activeSessions) =>
            this.secureSyncSelectors.syncInviteSessionId$.pipe(
              map((syncId) => ({
                activeSessions,
                syncId,
              }))
            )
          ),
          filter(
            ({
              activeSessions,
              syncId,
            }: {
              activeSessions: SerializedActiveSession[];
              syncId: string;
            }) => {
              const prevLength = this.activeSessions.length;
              return activeSessions.length > prevLength;
            }
          ),
          tap(
            ({
              activeSessions,
              syncId,
            }: {
              activeSessions: SerializedActiveSession[];
              syncId: string;
            }) => {
              this.activeSessions = activeSessions;
            }
          ),
          map(
            ({
              activeSessions,
              syncId,
            }: {
              activeSessions: SerializedActiveSession[];
              syncId: string;
            }) => ({
              activeSession: activeSessions[activeSessions.length - 1],
              syncId,
            })
          ),
          filter(({ activeSession, syncId }) => {
            return activeSession.inviteSessionId !== syncId;
          })
        )
        .subscribe(({ activeSession }) => {
          if (this.profileMeta) {
            // send user profile on new session
            this.secureChatDispatchers.outgoingMessage({
              sessionId: activeSession.id,
              content: JSON.stringify(this.profileMeta),
              type: this.TYPE,
              noStore: true,
            });
          }
        });
    }
  }

  onMessage(
    sessionId: string,
    message: DecryptedOlmEnvelope
  ): void | Promise<void> {
    console.log(
      "Profile Message received, updating peers metadata",
      sessionId,
      message
    );
    
    const msg = JSON.parse(message.content);

    this.secureChatDispatchers.updateActiveSessionMeta(
      sessionId,
      this.TYPE,
      msg
    );
    this.showProfileChangeSnackbar(sessionId);
    // this.secureChatSelectors
    //   .getActiveSessionMeta(sessionId, this.TYPE)
    //   .pipe(take(1)) // fire only once, crucial
    //   .subscribe((meta: MetadataMap) => {
    //     if (JSON.stringify(meta) !== JSON.stringify(msg)) {
    //       console.log("updating active session meta, objs are different");

    //     }
    //   });
  }

  showProfileChangeSnackbar(forSessionId: string) {
    const snackbarComponent = this.matSnackbar.openFromComponent(
      SnackbarComponent,
      {
        duration: 3000,
        horizontalPosition: "end",
        verticalPosition: "bottom",
      }
    );
    snackbarComponent.instance.message = `Profile updated for session ${forSessionId}`;
    snackbarComponent.instance.snackbarState = SnackbarState.Neutral;
  }

  getProfileForSession$(sessionId: string): Observable<any> {
    return this.secureChatSelectors.getActiveSession(sessionId).pipe(
      filter((x) => !!x.meta),
      map((x: SerializedActiveSession) => x.meta.myprofile)
    );
  }

  changeProfile(metadataMap: MetadataMap) {
    this.profileMeta = metadataMap;
    this.updateSessionMetadata(this.profileMeta);
    this.updatePeersActiveSessionProfile();
  }

  // send profile updates to activeSessions
  private updatePeersActiveSessionProfile() {
    if (this.profileMeta) {
      this.secureChatSelectors
        .getActiveSessions()
        .pipe(first())
        .subscribe((activeSessions) => {
          activeSessions.forEach((as) => {
            this.secureChatDispatchers.outgoingMessage({
              sessionId: as.id,
              content: JSON.stringify(this.profileMeta),
              type: this.TYPE,
              noStore: true,
            });
          });
        });
    }
  }

  private updateSessionMetadata(metadata: MetadataMap) {
    this.secureChatDispatchers.updateMasterSessionMeta(this.TYPE, metadata);
  }
}
