import { Injectable } from "@angular/core";
import { map, switchMap } from "rxjs/operators";
import { DecryptedOlmEnvelope } from "../modules/secure-chat/models/decryptedOlmEnvelope.model";
import { SecureChatSelectors } from "../modules/secure-chat";
import { NotificationsDispatchers } from "../modules/secure-chat/services/notifications.dispatchers";

@Injectable()
export class NotifyMessageService {
  messages$ = this.secureChatSelectors
    .getMostRecentMessage({
        type: 'text'
    })
  constructor(
    private secureChatSelectors: SecureChatSelectors,
    private notificationsDispatchers: NotificationsDispatchers
  ) {}

  init() {
    this.messages$.pipe(switchMap((msg) => {
        return this.secureChatSelectors.getActiveSession(msg.sessionId).pipe(map((as) => ({ activeSession: as, msg })))
    })).subscribe(({ msg, activeSession}) => {
        if (activeSession.meta.notifications) {
            console.log(`[NotifyMessageService] notify device activeSession.meta.notifications.devTokens`);
            
            this.notificationsDispatchers.notify({
                content: 'msg',
                devTokens: activeSession.meta.notifications.devTokens
            })
        }
        
    })
  }
}
