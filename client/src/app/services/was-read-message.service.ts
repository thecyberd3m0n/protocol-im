import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import {
  distinctUntilKeyChanged,
  map,
  takeUntil
} from "rxjs/operators";
import {
  SecureChatDispatchers,
  SecureChatSelectors,
} from "../modules/secure-chat";
import { DecryptedOlmEnvelope } from "../modules/secure-chat/models/decryptedOlmEnvelope.model";
import { OutgoingMessageModel } from "../modules/secure-chat/models/outgoingMessage.model";
import { MessageInterpreterService } from "./message-interpreter.service";
import { CustomMessageCallbacks } from "./message-service-lifecycle.interface";
export interface LastReadMessage {
  id: string;
}
@Injectable({
  providedIn: "root",
})
export class WasReadMessageService
  implements CustomMessageCallbacks
{
  TYPE = "wasReadMessage";
  private destroy$: Subject<boolean> = new Subject<boolean>();
  private wasReadMessageSubject: BehaviorSubject<OutgoingMessageModel> =
    new BehaviorSubject<OutgoingMessageModel>({
      content: "",
      noStore: true,
      sessionId: "",
      type: "",
    });
  constructor(
    private messageInterpreterService: MessageInterpreterService,
    private secureChatSelectors: SecureChatSelectors,
    private secureChatDispatchers: SecureChatDispatchers
  ) {}

  init() {
    this.messageInterpreterService.registerMessageType(this.TYPE, this);
  }

  sendWasReadMessage(sessionId: string, messageId: string) {
    this.wasReadMessageSubject.next({
      sessionId,
      type: this.TYPE,
      noStore: false,
      content: JSON.stringify({ id: messageId }),
    });
  }

  private parseMsg(message: DecryptedOlmEnvelope) {
    return JSON.parse(message.content);
  }

  onMessage(
    sessionId: string,
    message: DecryptedOlmEnvelope
  ): Promise<void> | void {
    this.setLastReadMessage(sessionId, { id: this.parseMsg(message).id });
  }

  setLastReadMessage(sessionId: string, lastReadMessage: LastReadMessage) {
    this.secureChatDispatchers.updateActiveSessionMeta(
      sessionId,
      this.TYPE,
      lastReadMessage
    );
  }

  getLastReadedMessages$(sessionId): Observable<LastReadMessage> {
    return this.secureChatSelectors
      .getActiveSessionMeta(sessionId, this.TYPE)
      .pipe(
        map((lastReadMessage) => (lastReadMessage ? lastReadMessage : {})),
        takeUntil(this.destroy$)
      );
  }

}
