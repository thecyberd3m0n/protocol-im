import { Injectable } from '@angular/core';

export interface RGB {
  r: number;
  g: number;
  b: number;
}

@Injectable({
  providedIn: 'root'
})

export class ColorService {

  constructor() { }

  normalizeColor(color: string) {
    if (color.charAt(0) === '#') {
      return this.hexToRgb(color);
    }
    return this.getRGBFromString(color);
  }

  addBrightness(color: RGB, brightness: number) {
    return {
      r: color.r + brightness,
      g: color.g + brightness,
      b: color.b + brightness
    };
  }

  getRGBFromString(color: string): RGB {
    const colorMatcher = /rgb\((\d{1,3}), (\d{1,3}), (\d{1,3})\)/;
    const match = colorMatcher.exec(color);
    return {
      r: parseInt(match[1], 10),
      g: parseInt(match[2], 10),
      b: parseInt(match[3], 10)
    };
  }

  getString(color: RGB) {
    return `rgb(${color.r},${color.g},${color.b})`;
  }

  private hexToRgb = hex => {
    const arr = hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i
      , (m, r, g, b) => '#' + r + r + g + g + b + b)
      .substring(1).match(/.{2}/g)
      .map(x => parseInt(x, 16));
    return {
      r: arr[0],
      g: arr[1],
      b: arr[2]
    };
  }
}
