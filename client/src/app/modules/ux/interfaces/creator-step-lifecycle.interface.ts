import { Component } from '@angular/core';
import { Observable } from 'rxjs';

export interface CreatorStepLifecycle extends Component {
  canGoNext: Observable<boolean>;
  canGoBack: Observable<boolean>;
  getStepData(): any;
  onNext(): Promise<any>;
}
