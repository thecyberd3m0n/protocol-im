import { Observable } from 'rxjs';

export interface CreatorStepLifecycle {
  canGoNext: Observable<boolean>;
  canGoBack: Observable<boolean>;
  getStepData(): any;
  onNext(): Promise<any>;
}
