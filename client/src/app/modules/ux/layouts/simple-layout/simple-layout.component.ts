import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ux-layout-top',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpleLayoutTopComponent {

}

@Component({
  selector: 'ux-layout-content',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpleLayoutContentComponent {

}

@Component({
  selector: 'ux-layout-footer',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpleLayoutFooterComponent {

}

@Component({
  selector: 'ux-simple-layout',
  templateUrl: './simple-layout.component.html',
  styleUrls: ['./simple-layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpleLayoutComponent {
}
