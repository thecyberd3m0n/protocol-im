import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ux-nav-tabs-layout',
  templateUrl: './nav-tabs-layout.component.html',
  styleUrls: ['./nav-tabs-layout.component.scss'],
})
export class NavTabsLayoutComponent implements OnInit {
  navLinks = [];
  constructor(private route: ActivatedRoute) { }
  ngOnInit(): void {
    this.navLinks = this.route.snapshot.routeConfig.children.map((child) => {
      return {
        label: child.data.label,
        path: child.path
      };
    });
  }
}
