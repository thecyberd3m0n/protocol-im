import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { UxService } from '../../ux.service';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map, mergeMap } from 'rxjs/operators';
@Component({
  selector: 'ux-bottom-menu-layout',
  templateUrl: './bottom-menu-layout.component.html',
  styleUrls: ['./bottom-menu-layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BottomMenuLayoutComponent implements OnDestroy, OnInit {

  label = '';
  navLinks = [];
  routeSub: Subscription;
  constructor(
    private uxService: UxService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.routeSub = this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map(route => {
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        }),
        mergeMap(route => route.data)
      )
      .subscribe(data => {
        this.setTitle(data.label);
        this.label = data.label;
      });
  }

  ngOnInit(): void {
    if (!this.activatedRoute.snapshot.routeConfig.children) {
      throw new Error('Please add children to route where you use ux-bottom-menu-layout');
    }
    this.navLinks = this.activatedRoute.snapshot.routeConfig.children.map((child) => {
      if (!child.data.label || !child.path || !child.data.icon) {
        throw new Error('Your child route should have following data: icon, label');
      }
      return {
        label: child.data.label,
        path: child.path,
        icon: child.data.icon,
      };
    });
  }

  private setTitle(routeTitle: string) {
    this.uxService.setTitle(routeTitle);
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }
}
