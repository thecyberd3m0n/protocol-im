import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  ContentChildren,
  Input,
  OnDestroy,
  QueryList,
  ViewChild,
  Directive,
  TemplateRef,
  HostListener,
  OnInit,
  AfterViewInit
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { noop, Subscription } from 'rxjs';
import { filter, map, mergeMap } from 'rxjs/operators';
import { UxService } from '../../ux.service';
import { TopbarComponent } from '../../components/topbar/topbar.component';

@Directive({ selector: '[uxMenuComponent]' })
export class MenuDirective {
  constructor(public template: TemplateRef<any>) { }
}

@Component({
  selector: 'ux-sidenav-menu-layout',
  templateUrl: './sidenav-menu-layout.component.html',
  styleUrls: ['./sidenav-menu-layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidenavMenuLayoutComponent implements OnDestroy, AfterViewInit {
  @Input()
  showMenu = true;
  @Input()
  leftSideOpened = false;
  label: string = '';
  @ViewChild('sidenav') sidenav;

  @ContentChildren(MenuDirective)
  menuComponents: QueryList<MenuDirective>;
  sideNavOpened = false;
  toggle = true;
  title = '';
  topBarLabel = '';
  routeSub: Subscription;
  alwaysOpened = false;
  width = 0;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private uxService: UxService,
    private ref: ChangeDetectorRef
  ) {
    // TODO: move to commons/routing-helper service
    this.routeSub = this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map(route => {
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        }),
        mergeMap(route => route.data)
      )
      .subscribe(data => {
        this.label = data.label;

      });
  }
  ngAfterViewInit(): void {
    // this.topbarComponent
    // ? (this.topbarComponent.label = this.label)
    // : noop();
  this.uxService.setTitle(this.label);
  }
  ngOnInit(): void {
    this.onResize({target: {
      innerWidth: window.innerWidth
    }});
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.width = event.target.innerWidth;
    this.alwaysOpened = this.width > 1366;
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }
}
