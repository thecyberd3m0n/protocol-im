import { Component, Injectable, Type } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";

@Injectable()
export class SidenavService {
  private mainSidenavSub = new BehaviorSubject<{
    component: Type<any>;
    params: any;
  } | null>(null);
  private childSidenavSub = new BehaviorSubject<{
    component: Type<any>;
    params: any;
  }>(null);
  mainSidenav$ = this.mainSidenavSub.asObservable();
  childSidenav$ = this.childSidenavSub.asObservable();

  private childOpenedSub = new BehaviorSubject<boolean>(false);
  childOpened$ = this.childOpenedSub.asObservable();
  currentChildComponent: Type<any> | null = null;
  load(component: Type<any>, params: any): void {
    console.log('sidenav load', component, params);
    this.mainSidenavSub.next({ component, params });
  }

  unloadSidenav(): void {
    this.childOpenedSub.next(false);
    this.mainSidenavSub.next(null);
  }

  closeChildSidenav(): void {
    this.childOpenedSub.next(false);
  }

  loadAndOpenChildView(component: Type<any>, params: any): void {
    // load child view
    if (
      !this.childSidenavSub.getValue() ||
      this.childSidenavSub.getValue().component !== component
    ) {
      this.childSidenavSub.next({ component, params });
    }
    this.childOpenedSub.next(true);
  }

}
