import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  Input,
  OnDestroy,
  Type,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { Subscription } from "rxjs";
import {
  distinctUntilChanged,
  filter,
  map,
  mergeMap,
  switchMap
} from "rxjs/operators";
import { UxService } from "../../ux.service";
import { SidenavService } from "./sidenav-layout.service";

@Component({
  selector: "ux-sidenav-layout",
  templateUrl: "./sidenav-layout.component.html",
  styleUrls: ["./sidenav-layout.component.scss"],
})
export class SidenavLayoutComponent implements OnDestroy, AfterViewInit {
  @Input()
  showMenu = true;
  @Input()
  leftSideOpened = false;
  @Input()
  rightSideOpened = false;
  label: string;
  @ViewChild("leftSide") leftSide;

  @ViewChild("rightSide") rightSide;
  sideNavOpened = false;
  @Input()
  showFooter = true;
  toggle = true;
  title = "";
  topBarLabel = "";
  routeSub: Subscription;
  alwaysOpened = false;
  width = 0;
  isMobile$ = this.breakpointObserver
    .observe([Breakpoints.Small, Breakpoints.Handset])
    .pipe(distinctUntilChanged());
  subscriptions: Subscription[] = [];
  @ViewChild("dynamicComponentContainer", { read: ViewContainerRef })
  viewContainerRef: ViewContainerRef;
  @ViewChild("dynamicChildNavContainer", { read: ViewContainerRef })
  dynamicChildNavContainer: ViewContainerRef;
  childOpened: boolean;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private uxService: UxService,
    private ref: ChangeDetectorRef,
    private breakpointObserver: BreakpointObserver,
    public sidenavService: SidenavService,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {
    // TODO: move to commons/routing-helper service
    this.subscriptions.push(
      this.router.events
        .pipe(
          filter((event) => event instanceof NavigationEnd),
          map(() => this.activatedRoute),
          map((route) => {
            while (route.firstChild) {
              route = route.firstChild;
            }
            return route;
          }),
          mergeMap((route) => route.data)
        )
        .subscribe((data) => {
          this.uxService.setTitle(data.label);
        })
    );

    this.subscriptions.push(
      this.isMobile$.pipe(switchMap(() => this.sidenavService.mainSidenav$)).subscribe((sidenav) => {
        if (sidenav) {
          this.loadSidenav(sidenav.component, sidenav.params);
        }
      })
    );
  }
  ngAfterViewInit(): void {
    this.subscriptions.push(
      this.sidenavService.mainSidenav$.subscribe((mainSideNav) => {
        // load component into slot and open sidenav if opened = true
        if (mainSideNav) {
          this.loadSidenav(mainSideNav.component, mainSideNav.params);
        } else {
          // unload sidenav
          this.unloadSidenav();
        }
      })
    );

    this.subscriptions.push(
      this.sidenavService.childSidenav$.subscribe((childSidenav) => {
        // load component into slot and open sidenav if opened = true
        if (childSidenav) {
          this.loadAndShowChildSidenav(
            childSidenav.component,
            childSidenav.params
          );
        }
      })
    );
  }

  unloadSidenav() {
    this.viewContainerRef.clear();
  }
  
  loadSidenav(component: Type<any>, params: any) {
    const componentFactory =
      this.componentFactoryResolver.resolveComponentFactory(component);
    
    const componentRef =
      this.viewContainerRef.createComponent(componentFactory);
    // copy params
    Object.keys(params).forEach((key) => {
      componentRef.instance[key] = params[key];
    });
  }

  loadAndShowChildSidenav(component: Type<any>, params: any) {
    const componentFactory =
      this.componentFactoryResolver.resolveComponentFactory(component);

    const componentRef =
      this.dynamicChildNavContainer.createComponent(componentFactory);
    // copy params
    Object.keys(params).forEach((key) => {
      componentRef.instance[key] = params[key];
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
