import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
export class UxModuleConfig {
  appName: string;
}
@Injectable({
  providedIn: 'root'
})
export class UxService {
  label: string;
  constructor(
    private titleService: Title,
    private uxModuleConfig: UxModuleConfig
  ) { }

  public setTitle(routeTitle: string) {
    routeTitle && routeTitle.length > 0 ?
      this.titleService.setTitle(`${routeTitle} - ${this.uxModuleConfig.appName}`)
      : this.titleService.setTitle(`${this.uxModuleConfig.appName}`);

  }
}
