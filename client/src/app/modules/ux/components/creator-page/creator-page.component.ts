import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ux-creator-page',
  templateUrl: './creator-page.component.html',
  styleUrls: ['./creator-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreatorPageComponent implements OnInit {
  // @ViewChild('contentWrapper', { read: ElementRef }) content: ElementRef;
  @Input()
  label: string;
  constructor() {}
  ngOnInit() {}
}
