import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'ux-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListComponent {
  @Input()
  subheader: string;

}
