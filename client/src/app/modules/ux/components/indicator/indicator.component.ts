import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';


@Component({
  selector: 'ux-indicator',
  templateUrl: './indicator.component.html',
  styleUrls: ['./indicator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IndicatorComponent implements OnInit {
  @Input()
  color = 'black';
  @Input()
  size = 50;
  constructor() { }
  ngOnInit(): void {}

}
