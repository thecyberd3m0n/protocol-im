import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { flyInOut } from "../../animations/flyInOut";
import { CreatorStepLifecycle } from "../../interfaces/creator-step-lifecycle.interface";
import { Observable } from "rxjs";
import { take } from "rxjs/operators";

@Component({
  selector: "ux-childroute-creator",
  templateUrl: "./creator-childroute.component.html",
  styleUrls: ["./creator-childroute.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [flyInOut],
})
export class CreatorChildRouteComponent implements OnInit {
  navLinks = [];
  @Input()
  finishButtonLabel: Observable<string>;
  @Input()
  nextButtonLabel: Observable<string>;
  @Input()
  previousButtonLabel: Observable<string>;
  @Output()
  finished: EventEmitter<any> = new EventEmitter();
  @Output()
  nextStep: EventEmitter<{ step: number; data: any[] }> = new EventEmitter<{
    step: number;
    data: any[];
  }>();
  currentChild: CreatorStepLifecycle;
  dataAllSteps = [];
  constructor(public activatedRoute: ActivatedRoute, private router: Router) {}
  ngOnInit(): void {
    this.navLinks = this.activatedRoute.snapshot.routeConfig.children
      .filter((child) => child.path !== "**")
      .map((child) => {
        return {
          label: child.data.label,
          path: child.path,
        };
      });
  }

  onActivate($event) {
    this.currentChild = $event;
  }

  hasPreviousRoute() {
    return this.getChildIndex() > 0;
  }

  hasNextRoute() {
    return (
      this.getChildIndex() <
      this.activatedRoute.snapshot.routeConfig.children.length - 2
    );
  }

  goBack() {
    this.currentChild.canGoBack
      .pipe(take(1))
      .subscribe((canGoBack: boolean) => {
        if (canGoBack) {
          this.router.navigate(
            [
              this.activatedRoute.snapshot.routeConfig.children[
                this.getChildIndex() - 1
              ].path,
            ],
            { relativeTo: this.activatedRoute, state: this.dataAllSteps }
          );
        }
      });
  }

  goForward() {
    // get user data by calling getStepData from inner route's component
    this.currentChild.canGoNext
      .pipe(take(1))
      .subscribe((canGoNext: boolean) => {
        if (canGoNext) {
          this.currentChild.onNext().then((data) => {
            if (this.hasNextRoute()) {
              this.dataAllSteps[this.getChildIndex()] = data;
              this.nextStep.emit({
                step: this.getChildIndex(),
                data: this.dataAllSteps,
              });
              this.router.navigate(
                [
                  this.activatedRoute.snapshot.routeConfig.children[
                    this.getChildIndex() + 1
                  ].path,
                ],
                { relativeTo: this.activatedRoute, state: this.dataAllSteps }
              );
            } else {
              this.done();
            }
          });
        }
      });
  }

  done() {
    const data = this.currentChild.getStepData();
    this.dataAllSteps[this.getChildIndex()] = data;
    this.currentChild.onNext();
    this.finished.emit(this.dataAllSteps);
  }

  private getChildIndex() {
    return (
      this.activatedRoute.snapshot.routeConfig.children
        .filter((child) => child.path !== "**")
        .indexOf(
          this.activatedRoute.snapshot.routeConfig.children.find(
            (child) =>
              this.activatedRoute.snapshot.firstChild &&
              child.path ===
                this.activatedRoute.snapshot.firstChild.routeConfig.path
          )
        ) || 0
    );
  }
}
