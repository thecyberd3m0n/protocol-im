import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Menu } from '../../interfaces/menu.interface';

@Component({
  selector: 'ux-bottom-menu',
  templateUrl: './button-menu.component.html',
  styleUrls: ['./button-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonMenuComponent implements OnInit {
  @Input()
  navLinks: Array<Menu>;
  constructor() {
  }

  ngOnInit(): void {
    console.log(this.navLinks);
  }

}
