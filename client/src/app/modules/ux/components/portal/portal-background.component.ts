import { Component } from '@angular/core';

@Component({
  selector: 'ux-portal-background',
  template: `
  <div fxLayout="column" fxFlexFill>
    <ng-content></ng-content>
  </div>
  `
})
export class PortalBackgroundComponent {

}
