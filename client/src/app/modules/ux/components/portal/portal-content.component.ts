import { Component } from '@angular/core';

@Component({
  selector: 'ux-portal-content',
  template: `
  <ng-content></ng-content>
  `
})
export class PortalContentComponent {

}
