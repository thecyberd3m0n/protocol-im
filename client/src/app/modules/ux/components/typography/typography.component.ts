import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'ux-typography',
  templateUrl: './typography.component.html',
  styleUrls: ['./typography.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TypographyComponent {
  @Input()
  variant = 'mat-caption';
  @Input()
  text: string;
  @Input()
  link = false;
  @Input()
  color?;

}
