import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'ux-spinner-view',
  templateUrl: './spinner-view.component.html',
  styleUrls: ['./spinner-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpinnerViewComponent implements OnInit {
  @Input()
  loading: boolean;
  constructor() { }

  ngOnInit(): void {
  }

}
