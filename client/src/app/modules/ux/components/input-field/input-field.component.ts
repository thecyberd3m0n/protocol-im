import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, Input, forwardRef } from '@angular/core';

@Component({
  selector: 'ux-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InputFieldComponent),
    multi: true
  }]
})
export class InputFieldComponent implements ControlValueAccessor {
  constructor() { }
    /**
     * Holds the current value of the slider
     */
  @Input() ngModel;

    /**
     * Invoked when the model has been changed
     */
  // @Output() inputValueChange: EventEmitter<string> = new EventEmitter<string>();
  @Input()
  label: string;
  @Input()
  type: string;
  @Input()
  name: string;
  @Input()
  error: string;
  @Input()
  disabled: boolean;
  @Input()
  value: string;
  onChange = (value) => {};
  onTouched = () => {};
  updateChanges() {
    this.onChange(this.value);
  }
  writeValue(value: any): void {
    this.value = value;
    this.updateChanges();
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

}
