import { TestBed } from '@angular/core/testing';

import { UxStepperService } from './stepper.service';

describe('StepperService', () => {
  let service: UxStepperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UxStepperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
