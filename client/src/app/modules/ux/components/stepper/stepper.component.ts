import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ComponentFactoryResolver, ComponentRef, ContentChildren, Directive, ElementRef, EventEmitter, Injector, Input, OnInit, Output, QueryList, Type, ViewChild, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { CreatorStepLifecycle } from '../../interfaces/creator-step-lifecycle.interface';
import { UxStepperService } from './stepper.service';

@Directive({
  selector: '[uxStep]'
})
export class StepDirective {
  constructor(public component: ElementRef<any>) {}

  canGoBack(): Observable<boolean> {
    return (this.component as ElementRef<CreatorStepLifecycle>).nativeElement.canGoBack;
  }

  canGoNext(): Observable<boolean> {
    return (this.component as ElementRef<CreatorStepLifecycle>).nativeElement.canGoNext;
  }
}

@Component({
  selector: 'ux-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StepperComponent implements OnInit, AfterViewInit {
  @Input()
  finishButtonLabel: Observable<string>;
  @Input()
  nextButtonLabel: Observable<string>;
  @Input()
  previousButtonLabel: Observable<string>;
  @Output()
  finished: EventEmitter<any> = new EventEmitter();
  @Output()
  nextStep: EventEmitter<{ step: number; data: any[] }> = new EventEmitter<{
    step: number;
    data: any[];
  }>();
  currentChild: CreatorStepLifecycle;
  dataAllSteps = [];
  stepsIndex = 0;
  @ContentChildren(StepDirective, { read: StepDirective, descendants: true })
  steps!: QueryList<StepDirective>;
  @ViewChild('container', { read: ViewContainerRef })
  containerRef: ViewContainerRef;
  currentElementRef: ElementRef<StepDirective>;
  currentStep: ComponentRef<CreatorStepLifecycle>;
  @Input()
  componentTypes: Type<CreatorStepLifecycle>;

  constructor(
    public activatedRoute: ActivatedRoute,
    private changeDetectorRef: ChangeDetectorRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector,
    private uxStepperService: UxStepperService
    ) {}
  ngAfterViewInit(): void {
    this.loadStep();
  }

  loadStep() {
    this.containerRef.clear();
    // removing this detectChanges() will break width detection
    this.changeDetectorRef.detectChanges();
    this.currentElementRef = this.steps.toArray()[this.stepsIndex].component.nativeElement;
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory<CreatorStepLifecycle>(this.componentTypes[this.stepsIndex]);
    this.currentStep = this.containerRef.createComponent(componentFactory, null, this.injector);
    this.changeDetectorRef.detectChanges();
  }

  ngOnInit(): void {
    this.uxStepperService.dataSteps = [];
  }

  onActivate($event) {
    this.currentChild = $event;
  }

  hasPreviousRoute() {
    return this.stepsIndex > 0;
  }

  goToStep(i: number) {
    if (this.stepsIndex[i]) {
      this.stepsIndex = i;
      this.loadStep();
    }
  }

  hasNextPage() {
    return (
      this.stepsIndex <
      this.steps.length - 1
    );
  }

  goBack() {
    this.currentStep.instance.canGoBack
      .pipe(take(1))
      .subscribe((canGoBack: boolean) => {
        if (canGoBack) {
          this.stepsIndex--;
          this.loadStep();
        }
      });
  }

  goForward() {
    // get user data by calling getStepData from inner route's component
    this.currentStep.instance.canGoNext
      .pipe(take(1))
      .subscribe((canGoNext: boolean) => {
        if (canGoNext) {
          this.currentStep.instance.onNext().then((data) => {
            if (this.hasNextPage()) {
              this.dataAllSteps[this.stepsIndex] = data;
              this.uxStepperService.dataSteps[this.stepsIndex] = data;
              this.stepsIndex++;
              this.loadStep();
              this.nextStep.emit({
                step: this.stepsIndex,
                data: this.dataAllSteps,
              });

            } else {
              this.done();
            }
          });
        }
      });
  }

  done() {
    const data = this.currentStep.instance.getStepData();
    this.dataAllSteps[this.stepsIndex] = data;
    this.currentStep.instance.onNext();
    this.finished.emit(this.dataAllSteps);
  }

}
