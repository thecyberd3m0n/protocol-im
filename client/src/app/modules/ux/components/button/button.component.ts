import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit
} from '@angular/core';
@Component({
  selector: 'ux-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonComponent implements OnInit {
  @Input()
  label: string;
  @Input()
  icon: string;
  @Input()
  disabled: boolean;
  @Input()
  type: string;
  @Input()
  styleType: string;
  @Input()
  image: string;
  @Input()
  color: string;
  constructor() { }
  ngOnInit() {
  }
}
