import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'ux-preview-example',
  templateUrl: './ux-preview-example.component.html',
  styleUrls: ['./ux-preview-example.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreviewExampleComponent {
  @Input()
  name: string;
  @Input()
  description: string;
}
