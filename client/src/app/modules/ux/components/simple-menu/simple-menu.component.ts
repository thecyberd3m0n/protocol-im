import { Component, Input } from '@angular/core';
import { Menu } from '../../interfaces/menu.interface';
@Component({
  selector: 'ux-simple-menu',
  templateUrl: './simple-menu.component.html',
  styleUrls: ['./simple-menu.component.scss']
})
export class SimpleMenuComponent {
  @Input()
  menu: Array<Menu>;
}
