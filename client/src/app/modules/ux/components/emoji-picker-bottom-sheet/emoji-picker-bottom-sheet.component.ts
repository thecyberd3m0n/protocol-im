import { Component, OnInit } from "@angular/core";
import { MatBottomSheetRef } from "@angular/material/bottom-sheet";

@Component({
  selector: "ux-emoji-picker-bottom-sheet",
  templateUrl: "./emoji-picker-bottom-sheet.component.html",
  styleUrls: ["./emoji-picker-bottom-sheet.component.scss"],
})
export class EmojiPickerBottomSheetComponent implements OnInit {
  constructor(
    private bottomSheetRef: MatBottomSheetRef<EmojiPickerBottomSheetComponent>
  ) {}

  ngOnInit(): void {}

  addEmoji($event) {
    this.bottomSheetRef.dismiss($event);
  }
}
