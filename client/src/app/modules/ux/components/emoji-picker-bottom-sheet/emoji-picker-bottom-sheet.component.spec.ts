import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmojiPickerBottomSheetComponent } from './emoji-picker-bottom-sheet.component';

describe('EmojiPickerBottomSheetComponent', () => {
  let component: EmojiPickerBottomSheetComponent;
  let fixture: ComponentFixture<EmojiPickerBottomSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmojiPickerBottomSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmojiPickerBottomSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
