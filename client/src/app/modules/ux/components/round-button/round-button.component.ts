import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit
} from '@angular/core';
@Component({
  selector: 'ux-round-button',
  templateUrl: './round-button.component.html',
  styleUrls: ['./round-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoundButtonComponent implements OnInit {
  @Input()
  icon: string;
  @Input()
  disabled: boolean;
  @Input()
  type: string;
  constructor() { }
  ngOnInit() { }
}
