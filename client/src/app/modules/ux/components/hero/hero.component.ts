import { Component, Input } from '@angular/core';
import { ColorService } from '../../services/color.service';

@Component({
  selector: 'ux-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent {
  @Input()
  image: string;
  @Input()
  color: string;
  @Input()
  gradient = false;
  opened = false;
  constructor(private colorService: ColorService) { }

  handleOnClick() {
    this.opened = !this.opened;
  }

  getStyle() {
    if (this.image) {
      return { 'background-image': `url(${this.image})` };
    } else if (this.gradient) {
      const rgbColor = this.colorService.normalizeColor(this.color);
      const rgbColorBrighter = this.colorService.addBrightness(rgbColor, 20);

      return {
        background:
          `linear-gradient(132deg, ${this.colorService.getString(rgbColor)} 24%, ${this.colorService.getString(rgbColorBrighter)} 77%, rgba(255,255,255,0.9836309523809523) 100%)`
      };
    } else {
      return { backgroundColor: this.color };
    }
  }
}
