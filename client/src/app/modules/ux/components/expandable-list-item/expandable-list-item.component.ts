import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: 'ux-expandable-list-item',
  templateUrl: './expandable-list-item.component.html',
  styleUrls: ['./expandable-list-item.component.scss']
})
export class ExpandableListItemComponent implements OnInit {
  @Input()
  header: string;
  @Input()
  description: string;
  constructor() { }

  ngOnInit() {
  }

}
