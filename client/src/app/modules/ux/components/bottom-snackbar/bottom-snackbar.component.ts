import { Component, OnInit, ChangeDetectionStrategy, Inject, ChangeDetectorRef } from "@angular/core";
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from "@angular/material/bottom-sheet";

export interface BottomSnackbarAction {
  label: string;
  callback: () => void;
  color: string;
}

export interface BottomSnackbarConfig {
  message: string;
  actions: BottomSnackbarAction[];
}

@Component({
  selector: "ux-bottom-snackbar",
  templateUrl: "./bottom-snackbar.component.html",
  styleUrls: ["./bottom-snackbar.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BottomSnackbarComponent implements OnInit {
  warning: boolean;
  message: string;
  actions: BottomSnackbarAction[] = [];
  constructor(
    private bottomSheetRef: MatBottomSheetRef<BottomSnackbarComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: BottomSnackbarConfig,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.message = this.data && this.data.message;
    this.actions = this.data && this.data.actions;
    this.changeDetectorRef.detectChanges();
  }

  clearBar($event): void {
    this.bottomSheetRef.dismiss({
      message: "Cancel",
      data: this.data,
    });
    $event.preventDefault();
  }

  changeStatus() {
    this.bottomSheetRef.dismiss({
      message: "Status",
      data: this.data,
    });
  }
}
