import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BottomSnackbarComponent } from './bottom-snackbar.component';

describe('BottomSnackbarComponent', () => {
  let component: BottomSnackbarComponent;
  let fixture: ComponentFixture<BottomSnackbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BottomSnackbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomSnackbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
