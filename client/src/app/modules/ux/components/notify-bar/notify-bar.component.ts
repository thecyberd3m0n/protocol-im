import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges } from '@angular/core';

export enum NotifyBarColor {
  PRIMARY, SECONDARY, WARN
} 

export interface NotifyBarAction {
  label: string;
  color: NotifyBarColor;
  action: () => void;
}
@Component({
  selector: 'ux-notify-bar',
  templateUrl: './notify-bar.component.html',
  styleUrls: ['./notify-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('slideInOut', [
      state('true', style({
        height: '*',
        opacity: 1
      })),
      state('false', style({
        height: '0',
        opacity: 0
      })),
      transition('true => false', animate('300ms ease-in')),
      transition('false => true', animate('300ms ease-out'))
    ])
  ]
})
export class NotifyBarComponent implements OnInit, OnChanges {
  @Input()
  enable = true;
  @Input()
  color: NotifyBarColor;
  @Input()
  message = '';
  @Input()
  actions: NotifyBarAction[];
  @Input()
  icon
  warning: false;
  constructor() { }
  ngOnChanges(changes: SimpleChanges): void {
    console.log('notify bar changes', changes);
  }

  ngOnInit(): void {

  }

  actionClicked(action) {
    action();
  }

}
