import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
import {
  MatBottomSheet,
  MatBottomSheetRef
} from "@angular/material/bottom-sheet";
import { Subscription } from "rxjs";
import { EmojiPickerBottomSheetComponent } from "../emoji-picker-bottom-sheet/emoji-picker-bottom-sheet.component";

@Component({
  selector: "ux-emoji-picker",
  templateUrl: "./emoji-picker.component.html",
  styleUrls: ["./emoji-picker.component.scss"],
})
export class EmojiPickerComponent implements OnInit, OnDestroy {
  @Output()
  emoji = new EventEmitter();
  private bottomSheetRef =
    {} as MatBottomSheetRef<EmojiPickerBottomSheetComponent>;
  private bottomSheetSubscriber = new Subscription();
  constructor(private bottomSheet: MatBottomSheet) {}

  ngOnInit(): void {}

  openPicker() {
    this.bottomSheetRef = this.bottomSheet.open(
      EmojiPickerBottomSheetComponent,
      { panelClass: "emoji-sheet" }
    );
    this.bottomSheetSubscriber = this.bottomSheetRef
      .afterDismissed()
      .subscribe((emoji) => this.emoji.emit(emoji));
  }

  ngOnDestroy(): void {
    this.bottomSheetSubscriber.unsubscribe();
  }
}
