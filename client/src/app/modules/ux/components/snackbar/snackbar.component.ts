import { Component, Input, OnInit } from '@angular/core'
import { MatSnackBarRef } from '@angular/material/snack-bar'

export enum SnackbarState {
  Success, Neutral, Info, Warning, Error
}

@Component({
  selector: 'ux-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss'],
})
export class SnackbarComponent implements OnInit {
  SnackbarState = SnackbarState;
  @Input()
  snackbarState: SnackbarState = SnackbarState.Neutral;
  @Input()
  message: string
  action: string
  snackBarRef: MatSnackBarRef<any>
  constructor() {}

  ngOnInit(): void {}

  close() {

  }
}
