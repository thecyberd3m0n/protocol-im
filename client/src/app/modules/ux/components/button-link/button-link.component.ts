import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter
} from '@angular/core';
@Component({
  selector: 'ux-button-link',
  templateUrl: './button-link.component.html',
  styleUrls: ['./button-link.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonLinkComponent implements OnInit {
  @Input()
  value: string;
  @Input()
  buttonLabel: string;
  @Output()
  clicked = new EventEmitter();
  constructor() { }
  ngOnInit() { }
}
