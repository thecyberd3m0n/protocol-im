import { ChangeDetectionStrategy, Component, ContentChildren, Input, QueryList } from '@angular/core';
import { PreviewExampleComponent } from '../preview-example/ux-preview-example.component';

@Component({
  selector: 'ux-preview-wrapper',
  templateUrl: './ux-preview-wrapper.component.html',
  styleUrls: ['./ux-preview-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreviewWrapperComponent {
  @Input()
  name: string;
  @Input()
  description: string;
  constructor() { }
  @ContentChildren(PreviewExampleComponent, { descendants: true }) examples: QueryList<PreviewExampleComponent>;
}
