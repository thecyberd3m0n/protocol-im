import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject,
} from "@angular/core";
import {
  MAT_BOTTOM_SHEET_DATA,
  MatBottomSheetRef,
} from "@angular/material/bottom-sheet";
@Component({
  selector: "ux-message-context-menu",
  templateUrl: "./message-context-menu.component.html",
  styleUrls: ["./message-context-menu.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessageContextMenuComponent implements OnInit {
  reactions = ["👍", "👎", "😀", "😘", "😍", "😆", "😜", "😅", "😂", "😱"];

  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: { messageId: string },
    private bottomSheetRef: MatBottomSheetRef<MessageContextMenuComponent>
  ) {}

  ngOnInit(): void {}
  reactionClick(reaction: string) {
    this.bottomSheetRef.dismiss({
      messageId: this.data.messageId,
      emoji: reaction,
    });
  }
  editClick() {
    throw new Error("Method not implemented.");
  }
  responseClick() {
    throw new Error("Method not implemented.");
  }
  deleteClick() {
    throw new Error("Method not implemented.");
  }
  returnClick() {
    this.bottomSheetRef.dismiss();
  }
}
