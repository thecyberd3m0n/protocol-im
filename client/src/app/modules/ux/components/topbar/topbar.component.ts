import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
  Directive,
  ContentChild
} from '@angular/core';
import { UxService } from '../../ux.service';
@Directive({ selector: '[uxTopbarLeftComponent]' })
export class TopbarLeftComponentDirective {
  constructor(public template: TemplateRef<any>) { }
}

@Directive({ selector: '[uxTopbarRightComponent]' })
export class TopbarRightComponentDirective {
  constructor(public template: TemplateRef<any>) { }
}

@Component({
  selector: 'ux-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopbarComponent implements OnInit {
  @Input()
  label: string;
  @Output()
  rightIconClick = new EventEmitter<boolean>();
  @ContentChild(TopbarLeftComponentDirective)
  leftComponent?: TopbarLeftComponentDirective;
  @ContentChild(TopbarRightComponentDirective)
  rightComponent?: TopbarRightComponentDirective;
  constructor(private uxService: UxService) { }
  ngOnInit(): void {
    if (!this.label || this.label.length === 0) {
      this.label = this.uxService.label;
    }
  }
}
