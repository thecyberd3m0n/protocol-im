import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ux-sidenav-button',
  templateUrl: './sidenav-button.component.html',
  styleUrls: ['./sidenav-button.component.scss']
})
export class SidenavButtonComponent implements OnInit {
  @Input()
  label: string;
  @Input()
  icon: string;
  @Input()
  disabled: boolean;
  @Input()
  type: string;
  @Input()
  styleType: string;
  @Input()
  image: string;
  @Input()
  color: string;
  constructor() { }

  ngOnInit(): void {
  }

}
