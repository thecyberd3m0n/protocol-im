import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ux-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent implements OnInit{
  @Input()
  label: string;
  @Input()
  small: true;
  @Input()
  edit: false;
  color: string;
  ngOnInit() {
    const colors = ['yellow', 'green', 'blue', 'pink', 'purple'];
    this.color = colors[Math.floor(Math.random() * colors.length)];

    }
}
