import { Pipe, PipeTransform } from "@angular/core";
import {
  EmojiData, EmojiService
} from "@ctrl/ngx-emoji-mart/ngx-emoji";

@Pipe({
  name: "emoji",
})
export class EmojiPipe implements PipeTransform {
  constructor(private emojiService: EmojiService) {}
  transform(value: string, ...args: unknown[]): string {
    if (!value) return "";
    return this.replaceColons(value);
  }
  private findEmoji(name: string) {
    return this.emojiService.emojis.find((emoji) => emoji.shortName === name);
  }
  private replaceColons(text: string) {
    const colonsRegex: RegExp = new RegExp("(:(?![\\n])[()#$@-\\w]+:)", "g");
    const matches: string[] = text.match(colonsRegex);
    if (matches) {
      for (const match of matches) {
        const emoji: EmojiData = this.findEmoji(match.slice(1, -1));
        text = text.replace(match, emoji ? emoji.native : match);
      }
    }
    return text;
  }
}
