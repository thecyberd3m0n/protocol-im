import { animate, style, transition, trigger } from '@angular/animations';

export const flyInOut =
  trigger('flyInOut', [
    transition('* => *', [
      style({ transform: 'translateX(100%)' }),
      animate('300ms ease-out')
    ]),
  ]);
