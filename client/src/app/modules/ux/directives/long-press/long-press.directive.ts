import {
  AnimationBuilder,
  AnimationMetadata,
  AnimationPlayer,
  animate,
  style,
} from "@angular/animations";
import { unsupported } from "@angular/compiler/src/render3/view/util";
import {
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from "@angular/core";
import { Subject, Subscription, fromEvent, merge, of, pipe, timer } from "rxjs";
import {
  distinctUntilChanged,
  filter,
  map,
  switchMap,
  takeUntil,
  tap,
} from "rxjs/operators";

@Directive({
  selector: "[uxLongPress]",
  host: { ["style.cursor"]: "'pointer'" },
})
export class LongPressDirective implements OnDestroy, OnInit {
  private destroy$: Subject<boolean> = new Subject<boolean>();
  private player: AnimationPlayer;
  @Input() selectAnimation: any;
  @Input()
  pressTimeMs? = 5000;
  @Output()
  longPress = new EventEmitter();

  constructor(
    private builder: AnimationBuilder,
    private elementRef: ElementRef
  ) {}
  ngOnInit(): void {
    if (!this.selectAnimation)
      this.selectAnimation = this.defaultAnimation(this.pressTimeMs);
    const mouseDown = fromEvent<MouseEvent>(
      this.elementRef.nativeElement,
      "mousedown"
    ).pipe(
      filter((event) => event.button == 0),
      map((event) => true),
      tap((x) => this.startAnimation())
    );
    const touchStart = fromEvent(
      this.elementRef.nativeElement,
      "touchstart"
    ).pipe(
      map(() => true),
      tap((x) => this.startAnimation())
    );
    const mouseUp = fromEvent<MouseEvent>(window, "mouseup").pipe(
      filter((event) => event.button == 0),
      map(() => false)
    );
    const touchEnd = fromEvent(this.elementRef.nativeElement, "touchend").pipe(
      map(() => false)
    );
    merge(mouseDown, mouseUp, touchStart, touchEnd)
      .pipe(
        switchMap((state) => (state ? timer(this.pressTimeMs) : of(null))),
        filter((x) => x === 0),
        takeUntil(this.destroy$)
      )
      .subscribe(() => this.longPress.emit());
  }
  startAnimation() {
    if (this.player) {
      this.player.destroy();
    }
    const factory = this.builder.build(this.selectAnimation);
    const player = factory.create(this.elementRef.nativeElement);

    player.onDone(() => {
      player.reset();
    });
    player.play();
  }
  defaultAnimation(duration: number): AnimationMetadata[] {
    return [
      style({ border: "rgba(255, 152, 0, 0) 0px solid" }),
      animate(
        duration.toString() + "ms ease",
        style({ border: "rgba(255, 152, 0, 1) 2px solid" })
      ),
    ];
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }
}
