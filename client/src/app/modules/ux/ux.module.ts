import { CommonModule } from "@angular/common";
import { ModuleWithProviders, NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatCardModule } from "@angular/material/card";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatListModule } from "@angular/material/list";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatStepperModule } from "@angular/material/stepper";
import { MatTabsModule } from "@angular/material/tabs";
import { MatToolbarModule } from "@angular/material/toolbar";
import { RouterModule } from "@angular/router";
import { AvatarComponent } from "./components/avatar/avatar.component";
import { ButtonLinkComponent } from "./components/button-link/button-link.component";
import { ButtonMenuComponent } from "./components/button-menu/button-menu.component";
import { ButtonComponent } from "./components/button/button.component";
import { CreatorChildRouteComponent } from "./components/creator-childroute/creator-childroute.component";
import { CreatorPageComponent } from "./components/creator-page/creator-page.component";
import { ExpandableListItemComponent } from "./components/expandable-list-item/expandable-list-item.component";
import { ExpandableListComponent } from "./components/expandable-list/expandable-list.component";
import { HeroComponent } from "./components/hero/hero.component";
import { InputFieldComponent } from "./components/input-field/input-field.component";
import { ListItemComponent } from "./components/list-item/list-item.component";
import { ListComponent } from "./components/list/list.component";
import { PortalBackgroundComponent } from "./components/portal/portal-background.component";
import { PortalContentComponent } from "./components/portal/portal-content.component";
import { PortalComponent } from "./components/portal/portal.component";
import { PreviewExampleComponent } from "./components/preview-example/ux-preview-example.component";
import { PreviewWrapperComponent } from "./components/preview-wrapper/ux-preview-wrapper.component";
import { RoundButtonComponent } from "./components/round-button/round-button.component";
import { SimpleMenuComponent } from "./components/simple-menu/simple-menu.component";
import {
  StepDirective,
  StepperComponent,
} from "./components/stepper/stepper.component";
import { TabGroupComponent } from "./components/tab-group/tab-group.component";
import { TabComponent } from "./components/tab/tab.component";
import {
  TopbarComponent,
  TopbarLeftComponentDirective,
  TopbarRightComponentDirective,
} from "./components/topbar/topbar.component";
import { TypographyComponent } from "./components/typography/typography.component";
import { BottomMenuLayoutComponent } from "./layouts/bottom-menu-layout/bottom-menu-layout.component";
import { CreatorLayoutComponent } from "./layouts/creator-layout/creator-layout.component";
import { NavTabsLayoutComponent } from "./layouts/nav-tabs-layout/nav-tabs-layout.component";
import { SidenavLayoutComponent } from "./layouts/sidenav-layout/sidenav-layout.component";
import {
  MenuDirective,
  SidenavMenuLayoutComponent,
} from "./layouts/sidenav-menu-layout/sidenav-menu-layout.component";
import {
  SimpleLayoutComponent,
  SimpleLayoutContentComponent,
  SimpleLayoutFooterComponent,
  SimpleLayoutTopComponent,
} from "./layouts/simple-layout/simple-layout.component";
import { UxModuleConfig } from "./ux.service";
import { MatBottomSheetModule } from "@angular/material/bottom-sheet";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatTooltipModule } from "@angular/material/tooltip";
import { PickerModule } from "@ctrl/ngx-emoji-mart";
import { BottomSnackbarComponent } from "./components/bottom-snackbar/bottom-snackbar.component";
import { EmojiPickerBottomSheetComponent } from "./components/emoji-picker-bottom-sheet/emoji-picker-bottom-sheet.component";
import { EmojiPickerComponent } from "./components/emoji-picker/emoji-picker.component";
import { IndicatorComponent } from "./components/indicator/indicator.component";
import { LogoComponent } from "./components/logo/logo.component";
import { MessageContextMenuComponent } from "./components/message-context-menu/message-context-menu/message-context-menu.component";
import { SidenavButtonComponent } from "./components/sidenav-button/sidenav-button/sidenav-button.component";
import { SpinnerViewComponent } from "./components/spinner-view/spinner-view.component";
import { LongPressDirective } from "./directives/long-press/long-press.directive";
import { SidenavService } from "./layouts/sidenav-layout/sidenav-layout.service";
import { EmojiPipe } from "./pipes/emoji.pipe";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { SnackbarComponent } from "./components/snackbar/snackbar.component";
import { NotifyBarComponent } from "./components/notify-bar/notify-bar.component";

@NgModule({
  declarations: [
    ButtonComponent,
    ButtonLinkComponent,
    RoundButtonComponent,
    ExpandableListComponent,
    ExpandableListItemComponent,
    HeroComponent,
    InputFieldComponent,
    ListComponent,
    ListItemComponent,
    PreviewWrapperComponent,
    PreviewExampleComponent,
    TopbarComponent,
    TypographyComponent,
    SidenavMenuLayoutComponent,
    SidenavLayoutComponent,
    BottomMenuLayoutComponent,
    SimpleMenuComponent,
    MenuDirective,
    NavTabsLayoutComponent,
    TabComponent,
    TabGroupComponent,
    CreatorPageComponent,
    PortalComponent,
    PortalBackgroundComponent,
    PortalContentComponent,
    AvatarComponent,
    TopbarLeftComponentDirective,
    SimpleLayoutComponent,
    SimpleLayoutTopComponent,
    SimpleLayoutContentComponent,
    SimpleLayoutFooterComponent,
    TopbarRightComponentDirective,
    ButtonMenuComponent,
    CreatorChildRouteComponent,
    CreatorLayoutComponent,
    StepperComponent,
    StepDirective,
    IndicatorComponent,
    SpinnerViewComponent,
    BottomSnackbarComponent,
    EmojiPickerComponent,
    EmojiPickerBottomSheetComponent,
    EmojiPipe,
    LongPressDirective,
    MessageContextMenuComponent,
    LogoComponent,
    SidenavButtonComponent,
    SnackbarComponent,
    NotifyBarComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    FlexLayoutModule,
    MatListModule,
    MatTabsModule,
    RouterModule.forChild([]),
    MatStepperModule,
    MatButtonToggleModule,
    MatBottomSheetModule,
    PickerModule,
    MatSnackBarModule,
  ],
  exports: [
    ButtonComponent,
    ButtonLinkComponent,
    RoundButtonComponent,
    ExpandableListComponent,
    ExpandableListItemComponent,
    HeroComponent,
    InputFieldComponent,
    ListComponent,
    ListItemComponent,
    PreviewWrapperComponent,
    PreviewExampleComponent,
    TopbarComponent,
    TypographyComponent,
    SidenavMenuLayoutComponent,
    SidenavLayoutComponent,
    BottomMenuLayoutComponent,
    SimpleMenuComponent,
    MenuDirective,
    NavTabsLayoutComponent,
    TabComponent,
    TabGroupComponent,
    CreatorPageComponent,
    PortalComponent,
    PortalBackgroundComponent,
    PortalContentComponent,
    AvatarComponent,
    TopbarLeftComponentDirective,
    SimpleLayoutComponent,
    SimpleLayoutTopComponent,
    SimpleLayoutContentComponent,
    SimpleLayoutFooterComponent,
    TopbarRightComponentDirective,
    ButtonMenuComponent,
    CreatorChildRouteComponent,
    CreatorLayoutComponent,
    StepperComponent,
    StepDirective,
    IndicatorComponent,
    SpinnerViewComponent,
    EmojiPickerComponent,
    EmojiPipe,
    LongPressDirective,
    MessageContextMenuComponent,
    LogoComponent,
    SidenavButtonComponent,
    BottomSnackbarComponent,
    NotifyBarComponent
  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class UxModule {
  static forRoot(config: UxModuleConfig): ModuleWithProviders<UxModule> {
    return {
      ngModule: UxModule,
      providers: [
        {
          provide: UxModuleConfig,
          useValue: config,
        },
        SidenavService,
      ],
    };
  }
}
