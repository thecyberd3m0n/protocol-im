import { ModuleWithProviders, NgModule } from '@angular/core';
import { AngularFireModule, FirebaseOptions } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { EffectsModule } from '@ngrx/effects';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import 'firebase/database';
import { ArrayBufferUtilsService } from './services/arrayBufferUtils.service';
import { MasterSessionService } from './services/master-session.service';
import { CloudDatabaseService } from './services/cloud-database.service';
import { SecureChatDispatchers } from './services/secure-chat.dispatchers';
import { SecureChatSelectors } from './services/secure-chat.selectors';
import { SimpleCryptoService } from './services/simple-crypto.service';
import { SecureChatEffects } from './store/effects/secure-chat.effects';
import {
  reducer as secureChatReducer,
  SecureChatModuleState,
} from './store/reducers/secure-chat.reducer';
import {
  reducer as secureSyncReducer,
} from './store/reducers/secure-sync.reducer';
import {
  reducer as notificationsReducer,
} from './store/reducers/notifications.reducer';
import { OlmGroupService } from './services/olm-group.service';
import { SyncEffects } from './store/effects/secure-sync.effects';
import { SecureSyncDispatchers } from './services/secure-sync.dispatchers';
import { SecureSyncSelectors } from './services/secure-sync.selectors';
import { LocalDatabaseService } from './services/local-database.service';
import { NonceService } from './services/nonce.service';
import { CloudMessagingService } from './services/cloud-messaging.service';
import { NotificationsDispatchers } from './services/notifications.dispatchers';
import { NotificationsEffects } from './store/effects/notifications.effects';
import { NotificationsSelectors } from './services/notifications-selectors.service';

export class SecureChatModuleConfig {
  firebase: FirebaseOptions;
}

const reducers: ActionReducerMap<SecureChatModuleState> = {
  secureChatState: secureChatReducer,
  secureSyncState: secureSyncReducer,
  notificationsState: notificationsReducer
};
@NgModule({
  declarations: [],
  imports: [
    AngularFireModule,
    AngularFireDatabaseModule,
    StoreModule.forFeature('secure-chat', reducers),
    EffectsModule.forFeature([SecureChatEffects, SyncEffects, NotificationsEffects]),
  ],
  providers: [
    ArrayBufferUtilsService,
    SimpleCryptoService,
    SecureChatDispatchers,
    SecureSyncDispatchers,
    SecureChatSelectors,
    SecureSyncSelectors,
    CloudDatabaseService,
    MasterSessionService,
    CloudMessagingService,
    OlmGroupService,
    LocalDatabaseService,
    NonceService,
    NotificationsDispatchers,
    NotificationsSelectors
  ]
})
export class SecureChatModule {
  static forRoot(
    config: SecureChatModuleConfig
  ): ModuleWithProviders<SecureChatModule> {
    return {
      ngModule: SecureChatModule,
      providers: [
        {
          provide: SecureChatModuleConfig,
          useValue: config,
        },
        // { provide: FirebaseOptionsToken, useValue: config.firebase }
      ],
    };
  }
}
