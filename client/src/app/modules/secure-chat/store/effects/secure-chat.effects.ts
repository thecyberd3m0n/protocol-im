import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { forkJoin, from, noop, of, throwError } from "rxjs";
import {
  catchError,
  filter,
  map,
  mergeMap,
  switchMap,
  tap,
  withLatestFrom,
} from "rxjs/operators";
import { DecryptedOlmEnvelope } from "../../models/decryptedOlmEnvelope.model";
import { SerializedInviteSession } from "../../models/inviteSession.model";
import { InviteSessionInitMessage } from "../../models/inviteSessionInitMessage.model";
import { SerializedMasterSession } from "../../models/masterSession.model";
import { OutgoingMessageModel } from "../../models/outgoingMessage.model";
import { SessionTicket } from "../../models/sessionInvite.model";
import { CloudDatabaseService } from "../../services/cloud-database.service";
import { FingerprintService } from "../../services/fingerprint.service";
import { MasterSessionService } from "../../services/master-session.service";
import { OlmGroupService } from "../../services/olm-group.service";
import { SimpleCryptoService } from "../../services/simple-crypto.service";
import * as secureChatActions from "../actions/secure-chat.actions";
import { SessionConfig, SessionRequest } from "../reducers/secure-chat.reducer";
import { SerializedActiveSession } from "./../../models/activeSession.model";
import { SecureChatSelectors } from "./../../services/secure-chat.selectors";
import {
  AwaitSessionApproval,
  JoinedSession,
  OutgoingMessage,
  SetupProfile,
} from "./../actions/secure-chat.actions";
import { NonceService } from "../../services/nonce.service";
import { UnsignedProfile } from "../../models/unsignedProfile.model";
@Injectable()
export class SecureChatEffects {
  @Effect()
  init$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.Init),
    switchMap(() => {
      return from(this.masterSessionService.sessionExists()).pipe(
        switchMap(async (exists) => {
          if (exists) {
            return new secureChatActions.AwaitingUnlockMasterSession();
          } else {
            await this.masterSessionService.initAsGuest();

            return new secureChatActions.GuestSession();
          }
        })
      );
    })
  );

  @Effect()
  setupProfile$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.SetupProfile),
    map((action: SetupProfile) => action.payload),
    switchMap((profile) =>
      from(
        this.masterSessionService.createAndLoadMasterSession(profile.passphrase)
      )
    ),
    map(() => {
      return new secureChatActions.Ready();
    })
  );

  // @Effect()
  // backupMasterSession$ = this.actions$.pipe(
  //   ofType(secureChatActions.SecureChatActionTypes.BackupMasterSession),
  //   map((action: secureChatActions.BackupMasterSession) => action.payload),
  //   switchMap(({ passphrase, backupId }) => {
  //     // get encrypted and serialized master session
  //     return from(
  //       this.masterSessionService
  //         .getEncryptedMasterSession(passphrase)
  //         .then((backup) => ({ backup, backupId, passphrase }))
  //     );
  //   }),
  //   switchMap(
  //     ({
  //       backup,
  //       backupId,
  //       passphrase,
  //     }: {
  //       backup: { ciphertext: Uint8Array; nonce: Uint8Array };
  //       backupId: string;
  //       passphrase: string;
  //     }) => {
  //       if (!backupId) {
  //         backupId = RandomService.randomString(16);
  //       }
  //       return from(
  //         this.cloudDatabaseService
  //           .createBackup(backupId, backup.ciphertext)
  //           .then(() => {
  //             return { backupId, nonce: backup.nonce, passphrase };
  //           })
  //       );
  //     }
  //   ),
  //   map(({ backupId, nonce, passphrase }) => {
  //     return new secureChatActions.BackupMasterSessionSuccess({
  //       backupId,
  //       nonce: ArrayBufferUtilsService.arraybufferToString(nonce),
  //       passphrase,
  //     });
  //   })
  // );

  // stores metadata to MasterSession. It should be in database
  @Effect({ dispatch: false })
  updateMasterSessionMeta$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.UpdateMasterSessionMeta),
    map((action: secureChatActions.UpdateMasterSessionMeta) => action.payload),
    switchMap(({ type, meta }) => {
      // assign meta to type
      const fullMeta = {};
      fullMeta[type] = meta;
      console.log("storing meta", fullMeta);
      return from(this.masterSessionService.updateMasterSessionMeta(fullMeta));
    })

    // update master session backup
  );

  @Effect({ dispatch: false })
  updateActiveSessionMeta$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.UpdateActiveSessionMeta),
    map((action: secureChatActions.UpdateActiveSessionMeta) => action.payload),
    switchMap(({ sessionId, type, meta }) => {
      return this.secureChatSelectors.getActiveSession(sessionId).pipe(
        map((activeSession) => ({
          activeSession,
          type,
          meta,
        }))
      );
    }),
    switchMap(({ activeSession }) => {
      // assign meta to type
      return from(
        this.masterSessionService.storeActiveSessionInDB(activeSession)
      );
    })
  );

  @Effect()
  startSession$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.StartSession),
    map((action: secureChatActions.StartSession) => action.payload),
    switchMap((payload: SessionConfig) =>
      from(
        this.masterSessionService.startNewSession(
          payload.manualApprove,
          payload.groupMode,
          payload.oneTime
        )
      )
    ),
    tap(
      ({
        inviteSession,
        preKeyBundle,
      }: {
        inviteSession: SerializedInviteSession;
        preKeyBundle: ArrayBuffer;
      }) => {
        return this.cloudDatabaseService.uploadSessionPrekey(
          inviteSession.id,
          preKeyBundle
        );
      }
    ),
    map(
      ({ inviteSession }: { inviteSession: SerializedInviteSession }) =>
        new secureChatActions.StartedSession(inviteSession)
    )
  );

  @Effect({ dispatch: false })
  updateMasterSessionBackup$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.UpdateMasterSessionBackup),
    map(
      (action: secureChatActions.UpdateMasterSessionBackup) => action.payload
    ),
    withLatestFrom(this.secureChatSelectors.getSyncData()),
    switchMap((backup) => {
      // get encrypted and serialized master session
      console.log("update master session backup", backup);
      return from(
        this.masterSessionService
          .getEncryptedMasterSession(backup[1].passphrase, backup[1].nonce)
          .then((ems) => {
            return {
              backup: ems,
              backupId: backup[1].id,
              passphrase: backup[1].passphrase,
            };
          })
      );
    }),
    switchMap(
      ({
        backup,
        backupId,
        passphrase,
      }: {
        backup: { ciphertext: Uint8Array; salt: Uint8Array };
        backupId: string;
        passphrase: string;
      }) => {
        return from(
          this.cloudDatabaseService
            .createBackup(backupId, backup.ciphertext)
            .then(() => {
              return { backupId, nonce: backup.salt, passphrase };
            })
        );
      }
    )
  );

  @Effect()
  startedSession$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.StartedSession),
    map((x: secureChatActions.StartedSession) => x.payload),
    // TODO: investigate why each new action it loses previous listeners
    mergeMap((inviteSession: SerializedInviteSession) => {
      return this.cloudDatabaseService
        .listenSessionForPreKeyMessage$(inviteSession.id)
        .pipe(
          map(({ id, prekeymessage }) => {
            return { id, prekeymessage, inviteSession: inviteSession };
          })
        );
    }),
    switchMap(
      ({
        prekeymessage,
        inviteSession,
      }: {
        prekeymessage: ArrayBuffer;
        inviteSession: SerializedInviteSession;
      }) => {
        // save peer's pubkey to pendingSession
        return from(
          this.masterSessionService
            .peerConnected(inviteSession, prekeymessage)
            .then(({ inviteSession, initMessage }) => {
              return { id: inviteSession.id, inviteSession, initMessage };
            })
        );
      }
    ),
    map(({ initMessage, inviteSession }) => {
      // Alice's phone shows Bobs initMessage and ask her if accept him. Bob should add his fingerprint to that initMessage for sure
      return inviteSession.manualApprove
        ? new secureChatActions.ShouldApproveSession({
            initData: JSON.stringify(initMessage),
            pendingSessionId: inviteSession.id,
          })
        : new secureChatActions.SessionApprovedAction({
            initData: null,
            pendingSessionId: inviteSession.id,
          });
    })
  );
  // TODO: what about initData? they should be passed over peerConnected
  @Effect()
  sessionApproved$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.SessionApprovedAction),
    map((action: secureChatActions.SessionApprovedAction) => action.payload),
    // Alice approved Bob. She can create olmGroup and she is the rightful owner of it
    switchMap(({ pendingSessionId, initData }) => {
      return from(
        this.masterSessionService.approveSession(
          pendingSessionId,
          initData ? JSON.parse(initData) : null
        )
      );
    }),
    switchMap(
      ({
        e2eInviteMessage,
        inviteSession,
        activeSession,
        newPrekey,
        initData,
      }: {
        e2eInviteMessage: any;
        inviteSession: SerializedInviteSession;
        activeSession: SerializedActiveSession;
        newPrekey: ArrayBuffer;
        initData;
      }) => {
        return from(
          this.simpleCryptoService.randomId().then((id) => ({
            e2eInviteMessage,
            inviteSession,
            activeSession,
            newPrekey,
            initData,
            id,
          }))
        );
      }
    ),
    switchMap(
      ({
        e2eInviteMessage,
        inviteSession,
        activeSession,
        newPrekey,
        initData,
        id,
      }: {
        e2eInviteMessage: any;
        inviteSession: SerializedInviteSession;
        activeSession: SerializedActiveSession;
        newPrekey: ArrayBuffer;
        initData;
        id: string;
      }) => {
        return from(
          this.cloudDatabaseService
            .sendE2eMessage(inviteSession.id, e2eInviteMessage.serialise(), id)
            .then(() => {
              return {
                inviteSession,
                activeSession,
                newPrekey,
                e2eInviteMessage,
              };
            })
        );
      }
    ),
    tap(
      ({
        activeSession,
        inviteSession,
        newPrekey,
        e2eInviteMessage,
        initData,
      }: {
        activeSession: SerializedActiveSession;
        inviteSession: SerializedInviteSession;
        newPrekey: ArrayBuffer;
        e2eInviteMessage: any;
        initData: InviteSessionInitMessage;
      }) => {
        // drop new prekeymessage to inviteSession
        return from(
          this.cloudDatabaseService
            .uploadSessionPreKeyMessage(inviteSession.id, newPrekey)
            .then(() => ({
              activeSession,
              inviteSession,
              newPrekey,
              e2eInviteMessage,
              initData,
            }))
        );
      }
    ),
    switchMap(({ activeSession, initData, inviteSession }) => {
      // find session request
      return this.secureChatSelectors
        .getSessionRequestByInviteSessionId(inviteSession.id)
        .pipe(
          map((sessionRequest) => ({
            activeSession,
            initData,
            sessionRequest,
            inviteSession,
          }))
        );
    }),
    map(
      ({
        activeSession,
        initData,
        sessionRequest,
        inviteSession,
      }: {
        activeSession: SerializedActiveSession;
        initData: any;
        sessionRequest: SessionRequest;
        inviteSession: SerializedInviteSession;
      }) => {
        return new secureChatActions.PeerConnected({
          activeSession,
          inviteSession,
          storeInDb: true,
          ignoreSync: false,
          initData,
          requestInitData: sessionRequest
            ? sessionRequest.requestInitData
            : null,
        });
      }
    )
  );

  @Effect()
  importActiveSession$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.ImportActiveSession),
    map((action: secureChatActions.ImportActiveSession) => action.payload),
    // import active session
    tap(({ serializedActiveSession, unpickleNonce: nonce, initData }) => {
      this.masterSessionService.importActiveSession(
        serializedActiveSession,
        nonce
      );
    }),
    map(
      ({ serializedActiveSession, unpickleNonce: nonce, initData }) =>
        new secureChatActions.PeerConnected({
          activeSession: serializedActiveSession,
          initData: initData,
          ignoreSync: true,
          storeInDb: true,
          inviteSession: null,
        })
    )
  );

  @Effect()
  peerConnected$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.PeerConnected),
    map((action: secureChatActions.PeerConnected) => action.payload),
    // TODO: check if activeSession.id is defined here
    tap(({ activeSession, storeInDb }) => {
      storeInDb ??
        this.masterSessionService.storeActiveSessionInDB(activeSession);
    }),
    switchMap(({ activeSession, storeInDb }) => {
      return this.masterSessionService
        .getMessagesForSession(activeSession.id)
        .then(({ messages, count }) => {
          return {
            dbMessages: messages,
            activeSession,
          };
        });
    }),
    // tap(([{ dbMessages, activeSession }, backupData]) => {
    //   return new secureChatActions.UpdateMasterSessionBackup(backupData);
    // }),
    mergeMap(({ dbMessages, activeSession }) => {
      // Listen for new messages and invoke IncomingMessage
      // TODO: activeSession is defined?
      return this.cloudDatabaseService
        .listenSessionForOlmMessages$(activeSession.id)
        .pipe(
          switchMap(
            (onlineEncryptedMessages: {
              [x: string]: { olmSid: string; message: string };
            }) => {
              const messageIds = Object.keys(onlineEncryptedMessages).filter(
                (x) => {
                  return !dbMessages.find((dbMsg) => dbMsg.id === x);
                }
              );
              const decryptPromises = messageIds.map((mid) => {
                const message = onlineEncryptedMessages[mid];
                return this.olmGroupService.decryptOlmMessage(
                  activeSession.id,
                  message.message,
                  message.olmSid
                );
              });
              return Promise.all(decryptPromises)
                .then((onlineDecryptedMessages: DecryptedOlmEnvelope[]) => {
                  return onlineDecryptedMessages.map((message) => {
                    message.sessionId = activeSession.id;
                    return message;
                  });
                })
                .then((onlineDecryptedMessages: DecryptedOlmEnvelope[]) => {
                  return dbMessages.concat(onlineDecryptedMessages);
                });
            }
          )
        );
    }),
    switchMap((messages: DecryptedOlmEnvelope[]) => {
      const promises = messages.map((msg) =>
        this.masterSessionService.storeMessage(msg.id, msg)
      );
      return from(Promise.all(promises).then(() => messages));
    }),
    switchMap((messages: DecryptedOlmEnvelope[]) => {
      return messages.map((msg) => new secureChatActions.IncomingMessage(msg));
    })
  );

  @Effect()
  applyForSession$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.ApplyForSession),
    map((action: secureChatActions.ApplyForSession) => action.payload),
    // joining session is executed by publishing your prekeymessage and including welcomeData.
    switchMap((value: { welcomeData; sessionId }) => {
      if (!value.sessionId) {
        throw new Error("sessionId is undefined");
      }
      // get session from Firebase
      return forkJoin([
        from(this.cloudDatabaseService.downloadSessionPreKey(value.sessionId)),
        of(value),
      ]);
    }),
    // REF:START move to masterSessionService: applyToSession(sessionPrekey: ArrayBuffer): InviteSession

    switchMap(
      ([preKeyBundle, value]: [ArrayBuffer, { welcomeData; sessionId }]) => {
        return this.masterSessionService.applyToSession(
          value.sessionId,
          preKeyBundle,
          value.welcomeData
        );
      }
    ),
    switchMap(({ inviteSession, envelope }) => {
      return from(
        this.simpleCryptoService.randomId().then((id) => {
          return {
            inviteSession,
            envelope,
            id,
          };
        })
      );
    }),
    // REF:END - now send inviteE2eEnvelope
    switchMap(({ inviteSession, envelope, id }) => {
      return from(
        this.cloudDatabaseService
          .sendE2eMessage(inviteSession.id, envelope.serialise(), id)
          .then(() => ({
            inviteSession,
          }))
      );
    }),
    map(
      ({ inviteSession }) =>
        new secureChatActions.AwaitSessionApproval({ inviteSession })
    )
  );

  @Effect()
  awaitSessionApproval$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.AwaitSessionApproval),
    map((data: AwaitSessionApproval) => data.payload),
    switchMap(
      ({ inviteSession }: { inviteSession: SerializedInviteSession }) => {
        return this.cloudDatabaseService
          .listenSessionForE2eMessages$(inviteSession.id)
          .pipe(
            map((msg) => ({
              msg,
              inviteSession,
            }))
          );
        // TODO: decrypt answer on success
      }
    ),
    switchMap(({ msg, inviteSession }) => {
      return from(
        this.masterSessionService.approvalReceived(inviteSession, msg)
      );
    }),
    map(({ initData, serialized }) => {
      // report this activeSession to redux as it already saved
      return new JoinedSession({
        activeSession: serialized,
        ignoreSync: true,
        storeInDb: true,
        initData,
      });
    })
  );

  @Effect()
  joinedSession$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.JoinedSession),
    map((data: JoinedSession) => data.payload),
    tap(({ activeSession, storeInDb }) => {
      storeInDb ??
        this.masterSessionService.storeActiveSessionInDB(activeSession);
    }),
    mergeMap(({ activeSession, storeInDb }) => {
      return this.cloudDatabaseService
        .listenSessionForOlmMessages$(activeSession.id)
        .pipe(
          map((envelopeBuffer) => ({
            envelopeBuffer,
            sessionId: activeSession.id,
          }))
        );
    }),
    switchMap(({ envelopeBuffer, sessionId }) => {
      const data = {
        envelopeBuffer,
        sessionId,
      };
      const id = Object.keys(envelopeBuffer)[0];
      return from(this.masterSessionService.getMessage(id)).pipe(
        catchError((err) => of(noop())),
        map((message) => data)
      );
    }),
    switchMap(({ envelopeBuffer, sessionId }) => {
      const messageId = Object.keys(envelopeBuffer)[0];
      const message = envelopeBuffer[messageId];
      // I need peer's key here
      return this.olmGroupService.decryptOlmMessage(
        sessionId,
        message.message,
        message.olmSid
      );
    }),
    switchMap((value: DecryptedOlmEnvelope) => {
      return this.masterSessionService
        .storeMessage(value.id, value)
        .then(() => value);
    }),
    map((value: DecryptedOlmEnvelope) => {
      return new secureChatActions.IncomingMessage(value);
    })
  );

  @Effect()
  addPeerToSession$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.AddPeerToSessionByTicket),
    map((action: secureChatActions.AddPeerToSessionByTicket) => action.payload),
    // TODO: move this to masterSessionService
    filter((sessionTicket) => !!sessionTicket.activeSessionId),
    map((sessionTicket: SessionTicket) => {
      return this.masterSessionService.addPeerToSession(sessionTicket);
    }),
    map((activeSession: SerializedActiveSession) => {
      return new secureChatActions.AddPeerToSessionByTicketSuccess({
        activeSession,
      });
    })
  );

  @Effect()
  outgoingMessage$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.OutgoingMessage),
    map((action: OutgoingMessage) => action.payload),
    mergeMap((outgoingMessage: OutgoingMessageModel) => {
      return from(
        this.olmGroupService.encryptOlmMessage(
          outgoingMessage.sessionId,
          outgoingMessage.content,
          outgoingMessage.type
        )
      );
    }),
    tap(({ decrypted, encrypted }) => {
      // save to database encrypted version, we've got the ID
      this.masterSessionService.storeMessage(decrypted.id, decrypted);
    }),
    mergeMap(({ decrypted, encrypted }) => {
      return forkJoin([
        from(
          this.cloudDatabaseService.sendOlmMessage(
            decrypted.sessionId,
            encrypted,
            decrypted.id,
            decrypted.olmSid
          )
        ),
        of(encrypted),
        of(decrypted),
      ]);
    }),
    map(
      ([id, encrypted, decrypted]: [string, string, DecryptedOlmEnvelope]) =>
        new secureChatActions.OutgoingMessageSent(decrypted)
    )
  );

  @Effect({ dispatch: false })
  dumpSession$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.DumpSession),
    tap(() => this.masterSessionService.dumpSession())
  );

  @Effect()
  unlockMasterSession$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.UnlockMasterSession),
    map((action: secureChatActions.UnlockMasterSession) => action.payload),
    // todo: init service with decoded nonce
    switchMap((payload) => {
      return from(this.masterSessionService.unlock(payload)).pipe(
        map(
          ({
            serializedMasterSession,
            nonce,
          }: {
            serializedMasterSession: SerializedMasterSession;
            nonce: string;
          }) =>
            new secureChatActions.LoadMasterSession({
              masterSession: serializedMasterSession,
              nonce,
            })
        ),
        // Catch errors and dispatch UnlockMasterSessionError action
        catchError((error: string) =>
          of(
            new secureChatActions.UnlockMasterSessionError({
              operation:
                secureChatActions.SecureChatActionTypes.UnlockMasterSession,
              message: error,
            })
          )
        )
      );
    })
  );

  @Effect()
  loadMasterSession$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.LoadMasterSession),
    map((x: secureChatActions.LoadMasterSession) => x.payload),
    // deserialize masterSession
    switchMap(
      ({
        masterSession,
        nonce,
      }: {
        masterSession: SerializedMasterSession;
        nonce: string;
      }) =>
        from(
          this.masterSessionService
            .loadMasterSession(masterSession, nonce)
            .then(() => ({ masterSession }))
        )
    ),
    // sync fragment
    switchMap(
      ({ masterSession }: { masterSession: SerializedMasterSession }) => {
        const actions: secureChatActions.SecureChatActionTypesUnion[] =
          masterSession.inviteSessions.map(
            (inviteSession: SerializedInviteSession) => {
              return new secureChatActions.StartedSession(inviteSession);
            }
          );
        masterSession.activeSessions.forEach(
          (activeSession: SerializedActiveSession) => {
            // if active session is not own - invoke joinedsession, Otherwise - invoke peerconnected
            if (
              activeSession.peersIdentityPubkey ===
              this.masterSessionService.currentMasterSession.identity.public_key.fingerprint()
            ) {
              actions.push(
                new secureChatActions.JoinedSession({
                  activeSession,
                  storeInDb: false,
                  ignoreSync: true,
                })
              );
            } else {
              actions.push(
                new secureChatActions.JoinedSession({
                  activeSession,
                  storeInDb: false,
                  ignoreSync: true,
                })
              );
            }
          }
        );
        // actions = actions.concat(masterSession.activeSessions.map((activeSession) => {
        //   return new secureChatActions.JoinedSession(activeSession);
        // })
        actions.push(new secureChatActions.Ready());
        return actions;
      }
    )
  );

  @Effect()
  importMasterSession$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.ImportMasterSession),
    map((x: secureChatActions.ImportMasterSession) => x.payload),
    // deserialize masterSession
    // switchMap(({ masterSession, nonce }: { masterSession: SerializedMasterSession, nonce: string }) =>
    //   from(
    //     this.masterSessionService
    //       .importMasterSession(masterSession, nonce)
    //       .then(() => ({ masterSession }))
    //   )
    // ),
    // sync fragment
    switchMap(
      ({ masterSession }: { masterSession: SerializedMasterSession }) => {
        const actions: secureChatActions.SecureChatActionTypesUnion[] =
          masterSession.inviteSessions.map(
            (inviteSession: SerializedInviteSession) => {
              return new secureChatActions.StartedSession(inviteSession);
            }
          );
        masterSession.activeSessions.forEach(
          (activeSession: SerializedActiveSession) => {
            // if active session is not own - invoke joinedsession, Otherwise - invoke peerconnected
            if (
              activeSession.peersIdentityPubkey ===
              this.masterSessionService.currentMasterSession.identity.public_key.fingerprint()
            ) {
              actions.push(
                new secureChatActions.JoinedSession({
                  activeSession,
                  storeInDb: false,
                  ignoreSync: true,
                })
              );
            } else {
              actions.push(
                new secureChatActions.JoinedSession({
                  activeSession,
                  storeInDb: false,
                  ignoreSync: true,
                })
              );
            }
          }
        );
        // actions = actions.concat(masterSession.activeSessions.map((activeSession) => {
        //   return new secureChatActions.JoinedSession(activeSession);
        // })
        actions.push(new secureChatActions.Ready());
        return actions;
      }
    )
  );

  // TODO: remove dispatch: false. Listen to new messages on FB and load them to db.
  // Invoke incoming message if message comes from selectedSession
  @Effect({ dispatch: false })
  ready$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.Ready)
  );

  // loads messages by limit from db or activeSession to Redux store
  @Effect()
  loadMessages$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.LoadMessages),
    map((x: secureChatActions.LoadMessages) => x.payload),
    // TODO: recheck new messages in Firebase and store them in db
    switchMap(({ sessionId, limit, skipLast }) => {
      // load messages from idb
      return this.masterSessionService
        .getMessagesForSession(sessionId, limit, skipLast)
        .then(
          ({
            messages,
            count,
          }: {
            messages: DecryptedOlmEnvelope[];
            count: number;
          }) => {
            return {
              envelopes: messages,
              pagination: { sessionId, limit, skipLast },
              count,
            };
          }
        );
    }),
    map(
      ({ envelopes, pagination, count }) =>
        new secureChatActions.LoadedMessages({ envelopes, pagination, count })
    )
  );

  constructor(
    private actions$: Actions,
    private cloudDatabaseService: CloudDatabaseService,
    private masterSessionService: MasterSessionService,
    private olmGroupService: OlmGroupService,
    private secureChatSelectors: SecureChatSelectors,
    private fingerprintService: FingerprintService,
    private simpleCryptoService: SimpleCryptoService,
    private nonceService: NonceService
  ) {}
}
