import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { combineLatest, from } from "rxjs";
import {
  filter,
  first,
  map,
  switchMap,
  take,
  tap,
  withLatestFrom,
} from "rxjs/operators";
import { SerializedActiveSession } from "../../models/activeSession.model";
import { DecryptedOlmEnvelope } from "../../models/decryptedOlmEnvelope.model";
import { Device } from "../../models/device.model";
import { SerializedMasterSession } from "../../models/masterSession.model";
import { SyncInitData } from "../../models/syncInitData.model";
import { DBNonceCipher } from "../../services/db-models/nonce-cipher.dbmodel";
import { FingerprintService } from "../../services/fingerprint.service";
import { LocalDatabaseService } from "../../services/local-database.service";
import { MasterSessionService } from "../../services/master-session.service";
import { SecureChatDispatchers } from "../../services/secure-chat.dispatchers";
import { SecureChatSelectors } from "../../services/secure-chat.selectors";
import { SecureSyncSelectors } from "../../services/secure-sync.selectors";
import { SimpleCryptoService } from "../../services/simple-crypto.service";
import { SyncService } from "../../services/sync.service";
import * as secureChatActions from "../actions/secure-chat.actions";
import * as syncActions from "../actions/secure-sync.actions";
import { SecureSyncState } from "../reducers/secure-sync.reducer";
import { OlmGroupService } from "../../services/olm-group.service";

@Injectable()
export class SyncEffects {
  @Effect()
  createSyncSession$ = this.actions$.pipe(
    ofType(syncActions.SecureSyncActionTypes.CreateSyncSession),
    first(),
    switchMap(() => {
      return from(this.fingerprintService.generateDevice());
    }),
    switchMap((device: Device) => {
      return [
        new secureChatActions.StartSession({
          manualApprove: true,
          groupMode: true,
          oneTime: true,
        }),
        new syncActions.WaitForSyncSession(),
        new syncActions.GenerateDeviceFingerprint({ device }),
      ];
    })
  );

  @Effect()
  waitForSession$ = this.actions$.pipe(
    // wait for session
    ofType(secureChatActions.SecureChatActionTypes.StartedSession),
    map((action: secureChatActions.StartedSession) => action.payload),
    filter((payload) => payload.manualApprove === true),
    map(
      (payload) =>
        new syncActions.CreateSyncSessionDone({ syncSessionId: payload.id })
    )
  );

  // requires the link to PendingSession.
  // Sends device's fingerprint and name in JoinSession's preKey message
  @Effect()
  askForSyncSession$ = this.actions$.pipe(
    ofType(syncActions.SecureSyncActionTypes.AskForSyncSession),
    switchMap((action: syncActions.AskForSyncSession) => {
      const payload = action.payload;
      return [
        new secureChatActions.ApplyForSession({
          sessionId: payload.sessionId,
          welcomeData: JSON.stringify(
            this.masterSessionService.currentMasterSession.device
          ),
        }),
        new syncActions.GenerateDeviceFingerprint({
          device: this.masterSessionService.currentMasterSession.device,
        }),
      ];
    })
  );

  // decrypt fingerprint and name and save it to Store - unapprovedDevices
  @Effect()
  shouldApproveSession$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.ShouldApproveSession),
    map((action: secureChatActions.ShouldApproveSession) => action.payload),
    // add syncRequest data to unapproved
    map(
      (payload) =>
        new syncActions.ReceiveAskForSyncSession({
          device: JSON.parse(JSON.parse(payload.initData).welcomeData),
          pendingSessionId: payload.pendingSessionId,
        })
    )
  );

  // requires fingerprint of device approved by user. Creates ActiveSession and Sends masterSessionBundle with syncOlm Invite
  @Effect()
  approveSyncSessionJoin$ = this.actions$.pipe(
    ofType(syncActions.SecureSyncActionTypes.ApproveSyncSessionJoin),
    map((action: syncActions.ApproveSyncSessionJoin) => action.payload),
    // TODO: add device to trustedDevices
    switchMap(({ pendingSessionId, passphrase, fingerprint }) => {
      // TODO: device not found here
      return this.secureSyncSelectors
        .getUnapprovedDeviceByFingerprint(fingerprint)
        .pipe(
          map((device: Device) => ({
            pendingSessionId,
            passphrase,
            fingerprint,
            device,
          }))
        );
    }),
    switchMap((payload) => {
      return from(
        this.masterSessionService
          .exportMasterSessionForOtherDevice()
          .then((masterSessionBundle) => {
            // remove all outbound from activeSessions olms
            return {
              identity: masterSessionBundle.identity,
              activeSessions: masterSessionBundle.activeSessions,
              inviteSessions: [],
              preKeys: [],
              meta: masterSessionBundle.meta,
              device: masterSessionBundle.device,
            };
          })
          .then((masterSessionBundle: SerializedMasterSession) => {
            return this.localDatabaseService.dbInstance.nonceCipher
              .get(0)
              .then((nonceCipher) => ({ masterSessionBundle, nonceCipher }));
          })
          .then(
            ({
              masterSessionBundle,
              nonceCipher,
            }: {
              masterSessionBundle: SerializedMasterSession;
              nonceCipher: DBNonceCipher;
            }) => {
              // TODO: move whole nonceCipher entity
              const initData = {
                masterSessionBundle,
                device: payload.device,
                nonceCipher,
                type: "sync",
              } as SyncInitData;
              return {
                inviteSessionId: payload.pendingSessionId,
                initData: JSON.stringify(initData),
                device: payload.device,
                masterSessionBundle,
              };
            }
          )
      );
    }),
    map(({ inviteSessionId, initData }) => {
      // listen for peerconnected here
      return new secureChatActions.SessionApprovedAction({
        pendingSessionId: inviteSessionId,
        initData,
      });
    })
  );

  @Effect()
  receiveSyncSessionApprove$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.JoinedSession),
    map((action: secureChatActions.JoinedSession) => action.payload),
    filter((payload) => {
      return (
        payload.initData &&
        payload.initData.initData &&
        payload.initData.initData.type === "sync"
      );
    }),
    map((payload) => {
      return new syncActions.AwaitPassphraseImportUnlock({
        masterSessionToLoad: payload.initData.initData.masterSessionBundle,
        nonceCipher: payload.initData.initData.nonceCipher,
      });
    })
  );

  @Effect()
  unlockImport$ = this.actions$.pipe(
    ofType(syncActions.SecureSyncActionTypes.UnlockImport),
    map((action: syncActions.UnlockImport) => action.payload),
    switchMap((payload) => {
      // sync active session id not exist for now
      // get masterSessionToLoad and nonceCipher
      return combineLatest([
        this.secureSyncSelectors.masterSessionToLoad$,
        this.secureSyncSelectors.nonceCipher$,
      ]).pipe(
        filter(([masterSessionToLoad, nonceCipher]) => {
          return !!(masterSessionToLoad && nonceCipher);
        }),
        first(),
        map(([masterSessionToLoad, nonceCipher]) => ({
          masterSessionToLoad,
          nonceCipher,
          passphrase: payload.passphrase,
        }))
      );
    }),
    switchMap(({ masterSessionToLoad, nonceCipher, passphrase }) => {
      return from(
        this.syncService
          .importNeighbouringMasterSession(
            masterSessionToLoad,
            nonceCipher,
            passphrase
          )
          .then((obj) => {
            return {
              importerDevice: obj.importerDevice,
              masterSessionBundle: obj.masterSessionBundle,
              olmInvites: obj.olmInvites,
              activeSession: obj.syncSessionId,
              device:
                this.masterSessionService.currentMasterSession.device
                  .fingerprint,
            };
          })
      );
    }),
    switchMap(
      ({
        masterSessionBundle,
        olmInvites,
        activeSession,
        device,
        importerDevice,
      }) => {
        // TODO: add my device fingerprint to bind message to device
        return [
          new secureChatActions.OutgoingMessage({
            sessionId: activeSession,
            content: JSON.stringify({
              olmInvites,
              device,
            }),
            noStore: false,
            type: "sync:keyenrollment",
          }),
          new syncActions.UnlockImportSuccess({
            syncActiveSessionId: activeSession,
            masterSessionToLoad: masterSessionBundle,
          }),
        ];
      }
    )
  );
  // TODO: reject incoming session
  receiveSyncSessionRejected$ = this.actions$.pipe();

  @Effect()
  listenForKeyEnrollmentMessages$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.IncomingMessage),
    map((action: secureChatActions.IncomingMessage) => action.payload),
    switchMap((payload: DecryptedOlmEnvelope) =>
      this.secureSyncSelectors.syncInviteSessionId$.pipe(
        map((syncSessionId: string) => ({
          msg: payload,
          syncSessionId,
        }))
      )
    ),
    filter(({ msg, syncSessionId }) => msg.type === "sync:keyenrollment"),
    switchMap(({ msg, syncSessionId }) => {
      return this.secureSyncSelectors.approvedDevices$.pipe(
        map((devices: any) => {
          const device = devices.find(
            (x) => x.fingerprint === JSON.parse(msg.content).device
          );
          return { msg, syncSessionId, device };
        })
      );
    }),
    filter((payload) => !!payload.device),
    // find all active sessions
    switchMap(({ msg, syncSessionId, device }) => {
      return this.secureChatSelectors.activeSessions$.pipe(
        take(1),
        map((activeSessions) => ({
          msg,
          syncSessionId,
          activeSessions,
          device,
        }))
      );
    }),
    map(({ syncSessionId, msg, activeSessions, device }) => {
      activeSessions
        .filter((as) => as.inviteSessionId !== syncSessionId)
        .forEach((activeSession) => {
          this.secureChatDispatchers.outgoingMessage({
            content: JSON.stringify({
              olmInvite: JSON.parse(msg.content).olmInvites[activeSession.id]
                .olmInvite,
              device,
            }),
            sessionId: activeSession.id,
            noStore: false,
            type: "sync:trustkey",
          });
        });
      return {
        activeSessionLength: activeSessions.length,
        device,
        syncSessionId,
        msg,
        activeSessions,
      };
    }),
    switchMap(({ device, syncSessionId, msg, activeSessions }) => {
      this.olmGroupService.addTrustedDevices([device]);
      const addPeerActions = activeSessions
        .filter((as) => as.inviteSessionId !== syncSessionId)
        .map((activeSession) => {
          return new secureChatActions.AddPeerToSessionByTicket({
            activeSessionId: activeSession.id,
            olmInvite: JSON.parse(msg.content).olmInvites[activeSession.id]
              .olmInvite,
          });
        });
      const syncSession = activeSessions.find(
        (x) => x.inviteSessionId === syncSessionId
      );

      return (
        [
          new syncActions.ConfirmDeviceAdded({
            deviceFingerprint: device.fingerprint,
            syncActiveSessionId: syncSession.id,
          }),
          new secureChatActions.OutgoingMessage({
            sessionId: syncSession.id,
            content: JSON.stringify({
              device,
            }),
            noStore: false,
            type: "sync:enrolled",
          }),
        ] as any[]
      ).concat(addPeerActions);
    })
  );

  @Effect()
  listenForTrustedKeyMessages$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.IncomingMessage),
    map((action: secureChatActions.IncomingMessage) => action.payload),
    filter((msg) => msg.type === "sync:trustkey" && !msg.own),
    // add fingerprint to ActiveSession too
    map((decryptedOlmEnvelope: DecryptedOlmEnvelope) => {
      return new secureChatActions.AddPeerToSessionByTicket({
        activeSessionId: decryptedOlmEnvelope.sessionId,
        olmInvite: JSON.parse(decryptedOlmEnvelope.content).olmInvite,
      });
    })
  );

  @Effect()
  listenForEnrolledMessage$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.IncomingMessage),
    map((action: secureChatActions.IncomingMessage) => action.payload),
    switchMap((payload: DecryptedOlmEnvelope) =>
      this.secureSyncSelectors.syncActiveSessionId$.pipe(
        map((syncSessionId: string) => ({
          msg: payload,
          syncSessionId,
        }))
      )
    ),
    filter(
      ({
        msg,
        syncSessionId,
      }: {
        msg: DecryptedOlmEnvelope;
        syncSessionId: string;
      }) => msg.type === "sync:enrolled" && syncSessionId === msg.sessionId
    ),
    // TODO: add additional filters
    // TODO: type it
    switchMap(({ syncSessionId }) =>
      this.secureChatSelectors.getActiveSession(syncSessionId)
    ),
    // TODO: replace by switchMap and load passhash too
    switchMap((syncSession: SerializedActiveSession) =>
      combineLatest([
        this.secureSyncSelectors.masterSessionToLoad$,
        this.secureSyncSelectors.nonceCipher$,
      ]).pipe(
        map(([masterSessionToLoad, nonceCipher]) => ({
          masterSessionToLoad,
          nonceCipher,
          syncSession,
        }))
      )
    ),
    filter(
      ({
        masterSessionToLoad,
      }: {
        masterSessionToLoad: SerializedMasterSession;
        syncSession: SerializedActiveSession;
      }) => {
        return !!masterSessionToLoad;
      }
    ),
    switchMap(
      ({
        masterSessionToLoad,
        nonceCipher,
        syncSession,
      }: {
        masterSessionToLoad: SerializedMasterSession;
        nonceCipher: DBNonceCipher;
        syncSession: SerializedActiveSession;
      }) => {
        return from(
          this.masterSessionService
            .safeImportMasterSession(masterSessionToLoad)
            .then(({ masterSession, importerDevice }) => {
              return {
                nonceCipher,
                currentSyncSession: syncSession,
                masterSession,
                importerDevice,
              };
            })
        );
      }
    ),
    switchMap(
      ({ importerDevice, currentSyncSession, masterSession, nonceCipher }) => {
        return [
          new secureChatActions.ImportMasterSession({
            masterSession,
            nonce: this.masterSessionService.nonce
          }),
          new syncActions.EnrolledSuccessfully({
            syncActiveSessionId: currentSyncSession.id,
            loadReport: {
              activeSessionsLoaded: 0, // TODO: load activeSessions
              success: true,
              timestamp: new Date(Date.now()),
              device: {
                ...importerDevice,
                ...{ registeredAt: importerDevice.registeredAt.getTime() },
              },
            },
          }),
        ];
      }
    )
  );

  @Effect()
  handlePeerConnected$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.PeerConnected),
    map((action: secureChatActions.PeerConnected) => action.payload),
    // send sync:newsession message with map trustedDevices -> outbounds to unpickle by them
    switchMap((payload) =>
      this.secureSyncSelectors.global$.pipe(
        map((syncState) => ({
          payload,
          syncState,
        }))
      )
    ),
    filter(({ payload, syncState }) => {
      return (
        !!syncState.syncActiveSessionId &&
        payload.activeSession.id !== syncState.syncActiveSessionId &&
        Object.values(syncState.approvedDevices.entities).length > 0 &&
        !payload.ignoreSync
      );
    }),
    map(({ payload, syncState }) => {
      return new secureChatActions.OutgoingMessage({
        content: JSON.stringify({
          session: payload.activeSession,
          unpickleNonce: this.masterSessionService.nonce,
        }),
        noStore: false,
        sessionId: syncState.syncActiveSessionId,
        type: "sync:newsession",
      });
    })
  );

  /** key pre-enrollment */
  @Effect()
  handleJoinedSession$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.JoinedSession),
    map((action: secureChatActions.JoinedSession) => action.payload),
    // send sync:newsession message with map trustedDevices -> outbounds to unpickle by them
    withLatestFrom(this.secureSyncSelectors.global$),
    filter(
      ([payload, syncState]: [
        {
          activeSession: SerializedActiveSession;
          storeInDb: boolean;
          initData?: string;
          ignoreSync: boolean;
        },
        SecureSyncState
      ]) => {
        return (
          !!syncState.syncActiveSessionId &&
          payload.activeSession.id !== syncState.syncActiveSessionId &&
          !payload.ignoreSync
        );
      }
    ),
    map(
      ([payload, syncState]: [
        {
          activeSession: SerializedActiveSession;
          storeInDb: boolean;
          initData?: string;
        },
        SecureSyncState
      ]) => {
        return new secureChatActions.OutgoingMessage({
          content: JSON.stringify({
            session: payload.activeSession,
            unpickleNonce: this.masterSessionService.nonce,
            initData: payload.initData
          }),
          noStore: false,
          sessionId: syncState.syncActiveSessionId,
          type: "sync:newsession",
        });
      }
    )
  );

  // sideload session with outbound
  @Effect()
  handleNewSessionMessage$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.IncomingMessage),
    map((action: secureChatActions.IncomingMessage) => action.payload),
    switchMap((payload: DecryptedOlmEnvelope) =>
      this.secureSyncSelectors.syncActiveSessionId$.pipe(
        map((syncSessionId: string) => ({
          msg: payload,
          syncSessionId,
        }))
      )
    ),
    filter(
      ({
        msg,
        syncSessionId,
      }: {
        msg: DecryptedOlmEnvelope;
        syncSessionId: string;
      }) =>
        !!syncSessionId &&
        msg.type === "sync:newsession" &&
        syncSessionId === msg.sessionId
    ),
    map(({ msg }) => {
      // TODO: LoadActiveSession should be in secure-chat
      // it should invoke PeerConnected but loadActiveSession with custom
      // pickle key. It should be pickled again but by own nonce
      return new secureChatActions.ImportActiveSession({
        serializedActiveSession: JSON.parse(msg.content)
          .session as SerializedActiveSession,
        initData: JSON.parse(msg.content).initData,
        unpickleNonce: JSON.parse(msg.content).unpickleNonce,
      });
    })
  );

  constructor(
    private actions$: Actions,
    private fingerprintService: FingerprintService,
    private masterSessionService: MasterSessionService,
    private simpleCrypto: SimpleCryptoService,
    private secureChatSelectors: SecureChatSelectors,
    private secureSyncSelectors: SecureSyncSelectors,
    private secureChatDispatchers: SecureChatDispatchers,
    private syncService: SyncService,
    private localDatabaseService: LocalDatabaseService,
    private olmGroupService: OlmGroupService
  ) {}
}
