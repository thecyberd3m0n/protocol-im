import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { from, noop, of } from "rxjs";
import {
  catchError,
  filter,
  first,
  map,
  switchMap,
  take,
  tap,
} from "rxjs/operators";
import { DecryptedOlmEnvelope } from "../../models/decryptedOlmEnvelope.model";
import { CloudMessagingService } from "../../services/cloud-messaging.service";
import { SecureChatDispatchers } from "../../services/secure-chat.dispatchers";
import { SecureChatSelectors } from "../../services/secure-chat.selectors";
import * as notificationActions from "../actions/notifications.actions";
import * as secureChatActions from "../actions/secure-chat.actions";
import { OutgoingNotification } from "../../models/outgoingNotification.model";

@Injectable()
export class NotificationsEffects {
  @Effect()
  init$ = this.actions$.pipe(
    ofType(notificationActions.NotificationsActionTypes.Init),
    switchMap(() => {
      return from(this.cloudMessagingService.loadStatus());
    }),
    switchMap(({ permissions, token }) => {
      const actions = [];

      if (permissions === "granted" && token) {
        actions.push(
          new notificationActions.EnableSuccess(),
          new secureChatActions.UpdateMasterSessionMeta({
            type: "notifications",
            meta: {
              enabled: true,
              devToken: token,
            },
          })
        );
      } else if (permissions === 'default') {
        actions.push(
          new notificationActions.ProposeEnable()
        );
      } else if (permissions === 'denied') {
        actions.push(new notificationActions.EnableFailed('no permissions'))
      }
      return actions;
    })
  );

  @Effect()
  enable$ = this.actions$.pipe(
    ofType(notificationActions.NotificationsActionTypes.Enable),
    switchMap(() =>
      from(this.cloudMessagingService.requestPermission()).pipe(
        catchError((err) => {
          // invoke EnableFailed action
          return of(
            new notificationActions.EnableFailed("Permissions revoked")
          );
        })
      )
    ),
    // save notifications token to users metadata
    switchMap(() => {
      return from(this.cloudMessagingService.initServiceWorker());
    }),
    switchMap(() =>
      from(this.cloudMessagingService.initToken()).pipe(
        catchError((err) => {
          // invoke EnableFailed action
          return of(
            new notificationActions.EnableFailed("Cannot init token")
          );
        })
        // TODO: get current masterSession tokens
      )
    ),
    // send notify about notificationToken all peers
    switchMap((result: string) => {
      return this.secureChatSelectors.activeSessions$.pipe(
        take(1),
        map((activeSessions) => {
          activeSessions.forEach((activeSession) => {
            this.secureChatDispatchers.outgoingMessage({
              type: "notify:enable",
              content: JSON.stringify({
                devToken: result,
              }),
              sessionId: activeSession.id,
              noStore: true,
            });
          });
          return result;
        })
      );
    }),
    switchMap((result: string) => {
      return [
        new notificationActions.EnableSuccess(),
        new secureChatActions.UpdateMasterSessionMeta({
          type: "notifications",
          meta: {
            enabled: true,
            devToken: result,
          },
        }),
      ];
    })
  );

  @Effect()
  listenForNotifyEnableMsgs = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.IncomingMessage),
    map((action: secureChatActions.IncomingMessage) => action.payload),
    filter((msg) => msg.type === "notify:enable"),
    switchMap((msg: DecryptedOlmEnvelope) => {
      return this.secureChatSelectors.getActiveSession(msg.sessionId).pipe(
        first(),
        map((session) => ({ session, msg }))
      );
    }),
    map(({ session, msg }) => {
      const devTokens = session.meta.notifications
        ? session.meta.notifications.devTokens
        : [];
      devTokens.push(JSON.parse(msg.content).devToken);
      return new secureChatActions.UpdateActiveSessionMeta({
        sessionId: session.id,
        meta: { devTokens: devTokens },
        type: "notifications",
      });
    })
  );

  @Effect()
  peerConnected$ = this.actions$.pipe(
    ofType(secureChatActions.SecureChatActionTypes.PeerConnected),
    map((action: secureChatActions.PeerConnected) => action.payload),
    switchMap(({ activeSession }) => {
      return this.secureChatSelectors
        .getMasterSessionMeta("notifications")
        .pipe(
          filter((x) => !!x),
          map((x) => ({ meta: x, activeSession }))
        );
    }),
    map(({ meta, activeSession }) => {
      return new secureChatActions.OutgoingMessage({
        content: JSON.stringify({
          devToken: meta.devToken,
        }),
        noStore: false,
        sessionId: activeSession.id,
        type: "notify:enable",
      });
    })
  );

  @Effect({ dispatch: false })
  notify$ = this.actions$.pipe(
    ofType(notificationActions.NotificationsActionTypes.Notify),
    map((action: notificationActions.Notify) => action.payload),
    tap((outgoingNotification: OutgoingNotification) => {
      this.cloudMessagingService.sendUnencryptedNotificationMessage(
        outgoingNotification.content,
        outgoingNotification.devTokens
      );
    })
  );

  constructor(
    private actions$: Actions,
    private cloudMessagingService: CloudMessagingService,
    private secureChatSelectors: SecureChatSelectors,
    private secureChatDispatchers: SecureChatDispatchers
  ) {}
}
