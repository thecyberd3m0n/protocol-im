import {
  NotificationsActionTypes,
  NotificationsActionTypesUnion,
} from "../actions/notifications.actions";

export class NotificationsState {
  permissions: NotificationPermission | null;
  enabled: boolean;
}

const initialState: NotificationsState = {
  permissions: null,
  enabled: null,
};

export function reducer(
  state = initialState,
  action: NotificationsActionTypesUnion
): NotificationsState {
  switch (action.type) {
    case NotificationsActionTypes.Enable:
      return { ...state, ...{ enabled: true } };
    case NotificationsActionTypes.ProposeEnable:
        return { ...state, ...{enabled: false, permissions: 'default' }}
    case NotificationsActionTypes.EnableSuccess:
      return { ...state, ...{ allowed: true } };
    case NotificationsActionTypes.EnableFailed:
      return { ...state, ...{ enabled: false } };
  }
}
