import {
  approvedDevicesAdapter,
  ApprovedDevicesState,
} from "../../adapters/approvedDevices.adapter";
import {
  LockedDevicesState,
  lockedDevicesAdapter,
} from "../../adapters/lockedDevices.adapter";
import {
  unapprovedDevicesAdapter,
  UnapprovedDevicesState,
} from "../../adapters/unapprovedDevices.adapter";
import { Device } from "../../models/device.model";
import { SerializedMasterSession } from "../../models/masterSession.model";
import { DBNonceCipher } from "../../services/db-models/nonce-cipher.dbmodel";
import {
  SecureSyncActionTypes,
  SecureSyncActionTypesUnion,
} from "../actions/secure-sync.actions";

export enum SyncState {
  INIT,
  AWAIT_APPROVE,
  AWAIT_PASSPHRASE,
  READY_TO_EXPORT,
}

export interface LoadReport {
  device: {
    name: string;
    fingerprint: string;
    registeredAt: number;
  };
  activeSessionsLoaded: number;
  success: boolean;
  timestamp: Date;
}

export interface SecureSyncState {
  syncActiveSessionId?: string | null;
  syncInviteSessionId?: string | null;
  syncState: SyncState;
  unapprovedDevices: UnapprovedDevicesState;
  lockedDevices: LockedDevicesState;
  approvedDevices: ApprovedDevicesState;
  masterSessionToLoad?: SerializedMasterSession;
  nonceCipherToLoad?: DBNonceCipher;
  lastLoadReport?: LoadReport;
  device?: Device;
}

export const initialState: SecureSyncState = {
  syncActiveSessionId: null,
  syncInviteSessionId: null,
  unapprovedDevices: unapprovedDevicesAdapter.getInitialState(),
  approvedDevices: approvedDevicesAdapter.getInitialState(),
  lockedDevices: lockedDevicesAdapter.getInitialState(),
  syncState: SyncState.INIT,
  masterSessionToLoad: null,
  lastLoadReport: null,
  device: null,
};

export function reducer(
  state = initialState,
  action: SecureSyncActionTypesUnion
): SecureSyncState {
  switch (action.type) {
    case SecureSyncActionTypes.GenerateDeviceFingerprint:
      return { ...state, ...{ device: action.payload.device } };
    case SecureSyncActionTypes.CreateSyncSessionDone:
      return {
        ...state,
        ...{ syncInviteSessionId: action.payload.syncSessionId, syncState: SyncState.READY_TO_EXPORT },
      };
    case SecureSyncActionTypes.AskForSyncSession:
      return {
        ...state,
        ...{
          syncInviteSessionId: action.payload.sessionId,
          syncState: SyncState.AWAIT_APPROVE,
        },
      };
    case SecureSyncActionTypes.ReceiveAskForSyncSession:
      return {
        ...state,
        ...{
          unapprovedDevices: unapprovedDevicesAdapter.addOne(
            action.payload,
            state.unapprovedDevices
          ),
        },
      };
    case SecureSyncActionTypes.ApproveSyncSessionJoin:
      return {
        ...state,
        ...{
          approvedDevices: approvedDevicesAdapter.addOne(
            {
              fingerprint: action.payload.fingerprint,
            },
            state.approvedDevices
          ),
          unapprovedDevices: unapprovedDevicesAdapter.removeOne(
            action.payload.fingerprint,
            state.unapprovedDevices
          ),
        },
      };
    case SecureSyncActionTypes.AwaitPassphraseImportUnlock:
      return {
        ...state,
        masterSessionToLoad: action.payload.masterSessionToLoad,
        nonceCipherToLoad: action.payload.nonceCipher,
        syncState: SyncState.AWAIT_PASSPHRASE,
      };
    case SecureSyncActionTypes.UnlockImportSuccess:
      return {
        ...state,
        syncActiveSessionId: action.payload.syncActiveSessionId,
        masterSessionToLoad: action.payload.masterSessionToLoad
      }
    case SecureSyncActionTypes.ConfirmDeviceAdded:
      return {
        ...state,
        // unapprovedDevices: unapprovedDevicesAdapter.removeOne(
        //   action.payload.deviceFingerprint,
        //   state.unapprovedDevices
        // ),
        // approvedDevices: approvedDevicesAdapter.addOne(
        //   { fingerprint: action.payload.deviceFingerprint },
        //   state.approvedDevices
        // ),
        syncActiveSessionId: action.payload.syncActiveSessionId,
        syncState: SyncState.READY_TO_EXPORT
      };
    case SecureSyncActionTypes.EnrolledSuccessfully:
      return {
        ...state,
        ...{
          lastLoadReport: action.payload.loadReport,
          masterSessionToLoad: null,
          passhash: null,
        },
        unapprovedDevices: unapprovedDevicesAdapter.removeOne(
          action.payload.loadReport.device.fingerprint,
          state.unapprovedDevices
        ),
        approvedDevices: approvedDevicesAdapter.addOne(
          {
            ...action.payload.loadReport.device,
            ...{
              registeredAt: new Date(
                action.payload.loadReport.device.registeredAt
              ),
            },
          },
          state.approvedDevices
        ),
        syncActiveSessionId: action.payload.syncActiveSessionId,
      };
    default: {
      return state;
    }
  }
}
