import { messagesStateAdapter } from '../../adapters/decryptedMessages.adapter';
import { inviteSessionsAdapter } from '../../adapters/pendingSessions.adapter';
import { sessionRequestsAdapter, SessionRequestsState } from '../../adapters/sessionRequest.adapter';
import { MetadataMap } from '../../models/metadataMap.model';
import { activeSessionsAdapter, ActiveSessionsState } from './../../adapters/activeSessions.adapter';
import { MessagesState } from './../../adapters/decryptedMessages.adapter';
import { InviteSessionsState } from './../../adapters/pendingSessions.adapter';
import { SecureChatActionTypes, SecureChatActionTypesUnion, SecureChatError } from './../actions/secure-chat.actions';
import { NotificationsState } from './notifications.reducer';
import { SecureSyncState } from './secure-sync.reducer';

export interface SessionConfig {
  manualApprove: boolean;
  groupMode: boolean; // group mode will move users always to same active session. Otherwise they will be moved to new activeSessions.
  metadata?: MetadataMap;
  oneTime: boolean;
}

export interface SessionRequest {
  inviteSessionId: string;
  requestInitData: any;
  created: Date;
}

export enum SecureChatMode {
  DISABLED,
  INITIALIZING,
  EMPTY, // user has MasterSession but without Profile Data and ability to create new Session. He can join one session as a Guest.
  READY, // user has MasterSession (stored) with Profile Data
  ERROR,
  AWAITING_MASTER_PASSPHRASE,
  GUEST
}

export interface DecryptedMessagesPagination {
  limit: number;
  skipLast: number;
  sessionId?: string;
  count: number;
}

export interface BackupData {
  id?: string;
  nonce?: string;
  passphrase?: string;
}

export interface SecureChatState {
  backup: BackupData;
  mode: SecureChatMode;
  masterSessionMeta: MetadataMap;
  inviteSessions: InviteSessionsState;
  activeSessions: ActiveSessionsState;
  sessionRequests: SessionRequestsState;
  decryptedMessages: MessagesState;
  decryptedMessagesPagination: DecryptedMessagesPagination;
  stored: boolean;
  storedPIN?: string;
  error: SecureChatError | null;
}

export interface SecureChatModuleState {
  secureChatState: SecureChatState;
  secureSyncState: SecureSyncState;
  notificationsState: NotificationsState;
}

export const initialState: SecureChatState = {
  backup: null,
  mode: SecureChatMode.DISABLED,
  inviteSessions: inviteSessionsAdapter.getInitialState(),
  activeSessions: activeSessionsAdapter.getInitialState(),
  decryptedMessages: messagesStateAdapter.getInitialState(),
  sessionRequests: sessionRequestsAdapter.getInitialState(),
  stored: false,
  masterSessionMeta: {},
  decryptedMessagesPagination: {
    limit: 50,
    count: 0,
    sessionId: null,
    skipLast: 0
  },
  error: null
};

export function reducer(state = initialState, action: SecureChatActionTypesUnion): SecureChatState {
  switch (action.type) {
    case SecureChatActionTypes.Init: {
      // store data from server in AuthState
      return { ...state, mode: SecureChatMode.INITIALIZING };
    }
    case SecureChatActionTypes.GuestSession: {
      return { ...state, mode: SecureChatMode.GUEST };
    }
    case SecureChatActionTypes.Ready: {
      return { ...state, mode: SecureChatMode.READY };
    }
    case SecureChatActionTypes.AwaitingUnlockMasterSession: {
      return { ...state, mode: SecureChatMode.AWAITING_MASTER_PASSPHRASE };
    }
    case SecureChatActionTypes.StartSession: {
      return { ...state };
    }
    case SecureChatActionTypes.StartedSession: {
      return {
        ...state,
        inviteSessions: inviteSessionsAdapter.addOne(action.payload, state.inviteSessions),
      };
    }
    case SecureChatActionTypes.ShouldApproveSession: {
      return {
        ...state,
        sessionRequests: sessionRequestsAdapter.addOne({
          created: new Date(),
          inviteSessionId: action.payload.pendingSessionId,
          requestInitData: JSON.parse(JSON.parse(action.payload.initData).welcomeData)
        }, state.sessionRequests)
      }
    }
    case SecureChatActionTypes.PeerConnected: {
      return {
        ...state,
        inviteSessions: action.payload.inviteSession ? inviteSessionsAdapter.upsertOne(action.payload.inviteSession, state.inviteSessions) : state.inviteSessions,
        activeSessions: activeSessionsAdapter.upsertOne(action.payload.activeSession, state.activeSessions),
      };
    }
    case SecureChatActionTypes.SetupProfile: {
      return {
        ...state,
        stored: true,
      }
    }
    case SecureChatActionTypes.UpdateMasterSessionMeta: {
      const { type, meta } = action.payload;
      return {
        ...state,
        masterSessionMeta: {
          [type]: meta
        }
      };
    }
    case SecureChatActionTypes.UpdateActiveSessionMeta: {
      let obj = {};
      obj[action.payload.type] = action.payload.meta;
      return {
        ...state,
        activeSessions: activeSessionsAdapter.updateOne({
          id: action.payload.sessionId,
          changes: {
            meta: obj
          }
        }, state.activeSessions)
      }
    }
    case SecureChatActionTypes.JoinedSession: {
      return {
        ...state,
        inviteSessions: inviteSessionsAdapter.removeOne(action.payload.activeSession.id, state.inviteSessions),
        activeSessions: activeSessionsAdapter.addOne(action.payload.activeSession, state.activeSessions)
      };
    }
    case SecureChatActionTypes.IncomingMessage: {
      return {
        ...state,
        decryptedMessages: action.payload.sessionId === state.decryptedMessagesPagination.sessionId ? messagesStateAdapter.addOne(action.payload, state.decryptedMessages) : state.decryptedMessages
      };
    }
    case SecureChatActionTypes.OutgoingMessageSent: {
      return {
        ...state,
        decryptedMessages: messagesStateAdapter.addOne(action.payload, state.decryptedMessages),
      };
    }

    case SecureChatActionTypes.LoadMasterSession: {
      return {
        ...state,
        inviteSessions: inviteSessionsAdapter.addMany(action.payload.masterSession.inviteSessions, state.inviteSessions),
        activeSessions: activeSessionsAdapter.addMany(action.payload.masterSession.activeSessions, state.activeSessions),
        masterSessionMeta: JSON.parse(action.payload.masterSession.meta) as MetadataMap,
        stored: true,
      };
    }
    case SecureChatActionTypes.ImportMasterSession: {
      return {
        ...state,
        inviteSessions: inviteSessionsAdapter.addMany(action.payload.masterSession.inviteSessions, state.inviteSessions),
        activeSessions: activeSessionsAdapter.addMany(action.payload.masterSession.activeSessions, state.activeSessions),
        masterSessionMeta: JSON.parse(action.payload.masterSession.meta) as MetadataMap,
        stored: true,
      };
    }
    case SecureChatActionTypes.LockMasterSession: {
      return {
        ...state,
        inviteSessions: inviteSessionsAdapter.getInitialState(),
        activeSessions: activeSessionsAdapter.getInitialState(),
        stored: false,
        mode: SecureChatMode.AWAITING_MASTER_PASSPHRASE
      };
    }
    case SecureChatActionTypes.StoreSessionSuccess: {
      return {
        ...state,
        stored: true
      };
    }

    case SecureChatActionTypes.BackupMasterSessionSuccess: {
      return {
        ...state,
        backup: {
          id: action.payload.backupId,
          nonce: action.payload.nonce,
          passphrase: action.payload.passphrase
        }
      }
    }

    case SecureChatActionTypes.DumpSession: {
      return {
        ...state,
        mode: SecureChatMode.DISABLED,
        stored: false
      };
    }

    case SecureChatActionTypes.LoadMessages: {
      return {
        ...state,
        decryptedMessagesPagination: {
          limit: action.payload.limit,
          skipLast: action.payload.skipLast,
          sessionId: action.payload.sessionId,
          count: 0
        },
        decryptedMessages: messagesStateAdapter.getInitialState()
      }
    }

    case SecureChatActionTypes.LoadedMessages: {
      return {
        ...state,
        decryptedMessagesPagination: {
          ...state.decryptedMessagesPagination,
          count: action.payload.count
        },
        decryptedMessages: messagesStateAdapter.addAll(action.payload.envelopes, state.decryptedMessages)
      }
    }

    case SecureChatActionTypes.UnlockMasterSessionError: {
      return {
        ...state,
        error: action.payload
      }
    }

    default: {
      return state;
    }
  }
}
