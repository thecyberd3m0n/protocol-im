import { Action } from "@ngrx/store";
import { ActiveSession, SerializedActiveSession } from "../../models/activeSession.model";
import { DecryptedOlmEnvelope } from "../../models/decryptedOlmEnvelope.model";
import { SerializedInviteSession } from "../../models/inviteSession.model";
import { MetadataMap } from "../../models/metadataMap.model";
import { OutgoingMessageModel } from "../../models/outgoingMessage.model";
import { SessionTicket } from "../../models/sessionInvite.model";
import { UnsignedProfile } from "../../models/unsignedProfile.model";
import { BackupData, SessionConfig } from "../reducers/secure-chat.reducer";
import { SerializedMasterSession } from "./../../models/masterSession.model";

export interface SecureChatError {
  operation: string;
  message: string;
}

export enum SecureChatActionTypes {
  Init = "[Secure Chat] Init",
  Ready = "[Secure Chat] Ready",
  GuestSession = "[Secure Chat] Guest Session started",
  AwaitingUnlockMasterSession = "[Secure Chat] Awaiting unlock master session",
  UnlockMasterSession = "[Secure Chat] Unlock master session",
  LockMasterSession = "[Secure Chat] Lock master session",
  UpdateMasterSessionMeta = "[Secure Chat] Update master session meta",
  UpdateActiveSessionMeta = "[Secure Chat] Update active session meta",
  LoadMasterSession = "[Secure Chat] Load Master Session",
  ImportActiveSession = "[Secure Chat] Import Active Session",
  SetupProfile = "[Secure Chat] Setup Profile",
  ApplyForSession = "[Secure Chat] Apply For Session",
  JoinedSession = "[Secure Chat] Joined Session",
  StartSession = "[Secure Chat] Start Session",
  StartedSession = "[Secure Chat] Started Session",
  ShouldApproveSession = "[Secure Chat] Should Approve Session",
  SessionApprovedAction = "[Secure Chat] Session Approved Action",
  SessionRejectedAction = "[Secure Chat] Session Rejected Action",
  PeerConnected = "[Secure Chat] Peer Connected",
  AddPeerToSessionByTicket = "[Secure Chat] Add Peer To Active Session By Invite",
  AddPeerToSessionByTicketSuccess = "[Secure Chat] Add Peer To Active Session By Invite Success",
  BackupMasterSession = "[Secure Chat] Backup Master Session",
  BackupMasterSessionSuccess = "[Secure Chat] Backup Master Session Success",
  UpdateMasterSessionBackup = "[Secure Chat] Update Master Session Backup",
  LoadMessages = "[Secure Chat] Load Messages",
  LoadedMessages = "[Secure Chat] Loaded Messages",
  IncomingMessage = "[Secure Chat] Incoming Message",
  OutgoingMessage = "[Secure Chat] Outgoing Message",
  OutgoingMessageSent = "[Secure Chat] Outgoing Message Sent",
  DumpSession = "[Secure Chat] Dump Session",
  StoreSession = "[Secure Chat] Store Session",
  StoreSessionSuccess = "[Secure Chat] Store Session Success",
  StoreSessionError = "[Secure Chat] Store Session Error",
  AwaitSessionApproval = "[Secure Chat] Await Session Approval",
  ImportMasterSession = "[Secure Chat] Import Master Session",
  UnlockMasterSessionError = "[Secure Chat] Unlock Master Session Error"
}

export class Init implements Action {
  readonly type = SecureChatActionTypes.Init;
}

export class SetupProfile implements Action {
  readonly type = SecureChatActionTypes.SetupProfile;
  constructor(public payload: UnsignedProfile) {}
}

export class AwaitingUnlockMasterSession implements Action {
  readonly type = SecureChatActionTypes.AwaitingUnlockMasterSession;
}

export class UnlockMasterSession implements Action {
  readonly type = SecureChatActionTypes.UnlockMasterSession;
  constructor(public payload: string) {}
}

export class UnlockMasterSessionError implements Action {
  readonly type = SecureChatActionTypes.UnlockMasterSessionError;
  constructor(public payload: SecureChatError) {}
}

export class UpdateMasterSessionMeta implements Action {
  readonly type = SecureChatActionTypes.UpdateMasterSessionMeta;
  constructor(public payload: { type: string; meta: MetadataMap }) {}
}

export class UpdateActiveSessionMeta implements Action {
  readonly type = SecureChatActionTypes.UpdateActiveSessionMeta;
  constructor(
    public payload: { sessionId: string; type: string; meta: MetadataMap }
  ) {}
}

export class LockMasterSession implements Action {
  readonly type = SecureChatActionTypes.LockMasterSession;
}

export class Ready implements Action {
  readonly type = SecureChatActionTypes.Ready;
  constructor() {}
}

export class LoadMessages implements Action {
  readonly type = SecureChatActionTypes.LoadMessages;
  constructor(
    public payload: { sessionId: string; limit: number; skipLast: number }
  ) {}
}

export class UpdateMasterSessionBackup implements Action {
  readonly type = SecureChatActionTypes.UpdateMasterSessionBackup;
  constructor(public payload: BackupData) {}
}

export class LoadedMessages implements Action {
  readonly type = SecureChatActionTypes.LoadedMessages;
  constructor(
    public payload: {
      envelopes: DecryptedOlmEnvelope[];
      pagination: { sessionId: string; limit: number; skipLast: number };
      count: number;
    }
  ) {}
}

export class BackupMasterSession implements Action {
  readonly type = SecureChatActionTypes.BackupMasterSession;
  constructor(public payload?: { backupId?: string; passphrase: string }) {}
}

export class BackupMasterSessionSuccess implements Action {
  readonly type = SecureChatActionTypes.BackupMasterSessionSuccess;
  constructor(
    public payload: { nonce: string; backupId: string; passphrase: string }
  ) {}
}

export class GuestSession implements Action {
  readonly type = SecureChatActionTypes.GuestSession;
  constructor() {}
}

export class LoadMasterSession implements Action {
  readonly type = SecureChatActionTypes.LoadMasterSession;
  constructor(public payload: { masterSession: SerializedMasterSession, nonce?: string }) {}
}

export class ImportMasterSession implements Action {
  readonly type = SecureChatActionTypes.ImportMasterSession;
  constructor(public payload: { masterSession: SerializedMasterSession, nonce?: string }) {}
}

export class ApplyForSession implements Action {
  readonly type = SecureChatActionTypes.ApplyForSession;
  // TODO: define PreKeyInitMessage as must type
  constructor(public payload: { welcomeData?: string; sessionId: string }) {}
}

export class AwaitSessionApproval implements Action {
  readonly type = SecureChatActionTypes.AwaitSessionApproval;
  constructor(public payload: { inviteSession: SerializedInviteSession }) {}
}


export class JoinedSession implements Action {
  readonly type = SecureChatActionTypes.JoinedSession;
  constructor(
    public payload: {
      activeSession: SerializedActiveSession;
      storeInDb: boolean;
      ignoreSync: boolean;
      initData?: any;
    }
  ) {}
}

export class ImportActiveSession implements Action {
  readonly type = SecureChatActionTypes.ImportActiveSession;
  constructor(
    public payload: {
      serializedActiveSession: SerializedActiveSession;
      unpickleNonce: string
      initData: any
    }
  ) {}
}

export class AddPeerToSessionByTicket implements Action {
  readonly type = SecureChatActionTypes.AddPeerToSessionByTicket;
  constructor(public payload: SessionTicket) {}
}

export class AddPeerToSessionByTicketSuccess implements Action {
  readonly type = SecureChatActionTypes.AddPeerToSessionByTicketSuccess;
  constructor(public payload: { activeSession: SerializedActiveSession }) {}
}

export class StartSession implements Action {
  readonly type = SecureChatActionTypes.StartSession;
  constructor(public payload: SessionConfig) {}
}

export class ShouldApproveSession implements Action {
  readonly type = SecureChatActionTypes.ShouldApproveSession;
  constructor(public payload: { initData: string; pendingSessionId: string }) {}
}

export class SessionApprovedAction implements Action {
  readonly type = SecureChatActionTypes.SessionApprovedAction;
  constructor(
    public payload: { pendingSessionId: string; initData?: string }
  ) {}
}

export class SessionRejectedAction implements Action {
  readonly type = SecureChatActionTypes.SessionRejectedAction;
  constructor(public payload: { pendingSessionId: string }) {}
}

export class StartedSession implements Action {
  readonly type = SecureChatActionTypes.StartedSession;
  constructor(public payload: SerializedInviteSession) {}
}

export class PeerConnected implements Action {
  readonly type = SecureChatActionTypes.PeerConnected;
  constructor(
    public payload: {
      initData?: any;
      inviteSession?: SerializedInviteSession;
      activeSession: SerializedActiveSession;
      storeInDb: boolean;
      ignoreSync: boolean;
      requestInitData?: any 
    }
  ) {}
}

export class IncomingMessage implements Action {
  readonly type = SecureChatActionTypes.IncomingMessage;
  constructor(public payload: DecryptedOlmEnvelope) {}
}

export class OutgoingMessage implements Action {
  readonly type = SecureChatActionTypes.OutgoingMessage;
  constructor(public payload: OutgoingMessageModel) {}
}

export class OutgoingMessageSent implements Action {
  readonly type = SecureChatActionTypes.OutgoingMessageSent;
  constructor(public payload: DecryptedOlmEnvelope) {}
}

export class StoreSession implements Action {
  readonly type = SecureChatActionTypes.StoreSession;
  constructor() {}
}

export class DumpSession implements Action {
  readonly type = SecureChatActionTypes.DumpSession;
}

export class StoreSessionSuccess implements Action {
  readonly type = SecureChatActionTypes.StoreSessionSuccess;
  constructor() {}
}

export class StoreSessionError implements Action {
  readonly type = SecureChatActionTypes.StoreSessionError;
  constructor(public payload: Error) {}
}

export type SecureChatActionTypesUnion =
  | Init
  | SetupProfile
  | AwaitingUnlockMasterSession
  | UnlockMasterSession
  | LockMasterSession
  | UpdateMasterSessionMeta
  | UpdateActiveSessionMeta
  | AddPeerToSessionByTicket
  | AddPeerToSessionByTicketSuccess
  | BackupMasterSession
  | BackupMasterSessionSuccess
  | Ready
  | ShouldApproveSession
  | GuestSession
  | LoadMasterSession
  | ImportMasterSession
  | ImportActiveSession
  | LoadMessages
  | LoadedMessages
  | ApplyForSession
  | JoinedSession
  | StartSession
  | StartedSession
  | PeerConnected
  | DumpSession
  | IncomingMessage
  | OutgoingMessage
  | OutgoingMessageSent
  | StoreSession
  | StoreSessionSuccess
  | StoreSessionError
  | UnlockMasterSessionError;
