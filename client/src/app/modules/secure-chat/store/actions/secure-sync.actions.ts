import { Action } from "@ngrx/store";
import { Device } from "../../models/device.model";
import {
  SerializedMasterSession
} from "../../models/masterSession.model";
import { DBNonceCipher } from "../../services/db-models/nonce-cipher.dbmodel";
import { LoadReport } from "../reducers/secure-sync.reducer";

export enum SecureSyncActionTypes {
  WaitForSyncSession = "[Secure Sync] Wait for Sync Session",
  GenerateDeviceFingerprint = "[Secure Sync] Generate Device Fingerprint",
  CreateSyncSession = "[Secure Sync] Create Sync Session",
  CreateSyncSessionDone = "[Secure Sync] Create Sync Session Done",
  AskForSyncSession = "[Secure Sync] Ask For Sync Session",
  ReceiveAskForSyncSession = "[Secure Sync] Receive Ask For Sync Session",
  ShouldApproveSyncSession = "[Secure Sync] Should Approve Sync Session",
  ApproveSyncSessionJoin = "[Secure Sync] Approve Sync Session Join",
  ApprovedSyncSessionJoin = "[Secure Sync] Approved Sync Session Join",
  RejectSyncSessionJoin = "[Secure Sync] Reject Sync Session Join",
  ReceiveSyncSessionApprove = "[Secure Sync] Receive Sync Session Approve",
  ReceiveSyncSessionRejected = "[Secure Sync] Receive Sync Session Rejected",
  ConfirmDeviceAdded = "[Secure Sync] Confirm Device Added",
  EnrolledSuccessfully = "[Secure Sync] Device Enrolled Successfully",
  AwaitPassphraseImportUnlock = "[Secure Sync] Await Passphrase Import Unlock",
  UnlockImport = "[Secure Sync] Unlock Import",
  UnlockImportSuccess = "[Secure Sync] Unlock Import Success",
  UnlockImportErrror = "[Secure Sync] Unlock Import Error"
}

export class WaitForSyncSession implements Action {
  readonly type = SecureSyncActionTypes.WaitForSyncSession;
}

export class CreateSyncSession implements Action {
  readonly type = SecureSyncActionTypes.CreateSyncSession;
}

export class CreateSyncSessionDone implements Action {
  readonly type = SecureSyncActionTypes.CreateSyncSessionDone;
  constructor(public payload: { syncSessionId: string }) {}
}

export class AskForSyncSession implements Action {
  readonly type = SecureSyncActionTypes.AskForSyncSession;
  constructor(public payload: { sessionId: string }) {}
}

export class ReceiveAskForSyncSession implements Action {
  readonly type = SecureSyncActionTypes.ReceiveAskForSyncSession;
  constructor(public payload: { device: Device; pendingSessionId: string }) {}
}

export class ApproveSyncSessionJoin implements Action {
  readonly type = SecureSyncActionTypes.ApproveSyncSessionJoin;
  constructor(
    public payload: {
      pendingSessionId: string;
      passphrase: string;
      fingerprint: string;
    }
  ) {}
}

export class ApprovedSyncSessionJoin implements Action {
  readonly type = SecureSyncActionTypes.ApprovedSyncSessionJoin;
  constructor(public payload: { activeSessionId: string }) {}
}

export class RejectSyncSessionJoin implements Action {
  readonly type = SecureSyncActionTypes.RejectSyncSessionJoin;
  constructor(public payload: { pendingSessionId: string }) {}
}

export class ReceiveSyncSessionApprove implements Action {
  readonly type = SecureSyncActionTypes.ReceiveSyncSessionApprove;
  constructor(public payload: { activeSessionId: string }) {}
}

export class GenerateDeviceFingerprint implements Action {
  readonly type = SecureSyncActionTypes.GenerateDeviceFingerprint;
  constructor(public payload: { device: Device }) {}
}

export class ReceiveSyncSessionRejected implements Action {
  readonly type = SecureSyncActionTypes.ReceiveSyncSessionApprove;
}

export class AwaitPassphraseImportUnlock implements Action {
  readonly type = SecureSyncActionTypes.AwaitPassphraseImportUnlock;
  constructor(
    public payload: {
      masterSessionToLoad: SerializedMasterSession;
      nonceCipher: DBNonceCipher;
    }
  ) {}
}

export class UnlockImport implements Action {
  readonly type = SecureSyncActionTypes.UnlockImport;
  constructor(public payload: { passphrase: string }) {}
}

export class UnlockImportSuccess implements Action {
  readonly type = SecureSyncActionTypes.UnlockImportSuccess;
  constructor(public payload: { syncActiveSessionId: string, masterSessionToLoad: SerializedMasterSession }) {}
}

export class UnlockImportError implements Action {
  readonly type = SecureSyncActionTypes.UnlockImportErrror
  constructor(public payload: { message: string }) {}
}

export class ConfirmDeviceAdded implements Action {
  readonly type = SecureSyncActionTypes.ConfirmDeviceAdded;
  constructor(
    public payload: { deviceFingerprint: string; syncActiveSessionId: string }
  ) {}
}

export class EnrolledSuccessfully implements Action {
  readonly type = SecureSyncActionTypes.EnrolledSuccessfully;
  constructor(
    public payload: { loadReport: LoadReport; syncActiveSessionId: string }
  ) {}
}

export type SecureSyncActionTypesUnion =
  | CreateSyncSession
  | GenerateDeviceFingerprint
  | CreateSyncSessionDone
  | AskForSyncSession
  | ReceiveAskForSyncSession
  | ApproveSyncSessionJoin
  | ReceiveSyncSessionApprove
  | ReceiveSyncSessionRejected
  | ConfirmDeviceAdded
  | AwaitPassphraseImportUnlock
  | WaitForSyncSession
  | RejectSyncSessionJoin
  | ApprovedSyncSessionJoin
  | EnrolledSuccessfully
  | UnlockImport
  | UnlockImportSuccess;
