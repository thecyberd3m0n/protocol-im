import { Action } from "@ngrx/store";
import { OutgoingNotification } from "../../models/outgoingNotification.model";

export enum NotificationsActionTypes {
    Init = "[Notifications] Init",
    ProposeEnable = "[Notifications] Propose Enable",
    Enable = "[Notifications] Enable",
    EnableSuccess = "[Notifications] Enable Success",
    EnableFailed = "[Notifications] Enable Failed",
    Notify = "[Notifications] Notify"
}

export class Init implements Action {
    readonly type = NotificationsActionTypes.Init;
}

export class Enable implements Action {
    readonly type = NotificationsActionTypes.Enable;
}

export class ProposeEnable implements Action {
    readonly type = NotificationsActionTypes.ProposeEnable;
}

export class EnableSuccess implements Action {
    readonly type = NotificationsActionTypes.EnableSuccess;
}

export class EnableFailed implements Action {
    readonly type = NotificationsActionTypes.EnableFailed;
    constructor(public payload: string) {}
}

export class Notify implements Action {
    readonly type = NotificationsActionTypes.Notify;
    constructor(public payload: OutgoingNotification) {}
}

export type NotificationsActionTypesUnion =
    | Init
    | Enable
    | ProposeEnable
    | EnableSuccess
    | EnableFailed
    | Notify;