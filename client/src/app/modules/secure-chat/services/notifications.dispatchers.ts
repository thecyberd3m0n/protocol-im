import { Store } from "@ngrx/store";
import { NotificationsState } from "../store/reducers/notifications.reducer";
import * as NotificationAction from "../store/actions/notifications.actions";
import { Injectable } from "@angular/core";
import { OutgoingNotification } from "../models/outgoingNotification.model";

@Injectable()
export class NotificationsDispatchers {
    constructor(private store: Store<NotificationsState>) {}
    init() {
        this.store.dispatch(new NotificationAction.Init())
    }
    enable() {
        this.store.dispatch(new NotificationAction.Enable())
    }

    notify(notification: OutgoingNotification) {
        this.store.dispatch(new NotificationAction.Notify(notification))
    }

}