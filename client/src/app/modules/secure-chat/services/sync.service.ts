import { Injectable } from "@angular/core";
import { MasterSessionService } from "./master-session.service";
import {
  MasterSession,
  SerializedMasterSession,
} from "../models/masterSession.model";
import {
  OlmGroupDescriptor,
  OlmGroupService,
  OlmInvite,
} from "./olm-group.service";
import { Device } from "../models/device.model";
import { LocalDatabaseService } from "./local-database.service";
import { DBNonceCipher } from "./db-models/nonce-cipher.dbmodel";
import { NonceService } from "./nonce.service";

@Injectable({
  providedIn: "root",
})
export class SyncService {
  constructor(
    private masterSessionService: MasterSessionService,
    private olmGroupService: OlmGroupService,
    private localDatabaseService: LocalDatabaseService,
    private nonceSerice: NonceService
  ) {}

  async importNeighbouringMasterSession(
    serializedMasterSession: SerializedMasterSession,
    nonceCipher: DBNonceCipher,
    passphrase: string
  ): Promise<{
    masterSessionBundle: SerializedMasterSession;
    olmInvites: Record<
      string,
      {
        olmGroup: OlmGroupDescriptor;
        olmInvite: OlmInvite;
      }
    >;
    importerDevice: Device;
    syncSessionId: string;
  }> {
    let oldNonce = null;
    const nonce = this.masterSessionService.nonce;
    try {
      oldNonce = await this.nonceSerice.decryptNonceByPassphrase(passphrase, nonceCipher);
    } catch (e) {
      throw new Error(
        "[SyncService] importNeighbouringMasterSession: Incorrect nonce decryption password"
      );
    }
    const localNonceCipherData = await this.nonceSerice.encryptAndSealNonce(
      this.masterSessionService.nonce,
      passphrase
    );

    try {
      await this.localDatabaseService.putDBNonce(
        localNonceCipherData.salt,
        localNonceCipherData.cipherNonce,
        localNonceCipherData.saltSechash
      );
    } catch (e) {
      console.error(e);
      throw new Error(
        "[SyncService] importNeighbouringMasterSession: Cannot import Master Session: Cannot parse nonceCipher"
      );
    }

    const currentMasterSession = this.masterSessionService.currentMasterSession;
    const syncSessionId = currentMasterSession.activeSessions[0].id;
    const masterSession = MasterSession.deserialize(serializedMasterSession);
    const importerDevice = { ...masterSession.device };
    masterSession.device = currentMasterSession.device;
    if (!masterSession.trustedDevices) {
      masterSession.trustedDevices = [];
    }
    masterSession.trustedDevices.push(importerDevice);
    // during this procedure we should unpickle olmGroups using old
    this.olmGroupService.loadIdentity(masterSession.identity.public_key.fingerprint(), masterSession.trustedDevices);
    
    const olmInvites = masterSession.activeSessions
      .filter((x) => x.id !== syncSessionId)
      .map((as) => {
        return this.olmGroupService.importGroupAndEnrollMe(
          as.id,
          as.olmGroupDescriptor,
          oldNonce
        );
      })
      .reduce((result, current) => {
        // Iterate over each record in the array
        Object.entries(current).forEach(([key, value]) => {
          // Check if the key already exists in the result
          if (result.hasOwnProperty(key)) {
            // Merge the properties of the current record with the existing record
            result[key] = {
              ...result[key],
              ...value,
            };
          } else {
            // Add the new record to the result
            result[key] = value;
          }
        });

        return result;
      }, {});
    // assign olmGroup to activeSessions
    masterSession.activeSessions = masterSession.activeSessions
      .filter((x) => x.id !== syncSessionId)
      .map((as) => {
        as.olmGroupDescriptor = { ...olmInvites[as.id].olmGroup };
        delete olmInvites[as.id].olmGroup; // it will be stringified to message
        return as;
      });

    masterSession.activeSessions = masterSession.activeSessions.concat(
      currentMasterSession.activeSessions
    );
    masterSession.identity = currentMasterSession.identity;
    const serialized = MasterSession.serialize(masterSession);
    return {
      masterSessionBundle: serialized,
      olmInvites,
      syncSessionId,
      importerDevice,
    };
  }
}
