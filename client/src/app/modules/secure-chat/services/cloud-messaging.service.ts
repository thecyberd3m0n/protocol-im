import { Injectable } from "@angular/core";
import { AngularFireFunctions } from "@angular/fire/functions";
import { AngularFireMessaging } from "@angular/fire/messaging";
import { SerializedActiveSession } from "../models/activeSession.model";
import { LocalDatabaseService } from "./local-database.service";
import { environment } from "../../../../environments/environment";

const SW_FILENAME = "firebase-messaging-sw.js";

@Injectable()
export class CloudMessagingService {
  token: string | null = null;
  permissions: NotificationPermission;
  constructor(
    private afMessaging: AngularFireMessaging,
    private fns: AngularFireFunctions,
    private localDatabaseService: LocalDatabaseService
  ) {}

  loadStatus(): Promise<{
    permissions: NotificationPermission;
    token?: string;
  }> {
    return new Promise(async (resolve, reject) => {
      this.permissions = this.checkPermissions();
      if (this.permissions === "granted") {
        // check if has token in DB
        const notificationState =
          await this.localDatabaseService.dbInstance.notificationsState.toArray();
        if (notificationState.length === 0) {
          try {
            await this.initServiceWorker();
            this.token = await this.initToken();

            await this.localDatabaseService.dbInstance.notificationsState.put({
              id: 0,
              token: this.token,
            });
          } catch (e) {
            console.error(e);
            reject(e);
          }
        } else {
          // load and respond with token
          this.token = notificationState[0].token;
          resolve({
            permissions: this.permissions,
            token: this.token,
          });
        }
        // if (notificationState && notificationState.deviceToken) {
        //   this.token = notificationState.deviceToken;
        //   resolve({
        //     permissions: this.permissions,
        //     token: this.token
        //   })
      } else {
        resolve({
          permissions: this.permissions,
        });
      }
    });
  }

  requestPermission(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.afMessaging.requestPermission.subscribe(
        () => {
          console.log("[CloudMessagingService] Permission granted!");
          resolve();
        },
        (error) => {
          console.error(error);
          reject();
        }
      );
    });
  }

  private checkPermissions(): NotificationPermission | null {
    if ("Notification" in window) {
      return window.Notification.permission;
    } else {
      return null;
    }
  }

  initToken(): Promise<string> {
    return new Promise<string>((resolve, reject) => {

      this.afMessaging.requestToken.subscribe(
        (token) => {
          this.token = token;
          console.log("[CloudMessagingService] token acquired", token);
          resolve(this.token);
          return;
        },
        (err) => {
          this.afMessaging.requestToken.subscribe(
            (token) => {
              this.token = token;
              console.log("[CloudMessagingService] token acquired", token);
              resolve(this.token);
              return;
            },
            (err) => {
              reject(err);
              return;
            }
          );
          return;
        }
      );
    });
  }

  initServiceWorker(): Promise<void> {
    return new Promise((resolve, reject) => {
      if (!("serviceWorker" in navigator)) {
        reject("[CloudMessagingService] Service Workers not supported by browser");
        return;
      }
      navigator.serviceWorker.getRegistrations().then(
        (registrations) => {
          console.log(
            "[CloudMessagingService] current SW registrations",
            registrations
          );
          const swPath = `${
            environment.baseUrl
          }/${SW_FILENAME}`;
          if (
            registrations.findIndex((x) => x.active.scriptURL === swPath) === -1
          ) {
            console.log(
              `[CloudMessagingService] installing new Service Worker, path: ${swPath}`
            );
            navigator.serviceWorker.register(swPath).then(async (reg) => {
              await navigator.serviceWorker.ready;

              console.log("[CloudMessagingService] Service Worker status", reg);
              resolve();
            }, (err) => {
              console.error("[CloudMessagingService] Service Worker cannot be installed", err);
            }); 
          }
        },
        (err) => {
          console.error(
            "[CloudMessagingService] Cannot register Service Worker",
            err
          );
          reject(err);
        }
      );
    });
  }

  registerDevice(session: SerializedActiveSession, devToken: string) {
    navigator.serviceWorker.controller.postMessage({
      op: "regroom",
      session,
      devToken,
    });
  }

  async sendUnencryptedNotificationMessage(message: any, devTokens: string[]) {
    // do not enrypt the message

    //   // now send it to cloud function
    this.triggerNotifyFn({ deviceTokens: devTokens, payload: message }).then(
      (res) => {
        console.log("triggerNotifyFn res", res);
      },
      (err) => {
        console.error("triggerNotifyFn errpr", err);
      }
    );
    // } else {
    //   return;
    // }
  }

  private async triggerNotifyFn(data) {
    try {
      const notifyDevices = this.fns.httpsCallable(
        "notifyDevices-notifyDevices"
      );
      const response = await notifyDevices(data).toPromise();
      console.log(response);
    } catch (error) {
      console.error("Error triggering function:", error);
    }
  }
}
