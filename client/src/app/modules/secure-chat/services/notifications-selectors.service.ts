import { Store, createFeatureSelector, createSelector } from "@ngrx/store";
import { NotificationsState } from "../store/reducers/notifications.reducer";
import { SecureChatModuleState } from "../store/reducers/secure-chat.reducer";
import { Injectable } from "@angular/core";
import { filter } from "rxjs/operators";

const getSecureChatState =
  createFeatureSelector<SecureChatModuleState>("secure-chat");


const getNotificationsState = createSelector(
    getSecureChatState,
    (state: SecureChatModuleState) => {
        return state.notificationsState
    }
)

@Injectable()
export class NotificationsSelectors {
    public global$ = this.store.select(getNotificationsState).pipe(filter(x => !!x));
    constructor(private store: Store<NotificationsState>) {}
}