import { Injectable } from "@angular/core";
import {
  createFeatureSelector,
  createSelector,
  select,
  Store,
} from "@ngrx/store";
import { Observable } from "rxjs";
import {
  distinctUntilKeyChanged,
  filter,
  map,
  tap
} from "rxjs/operators";
import { messagesStateAdapter } from "../adapters/decryptedMessages.adapter";
import {
  SerializedActiveSession
} from "../models/activeSession.model";
import { DecryptedOlmEnvelope } from "../models/decryptedOlmEnvelope.model";
import {
  SerializedInviteSession
} from "../models/inviteSession.model";
import {
  BackupData,
  SecureChatState,
  SessionRequest,
} from "../store/reducers/secure-chat.reducer";
import { activeSessionsAdapter } from "./../adapters/activeSessions.adapter";
import { inviteSessionsAdapter } from "./../adapters/pendingSessions.adapter";
import { SecureChatModuleState } from "./../store/reducers/secure-chat.reducer";

const getSecureChatState =
  createFeatureSelector<SecureChatModuleState>("secure-chat");

const getDeviceState = createSelector(
  getSecureChatState,
  (state: SecureChatModuleState) => {
    return state.secureChatState;
  }
);

const getLastError = createSelector(getDeviceState, (state) => state.error);

const getMasterSessionMeta = createSelector(getDeviceState, (state, props) =>
  state.masterSessionMeta ? state.masterSessionMeta[props.type] : null
);

const getMode = createSelector(getDeviceState, (state) => state.mode);

const getMessages = createSelector(
  getDeviceState,
  (state) => state.decryptedMessages
);

const getSessionRequests = createSelector(getDeviceState, (state) => state.sessionRequests);

const getFilteredMessages = createSelector(getMessages, (state, filter) => {
  const messages = messagesStateAdapter.getSelectors().selectAll(state);
  if (!filter) {
    return messages;
  }
  const filterEntries = Object.entries(filter);
  const filteredMessages = messages.filter((message) => {
    return filterEntries.every(([key, value]) => {
      return message[key] === value;
    });
  });
  return filteredMessages.map((msg) => {
    let copy = { ...msg };
    if (!(msg.timestamp instanceof Date)) {
      copy.timestamp = new Date(msg.timestamp);
    }
    return copy;
  });
});

const getActiveSessions = createSelector(
  getDeviceState,
  (state) => state.activeSessions
);

const getPendingSessions = createSelector(
  getDeviceState,
  (state) => state.inviteSessions
);

const selectAllPendingSessions = createSelector(
  getPendingSessions,
  inviteSessionsAdapter.getSelectors().selectAll
);

const selectAllActiveSessions = createSelector(
  getActiveSessions,
  activeSessionsAdapter.getSelectors().selectAll
);

const selectAllMessages = createSelector(
  getFilteredMessages,
  (messages) => messages
);

const selectLastMessage = createSelector(
  getFilteredMessages,
  (messages) =>
    messages.sort((a, b) => b.timestamp.getTime() - a.timestamp.getTime())[0]
);

const selectActiveSessionById = createSelector(
  getActiveSessions,
  (entities, props) => {
    return entities.entities[props.id];
  }
);

// TODO: to test if works, probably not
const selectActiveSessionMeta = createSelector(
  getActiveSessions,
  (entities, props) => {
    return entities.entities[props.id]
      ? entities.entities[props.id].meta[props.type]
      : null;
  }
);

const selectPendingSessionById = createSelector(
  getPendingSessions,
  (entities, props) => {
    return entities.entities[props.id];
  }
);

const selectSessionRequestByInviteSessionId = createSelector(
  getSessionRequests,
  (entities, props) => {
    return entities.entities[props.id];
  }
)

const selectMessageById = (id) =>
  createSelector(getMessages, (entities) => entities[id]);

const getSyncData = () =>
  createSelector(getDeviceState, (state) => state.backup);

@Injectable()
export class SecureChatSelectors {
  public global$ = this.store.select(getDeviceState);
  public error$ = this.store.select(getLastError);
  public mode$ = this.store.select(getMode);
  public identityState$ = this.store.select(getDeviceState);
  public pendingSessions$ = this.store.select(selectAllPendingSessions);
  public activeSessions$ = this.store.select(selectAllActiveSessions);
  public decryptedMessages$ = this.store.select(getMessages);
  public sessionRequests$ = this.store.select(getSessionRequests);
  constructor(private store: Store<SecureChatState>) {}

  getMessageByID(id: string) {
    return this.store.pipe(select(selectMessageById(id)));
  }

  getSyncData(): Observable<BackupData> {
    return this.store.pipe(select(getSyncData()));
  }

  getMessages(
    filter: Partial<DecryptedOlmEnvelope>
  ): Observable<DecryptedOlmEnvelope[]> {
    return this.store.pipe(select(selectAllMessages, filter));
  }

  getMostRecentMessage(filters: Partial<DecryptedOlmEnvelope>) {
    return this.store.pipe(
      select(selectLastMessage, filters),
      filter((x) => !!x),
      distinctUntilKeyChanged("id")
    );
  }

  getActiveSession(id: string): Observable<SerializedActiveSession> {
    return this.store.pipe(select(selectActiveSessionById, { id }), filter(x => !!x));
  }

  getMasterSessionMeta(type: string) {
    return this.store.pipe(
      select(getMasterSessionMeta, { type }),
      filter((x) => !!x)
    );
  }

  getPendingSessionById(id: string): Observable<SerializedInviteSession> {
    return this.store.pipe(select(selectPendingSessionById, { id }));
  }

  getSessionRequestByInviteSessionId(id: string): Observable<SessionRequest> {
    return this.store.pipe(select(selectSessionRequestByInviteSessionId, { id }));
  }

  getActiveSessionMeta(id: string, type: string) {
    return this.store.pipe(
      select(selectActiveSessionMeta, { id, type }),
      filter((x) => !!x)
    );
  }

  getActiveSessions() {
    return this.store.pipe(select(selectAllActiveSessions));
  }

  getPendingSessions() {
    return this.store.pipe(select(selectAllPendingSessions));
  }
}
