import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";
import { ArrayBufferUtilsService } from "./arrayBufferUtils.service";
import { SimpleCryptoService } from "./simple-crypto.service";

export interface NonceCipherData {
  salt: string;
  cipherNonce: string;
  saltSechash: string;
}

@Injectable()
export class NonceService {
  constructor(private simpleCrypto: SimpleCryptoService) {}

  /**
   * Generates random nonce and exposes as current one. Use for guest sessions
   */
  async generateNonce(): Promise<string> {
    await this.simpleCrypto.init(environment.appName);
    return this.simpleCrypto.randomString(64);
  }

  /**
   * Hashes passphrase, and encrypts nonce by the argon2 passhash.
   * Then stores the salt and resulting ciphertext in masterSessionTable
   * If has no currentNonce - auto-generates one using generateNonce()
   * returns material that needs to be stored in order to decrypt the Nonce
   *
   * @param {string} passphrase
   * @returns {string, string, string}: serializable data required to decrypt the nonce using the passphrase
   */
  async encryptAndSealNonce(
    nonce: string,
    passphrase: string
  ): Promise<NonceCipherData> {
    const passhash = await this.simpleCrypto.secHash(passphrase);
    // TODO: store salt
    await this.simpleCrypto.init(environment.appName, passhash.hash);

    const { salt: cryptNonce, ciphertext } = await this.simpleCrypto.encrypt(
      nonce
    );
    const saltString = await ArrayBufferUtilsService.uint8ArrayToB64String(
      cryptNonce
    );
    const cipherNonce = await ArrayBufferUtilsService.uint8ArrayToB64String(
      ciphertext
    );
    return {
      salt: saltString,
      cipherNonce: cipherNonce,
      saltSechash: passhash.salt,
    };
  }

  async decryptNonceByPassphrase(
    passphrase: string,
    cipherData: NonceCipherData
  ): Promise<string> {
    await this.simpleCrypto.init(environment.appName);
    const phSaltString = await ArrayBufferUtilsService.b64stringToUint8Array(
      cipherData.saltSechash
    );
    const passhash = await this.simpleCrypto.secHash(passphrase, phSaltString);
    let nonce = null;
    try {
      await this.simpleCrypto.init(environment.appName, passhash.hash);
      nonce = await this.simpleCrypto.decrypt({
        ciphertextStr: cipherData.cipherNonce,
        nonceStr: cipherData.salt,
      });
    } catch (e) {
      console.error(e);
      console.error("[NonceService] Cannot decrypt nonce");
      throw new Error("[NonceService] Cannot decrypt nonce");
    }
    return nonce;
  }
}
