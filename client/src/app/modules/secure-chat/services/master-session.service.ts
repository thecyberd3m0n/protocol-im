import { Injectable } from "@angular/core";
import {
  IdentityKey,
  IdentityKeyPair,
  PreKey,
  PreKeyBundle,
} from "@wireapp/proteus/src/main/keys";
import { LocalDatabaseService } from "./local-database.service";
import {
  ActiveSession,
  SerializedActiveSession,
} from "../models/activeSession.model";
import { MetadataMap } from "../models/metadataMap.model";
import {
  InviteSession,
  SerializedInviteSession,
} from "../models/inviteSession.model";
import { DecryptedOlmEnvelope } from "../models/decryptedOlmEnvelope.model";
import {
  MasterSession,
  SerializedMasterSession,
} from "../models/masterSession.model";
import { ArrayBufferUtilsService } from "./arrayBufferUtils.service";
import { FingerprintService } from "./fingerprint.service";
import { OlmGroupService, OlmInvite } from "./olm-group.service";
import { SimpleCryptoService } from "./simple-crypto.service";
import { PreKeyStore, Session } from "@wireapp/proteus/src/main/session";
import { initialState } from "../store/reducers/secure-sync.reducer";
import { keys, session } from "@wireapp/proteus";
import { Envelope } from "@wireapp/proteus/src/main/message";
import { DecryptedE2eEnvelope } from "../models/decryptedE2eEnvelope";
import { Device } from "../models/device.model";
import { SessionTicket } from "../models/sessionInvite.model";
import { environment } from "../../../../environments/environment";
import { NonceService } from "./nonce.service";

const APP_NAME = environment.appName;

interface InviteSessionInitMessage {
  olmInvite: OlmInvite;
  userPublicKeyFingerprint: string;
}

@Injectable()
export class MasterSessionService extends PreKeyStore {
  guestMessagesCache = new Map<string, DecryptedOlmEnvelope>();
  salt: string;
  preInitMessage = "PreInit";
  private _nonce: string;
  public get nonce(): string {
    return this._nonce;
  }
  public set nonce(value: string) {
    console.log("NEW NONCE", value);
    this._nonce = value;
  }
  private _currentMasterSession: MasterSession;
  public get currentMasterSession(): MasterSession {
    return this._currentMasterSession;
  }
  public set currentMasterSession(value: MasterSession) {
    this._currentMasterSession = value;
  }
  constructor(
    private simpleCrypto: SimpleCryptoService,
    private olmGroupService: OlmGroupService,
    private fingerprintService: FingerprintService,
    private localDatabaseService: LocalDatabaseService,
    private nonceService: NonceService
  ) {
    super();
  }

  add_prekey(preKey: PreKey) {
    this.currentMasterSession.preKeys.push(preKey);
  }

  async load_prekey(prekey_id: number): Promise<PreKey> {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    return this.currentMasterSession.preKeys.find(
      (prekey) => prekey.key_id === prekey_id
    )!;
  }

  async delete_prekey(prekey_id: number): Promise<number> {
    const matches = this.currentMasterSession.preKeys.filter(
      (prekey) => prekey.key_id === prekey_id
    );
    delete matches[0];
    return prekey_id;
  }

  async load_prekeys(): Promise<PreKey[]> {
    return new Promise((resolve) => resolve(this.currentMasterSession.preKeys));
  }

  async init(nonce?: string): Promise<void> {
    return new Promise(async (resolve, reject) => {
      if (!nonce) {
        nonce = await this.nonceService.generateNonce();
      }

      try {
        this.localDatabaseService.init(nonce).then(() => {
          this.nonce = nonce;
          // encrypt and seal nonce
          resolve();
        });
      } catch (e) {
        reject(e);
      }
    });
  }

  verifyInit() {
    if (!this.nonce) {
      throw new Error("[MasterSession] Master Session Service not inited");
    }
  }

  // TODO: implement unlock from cipherData only, using SW's tempkey
  async softUnlock() {
    throw new Error("Not implemented yet");
  }

  async unlock(passphrase): Promise<{
    serializedMasterSession: SerializedMasterSession;
    nonce: string;
  }> {
    return new Promise<{
      serializedMasterSession: SerializedMasterSession;
      nonce: string;
    }>(async (resolve, reject) => {
      const cipherData = await this.localDatabaseService.getNonceCipher();
      if (!cipherData) {
        reject("[MasterSessionService] Could not find cipherData in IDB");
        return;
      }

      let nonce = null;
      try {
        nonce = await this.nonceService.decryptNonceByPassphrase(
          passphrase,
          cipherData
        );
      } catch (e) {
        reject("[MasterSessionService] Cannot unlock session: wrong password");
        return;
      }

      await this.init(nonce); //TODO: hack - after single init database encryption not works

      const decryptedMasterSession =
        await this.localDatabaseService.dbInstance.masterSessionTable.get(0);

      const masterSession = await MasterSession.deserialize(
        JSON.parse(decryptedMasterSession.encrypted)
      );
      // load pending and active sessions
      const activeSessions =
        await this.localDatabaseService.dbInstance.activeSessions.toArray();
      // TODO: cannot decrypt activeSessions
      const pendingSessions =
        await this.localDatabaseService.dbInstance.pendingSessions.toArray();

      masterSession.activeSessions = await Promise.all(
        activeSessions.map((activeSessionEnc) => {
          return ActiveSession.deserialize(
            JSON.parse(activeSessionEnc.encrypted)
          );
        })
      );
      masterSession.inviteSessions = pendingSessions.map((pendingSession) => {
        return InviteSession.deserialize(
          masterSession.identity,
          JSON.parse(pendingSession.encrypted)
        );
      });
      await this.olmGroupService.init(
        masterSession.identity.public_key.fingerprint(),
        masterSession.device,
        this.nonce,
        masterSession.syncStatus
          ? Object.values(masterSession.syncStatus.approvedDevices.entities)
          : null
      );
      resolve({
        serializedMasterSession: MasterSession.serialize(masterSession),
        nonce: this.nonce,
      });
    });
  }

  generatePreKeyBundle(pubKey: IdentityKey, preKey: PreKey): PreKeyBundle {
    return new keys.PreKeyBundle(pubKey, preKey);
  }

  loadActiveSession(serializedActiveSession: SerializedActiveSession) {
    return this.olmGroupService.loadActiveSession(
      ActiveSession.deserialize(serializedActiveSession),
      this.nonce
    );
  }

  importActiveSession(
    serializedActiveSession: SerializedActiveSession,
    nonce: string
  ) {
    // unpickle active session by given nonce. Don't change the current one
    this.olmGroupService.loadActiveSession(
      ActiveSession.deserialize(serializedActiveSession),
      nonce
    );
  }

  peerConnected(
    inviteSession: SerializedInviteSession,
    envelope: ArrayBuffer
  ): Promise<{
    inviteSession: SerializedInviteSession;
    initMessage: InviteSessionInitMessage;
  }> {
    return new Promise((resolve) => {
      let deserialisedInviteSession: InviteSession;
      try {
        deserialisedInviteSession = InviteSession.deserialize(
          this.currentMasterSession.identity,
          inviteSession
        );
      } catch (e) {
        console.error(e);
        throw new Error(
          `[MasterSessionService] peerConnected: Cannot deserialize InviteSession using current Identity`
        );
      }
      session.Session.init_from_message(
        this.currentMasterSession.identity,
        this,
        Envelope.deserialise(envelope)
      ).then(async ([session, decryptedMessage]: [Session, Uint8Array]) => {
        const initMessage = new TextDecoder("utf-8").decode(decryptedMessage);
        deserialisedInviteSession.e2eSession = session;
        deserialisedInviteSession.olmInvite = JSON.parse(initMessage)
          .olmInvite as OlmInvite;
        deserialisedInviteSession.peersPublicKey =
          JSON.parse(initMessage).userPublicKeyFingerprint;
        await this.storePendingSession(deserialisedInviteSession);
        resolve({
          inviteSession: InviteSession.serialize(deserialisedInviteSession),
          initMessage: JSON.parse(initMessage) as InviteSessionInitMessage,
        });
      });
    });
  }

  async approvalReceived(
    inviteSession: SerializedInviteSession,
    encryptedApproveMessage: { [x: string]: ArrayBuffer | SharedArrayBuffer }
  ): Promise<{ initData; serialized }> {
    // produce SerializedActiveSession here
    return new Promise(async (resolve) => {
      // decrypt the message using proteus
      const decrypted = await this.decryptDirectMessage(
        inviteSession.id,
        Envelope.deserialise(Object.values(encryptedApproveMessage)[0])
      );
      // read activeSession data from it and create ActiveSession
      const approvalMessage = JSON.parse(decrypted.content);
      const olmInvite = approvalMessage.olmInvite;
      const activeSessionId = approvalMessage.activeSessionId;

      const olmGroupDescriptor = this.olmGroupService.acceptInviteToSession(
        inviteSession.id,
        activeSessionId,
        olmInvite
      );
      // TODO: olmGroup is undefined
      const activeSession = new ActiveSession(
        activeSessionId,
        {},
        olmGroupDescriptor,
        inviteSession.peersPublicKey,
        inviteSession.id
      );
      const serialized = ActiveSession.serialize(activeSession);

      await this.storeActiveSessionInDB(serialized);
      resolve({
        serialized,
        initData: approvalMessage,
      });
    });
  }

  async approveSession(
    inviteSessionId: string,
    initData: InviteSessionInitMessage
  ): Promise<{
    e2eInviteMessage;
    inviteSession: SerializedInviteSession;
    activeSession: SerializedActiveSession;
    newPrekey: ArrayBuffer;
    initData: InviteSessionInitMessage;
  }> {
    // get invite session by id
    // TODO: why inviteSession has always activeSessionId = null?
    let inviteSession = this.currentMasterSession.inviteSessions.find(
      (x) => x.id === inviteSessionId
    );
    const inviteSessionIdx = this.currentMasterSession.inviteSessions.findIndex(
      (x) => x.id === inviteSessionId
    );
    if (!inviteSession) {
      throw new Error(
        `Cannot approve InviteSession id ${inviteSessionId} - InviteSession not found in MasterSession`
      );
    }
    // calculate random active session id
    if (!inviteSession.e2eSession) {
      throw new Error(
        `Bad inviteSessionState: ${inviteSession.id} has no e2eSession to approve`
      );
    }
    let activeSessionId;
    const randomId = await this.simpleCrypto.randomId();

    if (inviteSession.groupMode) {
      activeSessionId = inviteSession.activeSessionId
        ? inviteSession.activeSessionId
        : randomId;
    } else {
      activeSessionId = await this.simpleCrypto.randomId();
    }
    // TODO: make array
    inviteSession.activeSessionId = activeSessionId;
    // create ActiveSession and storeActiveSessionInDB
    this.currentMasterSession.inviteSessions[inviteSessionIdx] = inviteSession;
    const olmGroup = await this.olmGroupService.joinGroupByInvite(
      activeSessionId,
      inviteSession.olmInvite
    );
    const activeSession = new ActiveSession(
      activeSessionId,
      {},
      olmGroup,
      inviteSession.peersPublicKey,
      inviteSession.id
    );
    const serialized = ActiveSession.serialize(activeSession);
    await this.storeActiveSessionInDB(serialized);
    // create direct message for send

    const e2eInviteMessage = {
      olmInvite: this.olmGroupService.inviteToPendingSession(activeSession.id),
      activeSessionId: activeSession.id,
      initData,
    };
    // encrypt e2eInviteMessage using Proteus

    const message = await this.encryptDirectMessage(
      inviteSession.id,
      JSON.stringify(e2eInviteMessage),
      "session:approve"
    );

    // if it's group - reflow prekeys
    if (inviteSession.groupMode) {
      inviteSession = await this.reflowInviteSessionPrekeys(inviteSession);
    }

    return {
      e2eInviteMessage: message.encrypted,
      inviteSession: InviteSession.serialize(inviteSession),
      activeSession: serialized,
      newPrekey: inviteSession.preKeyBundle.serialise(),
      initData,
    };
  }

  async applyToSession(
    sessionId: string,
    sessionPrekey: ArrayBuffer,
    welcomeData?: any
  ): Promise<{ inviteSession: SerializedInviteSession; envelope: Envelope }> {
    return new Promise(async (resolve, reject) => {
      // create olm session for peer
      const olmInvite = this.olmGroupService.createSession(sessionId);
      // TODO: I need InviteSession. copy joinsession impl
      const onSession = (session: Session) => {
        session
          .encrypt(JSON.stringify({ olmInvite, welcomeData }))
          .then((envelope: Envelope) => {
            const inviteSession = new InviteSession(
              sessionId,
              false, //TODO: we don't realy know here
              true,
              true,
              new Date(Date.now()),
              "",
              session.remote_identity.fingerprint(),
              session,
              PreKeyBundle.deserialise(sessionPrekey),
              olmInvite
            );
            this.currentMasterSession.inviteSessions.push(inviteSession);
            resolve({
              inviteSession: InviteSession.serialize(inviteSession),
              envelope,
            });
          });
      };

      this.initSessionFromPrekey(
        this.currentMasterSession.identity,
        PreKeyBundle.deserialise(sessionPrekey)
      ).then(onSession, reject);
    });
  }

  async initAsGuest() {
    let nonce = null;
    if (!this.nonce) {
      nonce = await this.nonceService.generateNonce();
    } else {
      nonce = this.nonce;
    }
    this.nonce = nonce;
    const identity = await this.generateIdentityKeys();
    const device = await this.fingerprintService.generateDevice();
    this.currentMasterSession = new MasterSession(
      identity,
      [],
      [],
      [],
      null,
      device,
      null
    );
    // init olmGroupService (guest mode)
    await this.olmGroupService.init(
      this.currentMasterSession.identity.public_key.fingerprint(),
      this.currentMasterSession.device,
      nonce
    );

    await this.localDatabaseService.init(this.nonce);

    return MasterSession.serialize(this.currentMasterSession);
  }

  addPeerToSession(sessionTicket: SessionTicket): SerializedActiveSession {
    //get activeSession by sessionTicket.id
    const activeSessionId = sessionTicket.activeSessionId;
    const activeSession = this.currentMasterSession.activeSessions.find(
      (x) => x.id === activeSessionId
    );
    if (!activeSession) {
      throw new Error(
        `[MasterSessionService] Not found sessionId ${sessionTicket.activeSessionId}`
      );
    }
    activeSession.olmGroupDescriptor = this.olmGroupService.addPeerToSession(
      activeSessionId,
      sessionTicket.olmInvite
    );
    const serialized = ActiveSession.serialize(activeSession);
    this.storeActiveSessionInDB(serialized);
    return serialized;
  }

  /**
   * Bob received PreKeyBundle from Alice, so he can initSessionFromPrekey and send his welcomeData
   *
   * @param sessionId received from Alice PendinSession object
   * @param preKeyMessageBundle: ArrayBuffer with sessions prekeymessage
   * @param string message - welcomeData
   * @returns inviteSession - Bob should store this object for decrypting messages
   * @returns envelope - PreInitMessage, which Bob should send to Alice to create session from it
   */
  joinSession(
    sessionId: string,
    preKeyMessageBundle: ArrayBuffer,
    message: string
  ): Promise<{ inviteSession: InviteSession; envelope: Envelope }> {
    return new Promise((resolve) => {
      // get custom message in args, pendingSession already have it
      const onSession = (session: Session) => {
        session
          .encrypt(JSON.stringify({ message }))
          .then((envelope: Envelope) => {
            const inviteSession = new InviteSession(
              sessionId,
              false, //TODO: we don't realy know here
              true,
              true,
              new Date(Date.now()),
              "",
              session.remote_identity.fingerprint(),
              session,
              null
            );
            this.currentMasterSession.inviteSessions.push(inviteSession);
            resolve({
              inviteSession: inviteSession,
              envelope,
            });
          });
      };
      // if (!this.currentMasterSession) {
      //   this.createGuestMasterSession()
      //     .then(() => {
      //       return this.initSessionFromPrekey(
      //         this.currentMasterSession.identity,
      //         PreKeyBundle.deserialise(preKeyMessageBundle)
      //       );
      //     })
      //     .then(onSession);
      // } else {
      return this.initSessionFromPrekey(
        this.currentMasterSession.identity,
        PreKeyBundle.deserialise(preKeyMessageBundle)
      ).then(onSession);
      // }
    });
  }

  initSessionFromPrekey(
    ourIdentity: IdentityKeyPair,
    theirPreKeyBundle: PreKeyBundle
  ): Promise<Session> {
    return session.Session.init_from_prekey(ourIdentity, theirPreKeyBundle);
  }

  generatePreKeys(size: number): Promise<PreKey[]> {
    return keys.PreKey.generate_prekeys(0, size);
  }

  initSessionFromPreKeyMessage(
    identity: IdentityKeyPair,
    preKeyStore: PreKeyStore,
    envelope: Envelope
  ): Promise<[Session, Uint8Array]> {
    return session.Session.init_from_message(identity, preKeyStore, envelope);
  }

  decryptDirectMessage(
    sessionId: string,
    envelope: Envelope,
    session?: Session
  ): Promise<DecryptedE2eEnvelope> {
    const pendingSession = this.currentMasterSession.inviteSessions.find(
      (x) => x.id === sessionId
    );
    if (!pendingSession && !session) {
      throw new Error(
        `[MasterSessionService] decryptDirectMessage: Session not ${sessionId} found`
      );
    }
    const e2e = pendingSession ? pendingSession.e2eSession : session;
    return new Promise((resolve) => {
      e2e
        .decrypt(this, envelope)
        .then((serializedEnvelope: Uint8Array) => {
          const decryptedEnvelope = JSON.parse(
            new TextDecoder().decode(serializedEnvelope)
          ) as DecryptedE2eEnvelope;
          const outgoingEnvelope = {
            id: decryptedEnvelope.id,
            content: decryptedEnvelope.content,
            sessionId,
            own: false,
            timestamp: decryptedEnvelope.timestamp,
            type: decryptedEnvelope.type,
          };
          resolve(outgoingEnvelope);
        })
        .catch((e) => console.error(e));
    });
  }

  /**
   * Alice starts new session. Returns InviteSession with PreKeyBundle for Bob to do joinSession()
   */
  startNewSession(
    manualApprove: boolean,
    groupMode: boolean,
    oneTime: boolean
  ): Promise<{
    inviteSession: SerializedInviteSession;
    preKeyBundle: ArrayBuffer;
  }> {
    //masterSession part
    return new Promise((resolve) => {
      keys.PreKey.generate_prekeys(
        this.currentMasterSession.preKeys.length,
        1
      ).then(async (preKeys: PreKey[]) => {
        this.add_prekey(preKeys[0]);
        // return prekey too
        const preKeyBundle = this.generatePreKeyBundle(
          this.currentMasterSession.identity.public_key,
          preKeys[0]
        );
        const id = await this.simpleCrypto.randomId();
        const inviteSession = new InviteSession(
          id,
          manualApprove,
          groupMode,
          oneTime,
          new Date(Date.now()),
          null,
          this.currentMasterSession.identity.public_key.fingerprint(),
          null,
          preKeyBundle
        );
        const serialized = InviteSession.serialize(inviteSession);
        if (this.localDatabaseService) {
          await this.storePendingSession(inviteSession, preKeys[0]);
        }
        const serializedPKeyBundle = preKeyBundle.serialise();
        resolve({
          inviteSession: serialized,
          preKeyBundle: serializedPKeyBundle,
        });
      });
    });
  }

  encryptDirectMessage(
    sessionId: string,
    anyMessage: any,
    type: string
  ): Promise<{ decrypted: DecryptedE2eEnvelope; encrypted: Envelope }> {
    // find session of current id, if not, throw error
    let message = anyMessage;
    if (typeof message !== "string") {
      message = JSON.stringify(message);
    }
    const pendingSession = this.currentMasterSession.inviteSessions.find(
      (x) => x.id === sessionId
    );
    if (!pendingSession) {
      throw new Error(
        `[MasterSessionService] encryptDirectMessage: Session ${sessionId} not found`
      );
    }
    return new Promise(async (resolve) => {
      const randomId = await this.simpleCrypto.randomId();
      const outgoingEnvelope = {
        timestamp: new Date(Date.now()),
        sessionId,
        content: message,
        id: randomId,
        type,
      };
      pendingSession.e2eSession
        .encrypt(JSON.stringify(outgoingEnvelope))
        .then((envelope: Envelope) => {
          // just to return proper data to executor.
          const decryptedEnvelope = {
            content: outgoingEnvelope.content,
            sessionId: pendingSession.id,
            own: true,
            timestamp: outgoingEnvelope.timestamp,
            id: randomId,
            type,
          };
          resolve({
            encrypted: envelope,
            decrypted: decryptedEnvelope,
          });
        });
    });
  }

  reflowInviteSessionPrekeys(
    inviteSession: InviteSession
  ): Promise<InviteSession> {
    return new Promise((resolve) => {
      keys.PreKey.generate_prekeys(
        this.currentMasterSession.preKeys.length,
        1
      ).then((preKeys: PreKey[]) => {
        this.add_prekey(preKeys[0]);
        const preKeyBundle = this.generatePreKeyBundle(
          this.currentMasterSession.identity.public_key,
          preKeys[0]
        );
        inviteSession.preKeyBundle = preKeyBundle;
        const idx = this.currentMasterSession.inviteSessions.findIndex(
          (x) => x.id === inviteSession.id
        );
        this.currentMasterSession.inviteSessions[idx] = inviteSession;
        resolve(inviteSession);
      });
    });
  }

  getCurrentIdentity() {
    return this.currentMasterSession.identity;
  }

  async safeImportMasterSession(
    serializedMasterSession: SerializedMasterSession
  ): Promise<{
    masterSession: SerializedMasterSession;
    importerDevice: Device;
  }> {
    // copy current sync session to imported bundle
    // generate dev fingerprint
    const device = this.currentMasterSession.device;
    const masterSession = await MasterSession.deserialize(
      serializedMasterSession
    );

    const importerDevice = masterSession.device;

    masterSession.device = device;
    return {
      masterSession: MasterSession.serialize(masterSession),
      importerDevice,
    };
  }

  /**
   * Create Master Session with Identity, PreKeyBundle.
   */
  createAndLoadMasterSession(passphrase: string): Promise<{
    masterSession: SerializedMasterSession;
  }> {
    return new Promise(async (resolve, reject) => {
      this.verifyInit();
      try {
        const nonceCipherData = await this.nonceService.encryptAndSealNonce(
          this.nonce,
          passphrase
        );
        await this.localDatabaseService.putDBNonce(
          nonceCipherData.salt,
          nonceCipherData.cipherNonce,
          nonceCipherData.saltSechash
        );
        const serialized = MasterSession.serialize(this.currentMasterSession);

        await this.encryptAndSaveMasterSession(serialized);
        resolve({
          masterSession: serialized
        });
      } catch (e) {
        console.error(
          "[MasterSessionService] Cannot store nonce or master session",
          e
        );
        reject(e);
      }
    });
  }

  private generateIdentityKeys(): Promise<IdentityKeyPair> {
    return keys.IdentityKeyPair.new();
  }
  async exportMasterSessionForOtherDevice(): Promise<SerializedMasterSession> {
    // TODO: calc hash from password
    const masterSession = { ...this.currentMasterSession };

    masterSession.activeSessions.map((activeSession: ActiveSession) => {
      return this.olmGroupService.shareOlmGroup(activeSession.id);
    });

    //remove this device from trusted devices
    masterSession.trustedDevices = masterSession.trustedDevices.filter(
      (x) => x.fingerprint !== masterSession.device.fingerprint
    );

    masterSession.inviteSessions = [];
    const serialized = MasterSession.serialize(masterSession);
    return serialized;
  }

  // async decryptMasterSessionBundle(
  //   backup: Uint8Array,
  //   nonce: string,
  //   passhash: string
  // ): Promise<SerializedMasterSession> {
  //   console.log('decryptMasterSessionBundle', backup, nonce, passhash);
  //   return new Promise<SerializedMasterSession>(async (resolve, reject) => {
  //     // TODO: decrypt session here
  //     // we'll gonna have serialized master session as uint8array
  //     try {
  //       const jsonString = decrypted;
  //       const sms: SerializedMasterSession = JSON.parse(jsonString);
  //       // but we need to store new mastersession in db
  //       let masterSession = await MasterSession.deserialize(sms);
  //       // and load it's dependencies
  //       masterSession = await this.loadMasterSessionDependenciesFromDB(
  //         masterSession
  //       );
  //       const serialized = MasterSession.serialize(masterSession);
  //       await this.encryptAndSaveMasterSession(serialized);

  //       resolve(serialized);
  //     } catch (e) {
  //       reject(e);
  //       throw new Error("Incorrect master session bundle");
  //     }
  //   });
  // }

  async encryptAndSaveMasterSession(
    serializedMasterSession: SerializedMasterSession
  ): Promise<SerializedMasterSession> {
    // // encrypt the session
    // if (!passphrase || passphrase.length === 0) {
    //   throw new Error("Passphrase cannot be empty");
    // }
    // await this.nonceService.encryptAndSealNonce(passphrase);
    this.verifyInit();
    const masterSession: MasterSession = await MasterSession.deserialize(
      serializedMasterSession
    );
    // save guest session to db
    if (masterSession.activeSessions.length > 0) {
      await this.storeActiveSessionInDB(
        serializedMasterSession.activeSessions[0]
      );
    }
    await this.localDatabaseService.dbInstance.masterSessionTable.put({
      id: 0,
      encrypted: JSON.stringify(serializedMasterSession),
    });
    if (this.guestMessagesCache.size > 0) {
      const msgArr = Array.from(this.guestMessagesCache, function (entry) {
        return { key: entry[0], value: entry[1] };
      });
      const promises = msgArr.map(({ key, value }) =>
        this.storeMessage(key, value)
      );
      await Promise.all(promises);
    }
    return serializedMasterSession;
  }

  /**
   * Loads masterSession. If nonce is not provided - current will be used
   * if session exists - current identity will be preserved
   *
   * @param masterSession
   * @param nonce
   */
  async loadMasterSession(
    masterSession: SerializedMasterSession,
    nonce: string
  ): Promise<void> {
    if (!this.currentMasterSession || !this.currentMasterSession.identity) {
      this.currentMasterSession = MasterSession.deserialize(masterSession);
    } else {
      // preserve current identity
      const currentIdentity = this.currentMasterSession.identity;
      this.currentMasterSession = MasterSession.deserialize(masterSession);
      this.currentMasterSession.identity = currentIdentity;
    }

    await this.olmGroupService.init(
      this.currentMasterSession.identity.public_key.fingerprint(),
      this.currentMasterSession.device,
      this.nonce,
      this.currentMasterSession.trustedDevices
    );
    this.currentMasterSession.activeSessions.forEach((activeSession) => {
      console.log(
        "[MasterSessionService] importing active session",
        activeSession.id,
        activeSession.olmGroupDescriptor,
        this.nonce
      );
      this.olmGroupService.loadOlmGroup(
        activeSession.id,
        activeSession.olmGroupDescriptor,
        nonce
      );
    });
    this.currentMasterSession.inviteSessions.forEach((inviteSesion) => {
      if (inviteSesion.olmInvite) {
        this.olmGroupService.loadOlmInvite(
          inviteSesion.id,
          inviteSesion.olmInvite
        );
      }
    });
    this.olmGroupService.loadIdentity(
      this.currentMasterSession.identity.public_key.fingerprint(),
      this.currentMasterSession.trustedDevices
    );
    await this.encryptAndSaveMasterSession(masterSession);
  }

  async updateMasterSessionMeta(
    metadataMap: MetadataMap
  ): Promise<MasterSession> {
    this.currentMasterSession.meta = metadataMap;
    const serializedMasterSession: SerializedMasterSession =
      MasterSession.serialize(this.currentMasterSession);
    try {
      await this.localDatabaseService.dbInstance.masterSessionTable.update(0, {
        encrypted: JSON.stringify(serializedMasterSession),
      });
    } catch (e) {
      console.error(e);
      throw new Error(e);
    }

    return this.currentMasterSession;
  }

  storeMessage(id: string, content: DecryptedOlmEnvelope): Promise<void> {
    return new Promise(async (resolve, reject) => {
      if (this.localDatabaseService) {
        const messageObj = {
          id,
          encrypted: content,
          activeSession: content.sessionId,
          olmSid: content.olmSid,
          masterSession: 1,
        };
        const res = await this.localDatabaseService.dbInstance.messages.put(
          messageObj
        );
        resolve();
      } else {
        this.guestMessagesCache.set(id, content);
        resolve();
      }
    });
  }

  getMessage(messageId: string): Promise<DecryptedOlmEnvelope> {
    return new Promise((resolve) => {
      if (this.localDatabaseService) {
        return this.localDatabaseService.dbInstance.messages
          .get(messageId)
          .then((x) => x.encrypted)
          .then((x) => {
            x.timestamp = new Date(x.timestamp);
            return x;
          })
          .catch((e) => resolve(null));
      } else {
        resolve(this.guestMessagesCache.get(messageId));
      }
    });
  }

  async getMessagesForSession(
    activeSessionId: string,
    limit = 50,
    skipLast = 0
  ): Promise<{ messages: DecryptedOlmEnvelope[]; count: number }> {
    return new Promise(async (resolve, reject) => {
      if (this.localDatabaseService) {
        const chunk = await this.localDatabaseService.dbInstance.messages
          .where("activeSession")
          .equals(activeSessionId)
          .reverse() // to get messages in descending order
          .offset(skipLast) // to skip last `skipLast` messages
          .limit(limit) // to get only `limit` messages
          .sortBy("id")
          .then((arr) => {
            return arr
              .map((x) => x.encrypted)
              .map((x) => {
                x.timestamp = new Date(x.timestamp);
                return x;
              });
          });
        const count =
          await this.localDatabaseService.dbInstance.messages.count();
        resolve({ messages: chunk, count });
      } else {
        if (
          this.currentMasterSession.activeSessions[0] &&
          activeSessionId === this.currentMasterSession.activeSessions[0].id
        ) {
          const msgArr = Array.from(this.guestMessagesCache, function (entry) {
            return entry[1];
          })
            .slice(-skipLast)
            .reverse()
            .slice(0, limit);
          resolve({
            messages: msgArr,
            count: msgArr.length,
          });
        } else {
          // no active session yet
          resolve({ messages: [], count: 0 });
        }
      }
    });
  }

  async storePendingSession(
    session: InviteSession,
    preKey?: PreKey
  ): Promise<string> {
    if (preKey) {
      await this.addPrekeyToMasterSession(0, preKey);
    }
    const currentIdx = this.currentMasterSession.inviteSessions.findIndex(
      (x) => x.id === session.id
    );
    if (currentIdx === -1) {
      this.currentMasterSession.inviteSessions.push(session);
    } else {
      this.currentMasterSession.inviteSessions[currentIdx] = session;
    }
    let dbEntity = null;
    let newId = null;
    try {
      dbEntity = await this.localDatabaseService.dbInstance.pendingSessions.get(
        session.id
      );
      newId = dbEntity.id;
      this.localDatabaseService.dbInstance.pendingSessions.update(newId, {
        encrypted: JSON.stringify(InviteSession.serialize(session)),
      });
    } catch (error) {
      newId = this.localDatabaseService.dbInstance.pendingSessions.put({
        id: session.id,
        encrypted: JSON.stringify(InviteSession.serialize(session)),
        masterSession: 1,
      });
    }
    return newId;
  }
  /**
   * Stores current session in database. Overwrites if exists
   * @param {SerializedActiveSession} session
   */
  async storeActiveSessionInDB(
    serializedSession: SerializedActiveSession
  ): Promise<void> {
    return new Promise(async (resolve, reject) => {
      try {
        const session = await this.deserializeActiveSession(serializedSession);
        const idx = this.currentMasterSession.activeSessions.findIndex(
          (x) => x.id === session.id
        );
        if (idx === -1) {
          this.currentMasterSession.activeSessions.push(session);
        } else {
          this.currentMasterSession.activeSessions[idx] = session;
        }

        try {
          const pendingSession =
            await this.localDatabaseService.dbInstance.pendingSessions.get(
              session.id
            );
          await this.localDatabaseService.dbInstance.pendingSessions.delete(
            pendingSession.id
          );
        } catch (e) {
          console.log("pending session not found");
        }
        const encrypted = JSON.stringify(ActiveSession.serialize(session));
        try {
          const dbSession =
            await this.localDatabaseService.dbInstance.activeSessions.get(
              session.id
            );
          if (dbSession) {
            await this.localDatabaseService.dbInstance.activeSessions.update(
              session.id,
              {
                encrypted,
              }
            );
            resolve();
          } else {
            await this.localDatabaseService.dbInstance.activeSessions.put({
              id: session.id,
              encrypted,
              masterSession: 1,
            });
            resolve();
          }
        } catch (e) {
          try {
            await this.localDatabaseService.dbInstance.activeSessions.put({
              id: session.id,
              encrypted,
              masterSession: 1,
            });
            resolve();
          } catch (e) {
            console.error("[MasterSessionService] error catched");
            console.error(e);
            reject(e);
          }
        }
      } catch (e) {
        console.error(
          `ERROR: MasterSessionService:982: cannot save ActiveSession metadata`
        );
        console.error(e);
        reject(e);
      }
    });
  }

  async getEncryptedMasterSession(
    passphrase: string,
    nonce?: string | Uint8Array
  ): Promise<{ ciphertext: Uint8Array; salt: Uint8Array }> {
    return new Promise(async (resolve) => {
      await this.simpleCrypto.init(APP_NAME, passphrase);

      const toBundle = JSON.stringify(
        MasterSession.serialize(this.currentMasterSession)
      );

      // TODO: I need to init db elsewhere

      if (typeof nonce === "string") {
        nonce = await ArrayBufferUtilsService.b64stringToUint8Array(nonce);
      }
      const encryptedBundle = await this.simpleCrypto.encrypt(toBundle, nonce);

      resolve(encryptedBundle);
    });
  }

  // TODO: it can't find activesession if it's guest session
  getActiveSessionById(sessionId: string): ActiveSession {
    return this.currentMasterSession.activeSessions.find(
      (x) => x.id === sessionId
    );
  }

  // async getSerializedMasterSessionFromDb(): Promise<SerializedMasterSession> {
  //   return new Promise(async (resolve, reject) => {
  //     return this.localDatabaseService.masterSessionTable
  //       .toArray()
  //       .then(async (decryptedMasterSession: any[]) => {
  //         const masterSession = await MasterSession.deserialize(
  //           JSON.parse(decryptedMasterSession[0].encrypted)
  //         );
  //         // load pending and active sessions
  //         const activeSessions =
  //           await this.localDatabaseService.activeSessions.toArray();
  //         // TODO: cannot decrypt activeSessions
  //         const pendingSessions =
  //           await this.localDatabaseService.pendingSessions.toArray();
  //         await this.olmGroupService.init(
  //           masterSession.identity.public_key.fingerprint(),
  //           masterSession.device,
  //           this.nonce,
  //           masterSession.trustedDevices
  //         );
  //         masterSession.activeSessions = await Promise.all(
  //           activeSessions.map((activeSessionEnc) => {
  //             return ActiveSession.deserialize(
  //               JSON.parse(activeSessionEnc.encrypted)
  //             );
  //           })
  //         );
  //         masterSession.inviteSessions = pendingSessions.map(
  //           (pendingSession) => {
  //             return InviteSession.deserialize(
  //               masterSession.identity,
  //               JSON.parse(pendingSession.encrypted)
  //             );
  //           }
  //         );
  //         await this.olmGroupService.init(
  //           masterSession.identity.public_key.fingerprint(),
  //           masterSession.device,
  //           this.nonce,
  //           masterSession.syncStatus
  //             ? Object.values(masterSession.syncStatus.approvedDevices.entities)
  //             : null
  //         );
  //         resolve(MasterSession.serialize(masterSession));
  //       })
  //       .catch((e) => {
  //         console.error("Wrong passphrase", e);
  //         reject("Wrong passphrase");
  //       });
  //   });
  // }

  dumpSession() {
    throw new Error("to be implemented");
  }

  sessionExists() {
    return LocalDatabaseService.sessionExists();
  }

  private async addPrekeyToMasterSession(
    masterSessionId: number,
    preKey: PreKey
  ) {
    const exists = await this.sessionExists();
    if (this.localDatabaseService && exists) {
      const serializedMasterSession =
        await this.localDatabaseService.dbInstance.masterSessionTable.get(
          masterSessionId
        );
      const masterSession = await MasterSession.deserialize(
        JSON.parse(serializedMasterSession.encrypted)
      );
      masterSession.preKeys.push(preKey);
      this.currentMasterSession = masterSession;
      return this.localDatabaseService.dbInstance.masterSessionTable.update(1, {
        encrypted: JSON.stringify(MasterSession.serialize(masterSession)),
      });
    } else {
      this.currentMasterSession.preKeys.push(preKey);
    }
  }

  // private createGuestMasterSession() {
  //   // create local in-memory masterSession with identity to use without touching db. Convenience feature
  //   Promise.all([
  //     this.generateIdentityKeys(),
  //     this.fingerprintService.generateDevice(),
  //   ]).then(async ([identity, device]: [IdentityKeyPair, Device]) => {
  //     await this.olmGroupService.init(
  //       identity.public_key.fingerprint(),
  //       device
  //     );
  //     this.currentMasterSession = new MasterSession(
  //       identity,
  //       [],
  //       [],
  //       [],
  //       initialState,
  //       device,
  //       []
  //     );
  //     this.nonce = RandomService.randomString(32);
  //     const serialized = MasterSession.serialize(
  //       this.currentMasterSession
  //     );
  //     return serialized;
  //   });
  // }

  private deserializeActiveSession(
    serializedActiveSession: SerializedActiveSession
  ) {
    try {
      return ActiveSession.deserialize(serializedActiveSession);
    } catch (e) {
      console.error(`Identity`, this.currentMasterSession.identity);
      console.error(`SerializedActiveSession`, serializedActiveSession);
      throw new Error(
        `[MasterSessionService] Cannot deserialize ActiveSession using current Identity`
      );
    }
  }
}
