export interface DBNonceCipher {
  masterSessionTableId: number;
  salt: string;
  cipherNonce: string;
  saltSechash: string;
}
