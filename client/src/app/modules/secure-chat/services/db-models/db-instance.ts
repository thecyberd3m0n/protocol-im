import { encrypted } from "@pvermeer/dexie-encrypted-addon";
import Dexie from "dexie";
import { DBMessage } from "./message.dbmodel";
import { DBNonceCipher } from "./nonce-cipher.dbmodel";

export const DB_VERSION = 5;

export class DbInstance extends Dexie {
  masterSessionTable: Dexie.Table<{ id: number; encrypted: string }, number>;
  nonceCipher: Dexie.Table<DBNonceCipher, number>;
  messages: Dexie.Table<DBMessage, string>;
  activeSessions: Dexie.Table<
    { id: string; encrypted: string; masterSession: number },
    string
  >;
  pendingSessions: Dexie.Table<
    { id: string; encrypted: string; masterSession: number },
    string
  >;
  guestMasterSession: Dexie.Table<
    {
      id: number;
      encrypted: string;
      timestamp: number;
    },
    number
  >;
  notificationsState: Dexie.Table<{ id: number; token: string }, number>;

  static databaseDescriptor = {
    masterSession: "#id, $encrypted",
    messages: "#id, masterSession, $encrypted, activeSession",
    activeSessions: "#id, $encrypted, masterSession",
    pendingSessions: "#id, $encrypted, masterSession",
    nonceCipher: "#masterSessionTableId, salt, nonce, saltSechash",
    guestMasterSession: "#id, $encrypted, timestamp",
    notificationsState: "#id, token, timestamp"
  };

  secret: string;
  constructor(secret: string) {
    super("secure-chat");
    const key = btoa(secret).substring(0, 43) + "=";

    encrypted(this, { secretKey: key });

    this.version(DB_VERSION).stores(DbInstance.databaseDescriptor);

    this.masterSessionTable = this.table("masterSession");
    this.messages = this.table("messages");
    this.activeSessions = this.table("activeSessions");
    this.pendingSessions = this.table("pendingSessions");
    this.nonceCipher = this.table("nonceCipher");
    this.guestMasterSession = this.table("guestMasterSession");
    this.notificationsState = this.table("notificationsState");
  }
}
