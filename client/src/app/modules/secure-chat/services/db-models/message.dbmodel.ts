import { DecryptedOlmEnvelope } from "../../models/decryptedOlmEnvelope.model";

export interface DBMessage {
  id: string;
  activeSession: string;
  encrypted: any;
  masterSession: number;
  olmSid: string;
}
