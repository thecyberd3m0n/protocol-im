import { Injectable } from "@angular/core";
import * as sodium from "libsodium-wrappers-sumo";
import { derived } from "@wireapp/proteus";
import { ArrayBufferUtilsService } from "./arrayBufferUtils.service";

export const RANDOM_ID_CHARS_LENGTH = 32;

@Injectable({
  providedIn: "root",
})
export class SimpleCryptoService {
  derivedSecret: derived.DerivedSecrets;
  appName: string;
  sodium;
  constructor() {}
  async init(appName: string, secret?: string): Promise<string> {
    let secretKey;
    sodium.ready.then(
      () => {
        this.sodium = sodium;
      },
      (err) => {
        console.error(err);
      }
    );

    if (secret) {
      secretKey = this.sodium.from_string(secret);
      this.derivedSecret = derived.DerivedSecrets.kdf_without_salt(
        secretKey,
        appName
      );
    }
    return secret;
  }

  validateInit() {
    if (!this.sodium) {
      throw new Error("SimpleCrypto not initialized");
    }
  }

  async encrypt(
    plainText: string,
    nonce?: Uint8Array
  ): Promise<{ salt: Uint8Array; ciphertext: Uint8Array }> {
    if (!this.derivedSecret) {
      throw new Error("Secret key is not initialized");
    }
    if (!nonce) {
      nonce = this.sodium.randombytes_buf(8);
    }

    const encrypted = this.derivedSecret.cipher_key.encrypt(
      this.sodium.from_string(plainText),
      nonce
    );

    return {
      salt: nonce,
      ciphertext: encrypted,
    };
  }

  async decrypt(encryptedData: {
    ciphertextStr: string;
    nonceStr: string;
  }): Promise<string> {
    if (!this.derivedSecret) {
      throw new Error("Secret key is not initialized");
    }
    const ciphertext = await ArrayBufferUtilsService.b64stringToUint8Array(encryptedData.ciphertextStr);
    const nonce = await ArrayBufferUtilsService.b64stringToUint8Array(encryptedData.nonceStr);

    const decrypted = this.derivedSecret.cipher_key.decrypt(ciphertext, nonce);
    return this.sodium.to_string(decrypted);
  }

  async randomId(): Promise<string> {
    return this.randomString(RANDOM_ID_CHARS_LENGTH);
  }

  async randomString(chars: number): Promise<string> {
    this.validateInit();
    return this.sodium.to_hex(this.sodium.randombytes_buf(chars));
  }

  randomNumber(length: number) {
    return Math.ceil(Math.random() * length);
  }

  async secHash(str, salt?: Uint8Array): Promise<{ hash: string, salt: string}> {
    return new Promise(async(resolve) => {
      this.validateInit();
      if (!salt) {
        salt = this.sodium.randombytes_buf(this.sodium.crypto_pwhash_SALTBYTES);
      }
      const hash: Uint8Array = this.sodium.crypto_pwhash(
        32,
        str,
        salt,
        this.sodium.crypto_pwhash_OPSLIMIT_INTERACTIVE,
        this.sodium.crypto_pwhash_MEMLIMIT_INTERACTIVE,
        this.sodium.crypto_pwhash_ALG_ARGON2ID13
      );
      const hashString = await ArrayBufferUtilsService.uint8ArrayToB64String(hash);
      const saltString = await ArrayBufferUtilsService.uint8ArrayToB64String(salt);
      resolve({ hash: hashString, salt: saltString });
    });
  }

}
