import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { MetadataMap } from '../models/metadataMap.model';
import * as SecureChatAction from '../store/actions/secure-chat.actions';
import { SecureChatModuleState, SessionConfig } from '../store/reducers/secure-chat.reducer';
import { OutgoingMessageModel } from './../models/outgoingMessage.model';
import { UnsignedProfile } from './../models/unsignedProfile.model';

@Injectable({
  providedIn: 'root',
})
export class SecureChatDispatchers {
  init() {
    this.store.dispatch(new SecureChatAction.Init());
  }
  
  joinSession(joinSessionData: { welcomeData?: string, sessionId: string }) {
    this.store.dispatch(new SecureChatAction.ApplyForSession(joinSessionData));
  }

  startSession(sessionConfig: SessionConfig) {
    this.store.dispatch(new SecureChatAction.StartSession({ manualApprove: sessionConfig.manualApprove, groupMode: sessionConfig.groupMode, oneTime: sessionConfig.oneTime }) );
  }

  setupProfile(unsignedProfile: UnsignedProfile) {
    this.store.dispatch(new SecureChatAction.SetupProfile(unsignedProfile));
  }

  outgoingMessage(message: OutgoingMessageModel) {
    this.store.dispatch(new SecureChatAction.OutgoingMessage(message));
  }

  dumpSession() {
    this.store.dispatch(new SecureChatAction.DumpSession());
  }

  unlockMasterSession(passphrase: string) {
    this.store.dispatch(new SecureChatAction.UnlockMasterSession(passphrase));
  }

  lockMasterSession() {
    this.store.dispatch(new SecureChatAction.LockMasterSession());
  }

  loadMessages(sessionId: string, limit: number, skipLast: number) {
    this.store.dispatch(new SecureChatAction.LoadMessages({ sessionId, limit, skipLast }));
  }

  updateMasterSessionMeta(type: string, meta: MetadataMap) {
    this.store.dispatch(new SecureChatAction.UpdateMasterSessionMeta({ type, meta }))
  }

  updateActiveSessionMeta(sessionId: string, type: string, meta: MetadataMap) {
    this.store.dispatch(new SecureChatAction.UpdateActiveSessionMeta({
      sessionId,
      type,
      meta
    }));
  }

  constructor(private store: Store<SecureChatModuleState>) { }
}
