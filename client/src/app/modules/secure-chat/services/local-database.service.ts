import { Injectable } from "@angular/core";
import Dexie from "dexie";
import { DB_VERSION, DbInstance } from "./db-models/db-instance";
import { DBNonceCipher } from "./db-models/nonce-cipher.dbmodel";
import { NonceCipherData } from "./nonce.service";

@Injectable()
export class LocalDatabaseService {
  dbInstance: DbInstance;
  async init(secret: string): Promise<void> {
    return new Promise((resolve, reject) => {
      console.log('[LocalDatabaseService] LocalDatabase inited with secret', secret);
      this.dbInstance = new DbInstance(secret);
    
      this.dbInstance.open()
        .then(() => {
          resolve();
        })
        .catch((e) => {
          console.error(e);
          reject(e);
        });
    });
  }

  async putDBNonce(
    saltNonce: string,
    cipherNonce: string,
    saltSechash: string
  ): Promise<any> { // TODO: typing
    return new Promise(async (resolve, reject) => {
      let currentNonceCipher = null;
      try {
        currentNonceCipher = await this.dbInstance.nonceCipher.get(0);
        if (currentNonceCipher) {
          await this.dbInstance.nonceCipher.update(0, {
            cipherNonce: cipherNonce,
            salt: saltNonce,
          });
        } else {
          await this.dbInstance.nonceCipher.put({
            cipherNonce: cipherNonce,
            salt: saltNonce,
            masterSessionTableId: 0,
            saltSechash
          });
          currentNonceCipher = await this.dbInstance.nonceCipher.get(0);
        }
      } catch (e) {
        try {
          await this.dbInstance.nonceCipher.put({
            cipherNonce: cipherNonce,
            salt: saltNonce,
            masterSessionTableId: 0,
            saltSechash
          });
          currentNonceCipher = await this.dbInstance.nonceCipher.get(0);
      
        } catch (e) {
          reject('[LocalDatabaseService]: Error when putting nonceCipher');
          console.error(e);
          return;
        }
      }
      resolve(currentNonceCipher);
    });
  }

  async getNonceCipher(): Promise<NonceCipherData | null> {
    const db = new Dexie("secure-chat");
    db.version(DB_VERSION).stores(DbInstance.databaseDescriptor);
    const nonceCipherTable = db.table("nonceCipher");
    await db.open();
    const nonceCipher: DBNonceCipher = await nonceCipherTable.get(0);
    if (!nonceCipher) {
      return null;
    }
    
    return nonceCipher;
  }

  static async sessionExists(): Promise<boolean> {
    const db = new Dexie("secure-chat");
    return new Promise((resolve) => {
      db.version(DB_VERSION).stores(DbInstance.databaseDescriptor);
      const masterSessionTable = db.table("masterSession");
      db.open().then(
        () => {
          return masterSessionTable.toArray().then(async (array) => {
            await db.close();
            resolve(array.length > 0);
          });
        },
        (err) => {
          console.error(err);
          resolve(false);
        }
      );
    });
  }
}
