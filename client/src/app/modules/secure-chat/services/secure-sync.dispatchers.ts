import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { SecureSyncState } from "../store/reducers/secure-sync.reducer";
import * as SecureSyncAction from '../store/actions/secure-sync.actions';

@Injectable()
export class SecureSyncDispatchers {
    createSyncSession() {
        this.store.dispatch(new SecureSyncAction.CreateSyncSession())
    }

    connectDevice(syncId: string) {
        this.store.dispatch(new SecureSyncAction.AskForSyncSession({ sessionId: syncId }));
    }

    unlockImport(passphrase: string) {
        this.store.dispatch(new SecureSyncAction.UnlockImport({ passphrase }));
    }

    approveSyncSession(pendingSessionId: string, passphrase: string, fingerprint: string) {
        this.store.dispatch(new SecureSyncAction.ApproveSyncSessionJoin({ pendingSessionId, passphrase, fingerprint }));
    }

    rejectSyncSession(pendingSessionId: string) {
        this.store.dispatch(new SecureSyncAction.RejectSyncSessionJoin({ pendingSessionId }));
    }

    constructor(private store: Store<SecureSyncState>) {}
}
