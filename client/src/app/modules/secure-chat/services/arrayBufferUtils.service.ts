import { Injectable } from "@angular/core";

@Injectable()
export class ArrayBufferUtilsService {
  static arraybufferToString(buf: ArrayBuffer): string {
    const result = btoa(
      new Uint8Array(buf).reduce(
        (data, byte) => data + String.fromCharCode(byte),
        ""
      )
    );
    return result;
  }

  static stringToArraybuffer(str: string): ArrayBuffer {
    const asciiString = atob(str);
    const result = new Uint8Array(
      [...asciiString].map((char) => char.charCodeAt(0))
    ).buffer;
    return result;
  }

  static uint8ArrayToBuffer(array: Uint8Array): ArrayBuffer {
    const result = array.buffer.slice(
      array.byteOffset,
      array.byteLength + array.byteOffset
    );
    return result;
  }

  static async b64stringToUint8Array(base64String: string): Promise<Uint8Array> {
    const binaryString = atob(base64String);
    const uint8Array = new Uint8Array(binaryString.length);
    for (let i = 0; i < binaryString.length; i++) {
      uint8Array[i] = binaryString.charCodeAt(i);
    }
    return uint8Array;
  }

  static async uint8ArrayToB64String(uint8Array: Uint8Array): Promise<string> {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();

      fileReader.onload = function () {
        // get the result as a string
        const base64String = fileReader.result as string;

        // remove the data URL prefix
        const base64StringWithoutPrefix = base64String.replace(
          /^data:.*;base64,/,
          ""
        );

        resolve(base64StringWithoutPrefix);
      };

      fileReader.onerror = function (error) {
        reject(error);
      };

      // read the Uint8Array as a Blob
      const blob = new Blob([uint8Array.buffer]);

      // read the Blob using the FileReader
      fileReader.readAsDataURL(blob);
    });
  }
}
