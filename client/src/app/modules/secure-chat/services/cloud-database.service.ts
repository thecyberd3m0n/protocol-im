import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import { Observable } from "rxjs/internal/Observable";
import { filter, first, map, skip, take, tap } from "rxjs/operators";
import { StoredPrekeybundle } from "../models/storedPreKeyBundle.model";
@Injectable()
export class CloudDatabaseService {
  lastMessageId: string;
  preKeyLock: boolean;
  constructor(private db: AngularFireDatabase) {}

  /**
   *
   * @param sessionId
   */
  listenSessionForOlmMessages$(
    sessionId: string
  ): Observable<{ [x: string]: { olmSid: string, message: string } }> {

    console.log(`listening ${sessionId} session for olm message`);
    return this.getOlmRef(sessionId)
      .snapshotChanges()
      .pipe(
        map((x) => x.payload.val()),
        filter((x) => !!x && Object.keys(x)[0] !== this.lastMessageId),
        // tap((x) =>
        //   this.getSingleMessageRef(sessionId, Object.keys(x)[0]).remove()
        // ),
        map((cipherText: { [key: string]: { olmSid: string, message: string } }) => {
          const id = Object.keys(cipherText)[0];
          console.log(`MessageService: got olm message id ${id} from cloud`);
          return {
            [id]: cipherText[id],
          };
        })
      );
  }

  listenSessionForE2eMessages$(
    sessionId: string
  ): Observable<{ [x: string]: ArrayBuffer | SharedArrayBuffer }> {
    return this.getMessagesRef(sessionId)
      .snapshotChanges()
      .pipe(
        map((x) => x.payload.val()),
        filter((x) => !!x && Object.keys(x)[0] !== this.lastMessageId),
        // tap((x) =>
        //   this.getSingleMessageRef(sessionId, Object.keys(x)[0]).remove()
        // ),
        map(
          (cipherText: {
            [key: string]: {
              buffer: Array<number>;
            };
          }) => {
            const id = Object.keys(cipherText)[0];
            console.log(`MessageService: got e2e message id ${id} from cloud`);
            return {
              [id]: new Uint8Array(cipherText[id].buffer).buffer,
            };
          }
        )
      );
  }

  listenSessionForPreKeyMessage$(
    sessionId: string
  ): Observable<{ id; prekeymessage: ArrayBuffer }> {
    console.log(`listening ${sessionId} session for prekeymessage`);
    return this.getSessionsRef(sessionId)
      .snapshotChanges()
      .pipe(
        map((x) => x.payload.val()),
        filter((x) => !!x && (x as any).e2e && !this.preKeyLock),
        tap(x => {
          console.log('prekeymsg', x);
        }),
        // TODO: check if previous messages are not needed to decrypt last one
        // tap(() => this.getSessionPrekeys(sessionId).remove()),
        map((snapshot: { [keys: string]: Array<number> }) => {
          return {
            id: sessionId,
            prekeymessage: Uint8Array.from(
              snapshot.e2e[Object.keys(snapshot.e2e)[0]].buffer
            ).buffer,
          };
        })
      );
  }

  uploadSessionPreKeyMessage(
    sessionId: string,
    preKeyMessage: ArrayBuffer
  ): Promise<void> {
    const sessionRef = this.getSessionsRef(sessionId);
    this.preKeyLock = true;
    return new Promise((resolve) => {
      sessionRef
        .snapshotChanges()
        .pipe(take(1))
        .subscribe(() => {
          resolve();
        });
      return sessionRef.set({
        // prekeybundle: {},
        prekeymessage: new Uint8Array(preKeyMessage),
      }).then((res) => {
        this.preKeyLock = false;
      });
    });
  }

  downloadSessionPreKey(sessionId: string): Promise<ArrayBuffer> {
    return this.getSessionsRef(sessionId)
      .snapshotChanges()
      .pipe(
        take(1),
        map(
          (snapshot) =>
            (snapshot.payload.val() as StoredPrekeybundle).prekeymessage
        ),
        map((snapshot) => Uint8Array.from(snapshot).buffer)
      )
      .toPromise();
  }

  uploadSessionPrekey(id: string, preKeyBundle: ArrayBuffer): Promise<void> {
    const sessionRef = this.getSessionsRef(id);
    return new Promise((resolve) => {
      sessionRef
        .snapshotChanges()
        .pipe(take(1))
        .subscribe(() => {
          resolve();
        });
      sessionRef.set({
        prekeymessage: new Uint8Array(preKeyBundle),
      });
    });
  }
  
  // TODO: make it observable
  importSyncFile(syncId: string): Observable<Uint8Array> {
    return this.getSyncSessionRef(syncId)
      .valueChanges()
      .pipe(
        first(),
        map((ab: ArrayBuffer) => {
          return new Uint8Array(ab);
        })
      );
  }

  sendOlmMessage(
    sessionId: string,
    message: string,
    id: string,
    olmSid: string
  ): Promise<string> {
    this.lastMessageId = id;
    const messageRef = this.getSessionsRef(sessionId);
    return new Promise((resolve) => {
      messageRef
        .snapshotChanges()
        .pipe(take(1))
        .subscribe(() => {
          resolve(id);
        });
      messageRef.set({
        olm: {
          [id]: {
            olmSid,
            message
          },
        },
      });
    });
  }

  sendE2eMessage(
    sessionId: string,
    message: ArrayBuffer,
    id: string
  ): Promise<string> {
    this.lastMessageId = id;
    const messageRef = this.getSessionsRef(sessionId);
    return new Promise((resolve) => {
      messageRef
        .snapshotChanges()
        .pipe(take(1))
        .subscribe(() => {
          resolve(id);
        });
      messageRef.update({
        e2e: {
          [id]: {
            buffer: new Uint8Array(message),
          },
        },
      });
    });
  }

  createBackup(backupId: string, backup: ArrayBuffer): Promise<void> {
    return this.getSyncSessionRef(backupId).update(backup);
  }

  /**
   *
   * @param syncId should be random
   * @returns reference to backup
   */
  getSyncSessionRef(syncId: string) {
    return this.db.object(`sync/${syncId}`);
  }

  getSingleMessageRef(sessionId: string, messageId: string) {
    return this.db.object(`sessions/${sessionId}/e2e/${messageId}`);
  }

  getSingleMessageById(sessionId: string, messageId: string) {
    return this.db.object(`sessions/${sessionId}/e2e/${messageId}`);
  }

  getOlmMessageRef(sessionId: string) {
    return this.db.object(`sessions/${sessionId}/olm`);
  }

  getSessionPrekeys(sessionId: string) {
    return this.db.object(`sessions/${sessionId}/prekeymessage`);
  }

  getSessionsRef(sessionId: string) {
    return this.db.object(`sessions/${sessionId}`);
  }

  // Type it and get original object
  getMessagesRef(sessionId: string) {
    return this.db.object(`sessions/${sessionId}/e2e`);
  }

  getOlmRef(sessionId: string) {
    return this.db.object(`sessions/${sessionId}/olm`);
  }
}
