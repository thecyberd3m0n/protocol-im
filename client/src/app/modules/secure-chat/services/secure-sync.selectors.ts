import { Injectable } from "@angular/core";
import { createFeatureSelector, createSelector, select, Store } from "@ngrx/store";
import { distinctUntilChanged, filter } from "rxjs/operators";
import { unapprovedDevicesAdapter } from "../adapters/unapprovedDevices.adapter";
import { SecureChatModuleState } from "../store/reducers/secure-chat.reducer";
import { SecureSyncState } from "../store/reducers/secure-sync.reducer";
import { approvedDevicesAdapter } from "../adapters/approvedDevices.adapter";
import { Device } from "../models/device.model";
import { Observable } from "rxjs";

const getSecureChatState =
  createFeatureSelector<SecureChatModuleState>("secure-chat");

const getSyncGlobal = createSelector(
    getSecureChatState,
    (state: SecureChatModuleState) => {
        return state.secureSyncState;
    }
)

const getSyncState = () => createSelector(
    getSyncGlobal, state => state.syncState
)
const getInviteSyncId = () => createSelector(
    getSyncGlobal, state => state.syncInviteSessionId
)

const getSyncActiveSessionId = () => createSelector(
    getSyncGlobal, state => state.syncActiveSessionId
)

const getUnapprovedDevices = createSelector(getSyncGlobal, state => state.unapprovedDevices);

const selectUnapprovedDevices = () => createSelector(
    getUnapprovedDevices, state => {
        return unapprovedDevicesAdapter.getSelectors().selectAll(state);
    }
)
const getApprovedDevices = createSelector(getSyncGlobal, state => state.approvedDevices);

const selectApprovedDevices = () => createSelector(
    getApprovedDevices, state => {
        return approvedDevicesAdapter.getSelectors().selectAll(state);
    }
)

const getMasterSessionToLoad = createSelector(getSyncGlobal, state => state.masterSessionToLoad);

const selectMasterSessionToLoad = () => createSelector(
    getMasterSessionToLoad, state => state
)

const getNonceCipherToLoad = createSelector(getSyncGlobal, state => state.nonceCipherToLoad);

const selectNonceCipherToLoad = () => createSelector(
    getNonceCipherToLoad, state => state
)

const getLastLoadReport = createSelector(getSyncGlobal, state => state.lastLoadReport);

const getDevice = () => createSelector(getSyncGlobal, state => state.device);

const selectUnapprovedDeviceByFingerprint = createSelector(
    getUnapprovedDevices,
    (entities, props) => {
        const device = ((Object.values(entities.entities)) as any[]).find(x => x.device.fingerprint === props.fingerprint);
        return device ? device.device : null;
    }
)

@Injectable()
export class SecureSyncSelectors {
    constructor(private store: Store<SecureSyncState>) {}
    public global$ = this.store.pipe(select(getSyncGlobal));
    public state$ = this.store.pipe(select(getSyncState()))
    public syncInviteSessionId$ = this.store.pipe(select(getInviteSyncId()),filter(x => !!x),distinctUntilChanged());
    public syncActiveSessionId$ = this.store.pipe(select(getSyncActiveSessionId()),distinctUntilChanged());
    public unapprovedDevices$ = this.store.pipe(select(selectUnapprovedDevices()),distinctUntilChanged());
    public approvedDevices$ = this.store.pipe(select(selectApprovedDevices()), distinctUntilChanged());
    public masterSessionToLoad$ = this.store.pipe(select(selectMasterSessionToLoad()));
    public nonceCipher$ = this.store.pipe(select(selectNonceCipherToLoad()))
    public lastLoadReport$ = this.store.pipe(select(getLastLoadReport));
    public getDevice$ = this.store.pipe(select(getDevice()));

    getUnapprovedDeviceByFingerprint(fingerprint: string): Observable<Device> {
        return this.store.pipe(select(selectUnapprovedDeviceByFingerprint, { fingerprint }))
    }
}
