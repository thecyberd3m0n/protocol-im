import { ArrayBufferUtilsService } from "./arrayBufferUtils.service";

describe("ArrayBufferUtilsService", () => {
  it("should convert Uint8Array to string", async () => {
    const uint8Array = new Uint8Array([137, 97, 217, 17, 250, 160, 78, 61]);
    const expected = "iWHZEfqgTj0=";

    const result = await ArrayBufferUtilsService.uint8ArrayToB64String(uint8Array);

    expect(result).toEqual(expected);
  });

  it("should convert string to Uint8Array", async () => {
    const base64String = "iWHZEfqgTj0=";
    const expected = new Uint8Array([137, 97, 217, 17, 250, 160, 78, 61]);

    const result = await ArrayBufferUtilsService.b64stringToUint8Array(
      base64String
    );

    expect(result).toEqual(expected);
  });
});
