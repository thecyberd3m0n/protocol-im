import { Injectable } from "@angular/core";
import * as olm from "@commapp/olm";
import { DecryptedOlmEnvelope } from "../models/decryptedOlmEnvelope.model";
import { Device } from "../models/device.model";
import { ActiveSession } from "../models/activeSession.model";
import { SimpleCryptoService } from "./simple-crypto.service";

const olmWasmPath = "/assets/olm/olm.wasm";

export interface OlmInvite {
  sessionId: string;
  sessionKey: string;
  messageIndex: number;
  pubkey: string;
  deviceFingerprint: string;
  trustedDevices: {
    deviceFingerprint: string;
    peerkey: string;
    registeredAt: number;
  }[];
}
export interface OlmGroupDescriptor {
  peersInbound: {
    [olmSid: string]: { pubkey: string; peerkey: string; fingerprint: string };
  };
  outbound?: string;
  creatorIdentityPubkey: string;
  outboundsForTrustedDevices: {
    [olmSid: string]: { key: string; fingerprint: string };
  };
}

interface OlmGroup {
  peersInbound: {
    [olmSid: string]: {
      inbound?: olm.InboundGroupSession;
      pubkey: string;
      peerkey: string;
      fingerprint: string;
    };
  };
  outbound?: olm.OutboundGroupSession;
  creatorIdentityPubkey: string;
  outboundsForTrustedDevices: {
    [olmSid: string]: {
      outbound: olm.OutboundGroupSession;
      fingerprint: string;
    };
  };
}

@Injectable({
  providedIn: "root",
})
export class OlmGroupService {
  outboundSession: olm.OutboundGroupSession | null = null;
  sessionMap = new Map<string, OlmGroup>();
  inviteSessionMap = new Map<string, OlmGroup>();
  pendingSessions: OlmGroup[] = [];
  inited = false;
  publicKey: string | null = null;
  trustedDevices: Partial<Device>[] = [];
  thisDevice: Device;
  nonce: string | null = null;

  constructor(private simpleCrypto: SimpleCryptoService) {}

  async init(
    pubkey: string,
    device: Device,
    nonce: string,
    trustedDevices?: Partial<Device>[]
  ): Promise<void> {
    console.log('OlmGroupService init', pubkey, device, nonce);
    (<any>window).OLM_OPTIONS = null;
    const OLM_OPTIONS = {
      locateFile: () => "./assets/olm/olm.wasm",
    };
    await olm.init(OLM_OPTIONS);
    this.inited = true;
    this.publicKey = pubkey;
    this.sessionMap = new Map<string, OlmGroup>();
    this.thisDevice = device;
    this.nonce = nonce;

    this.trustedDevices = trustedDevices || [];
  }

  loadIdentity(pubkey: string, trustedDevices: Partial<Device>[]) {
    this.publicKey = pubkey;
    this.trustedDevices = trustedDevices || [];
  }

  olmGroupToOlmGroupDescriptor(olmGroup: OlmGroup): OlmGroupDescriptor {
    this.checkIfInited();
    const outboundsForTrustedDevices = {};
    Object.keys(olmGroup.outboundsForTrustedDevices).forEach((key) => {
      const outbound = olmGroup[key] as {
        outbound: olm.OutboundGroupSession;
        fingerprint: string;
      };
      outboundsForTrustedDevices[key] = {
        outbound: outbound.outbound.pickle(this.nonce),
        fingerprint: outbound.fingerprint,
      };
    });

    return {
      creatorIdentityPubkey: olmGroup.creatorIdentityPubkey,
      outboundsForTrustedDevices,
      peersInbound: olmGroup.peersInbound,
      outbound: olmGroup.outbound ? olmGroup.outbound.pickle(this.nonce) : null,
    };
  }

  olmGroupDescriptorToOlmGroup(
    olmGroupDescriptor: OlmGroupDescriptor,
    nonce?: string
  ): OlmGroup {
    this.checkIfInited();
    const outboundsForTrustedDevices = {};
    const peersInbound = {};
    let outbound = null;

    try {
      Object.keys(olmGroupDescriptor.outboundsForTrustedDevices).forEach(
        (olmSid) => {
          // create outbound for each of them
          const val = olmGroupDescriptor.outboundsForTrustedDevices[olmSid];
          const outboundOlm = new olm.OutboundGroupSession();

          outboundOlm.unpickle(val.key, nonce ? nonce : this.nonce);
          outboundsForTrustedDevices[olmSid] = outboundOlm;
        }
      );

      if (olmGroupDescriptor.outbound) {
        outbound = new olm.OutboundGroupSession();
        outbound.unpickle(nonce, olmGroupDescriptor.outbound);
      }

      Object.entries(olmGroupDescriptor.peersInbound).forEach(
        (
          value: [
            string,
            {
              pubkey: string;
              peerkey: string;
              fingerprint: string;
            }
          ]
        ) => {
          const inbound = new olm.InboundGroupSession();
          inbound.create(value[1].peerkey);
          peersInbound[value[0]] = {
            fingerprint: value[1].fingerprint,
            peerkey: value[1].peerkey,
            inbound,
            pubkey: value[1].pubkey,
          };
        }
      );
    } catch (e) {
      console.error(e);
      console.log("nonce", nonce);
      console.log("olmGroupDescriptor: outbound", olmGroupDescriptor.outbound);
    }
    return {
      creatorIdentityPubkey: olmGroupDescriptor.creatorIdentityPubkey,
      outboundsForTrustedDevices,
      peersInbound,
      outbound,
    };
  }

  loadActiveSession(activeSession: ActiveSession, nonce: string) {
    const olmGroupDescriptor = activeSession.olmGroupDescriptor;

    const olmGroup = this.olmGroupDescriptorToOlmGroup(
      olmGroupDescriptor,
      nonce
    );

    this.sessionMap.set(activeSession.id, olmGroup);
  }

  addTrustedDevices(trustedDevices: Device[]) {
    this.trustedDevices = this.trustedDevices.concat(trustedDevices);
  }

  setNewIdentityPubkey(pubkey: string) {
    this.publicKey = pubkey;
  }

  /**
   * Bob creates new session without peer's inbound.
   * Peer must pass his olmInvite (acceptInviteToSession) to be able to receive your messages
   *
   * @param sid
   * @returns OlmInvite
   */
  createSession(tempSid: string, creatorPubkey?: string): OlmInvite {
    const outboundSession = new olm.OutboundGroupSession();
    outboundSession.create();
    const inboundSession = new olm.InboundGroupSession();
    const sessionKey = outboundSession.session_key();
    inboundSession.create(sessionKey);
    const olmSid = inboundSession.session_id();

    const newSession: OlmGroup = {
      outbound: outboundSession,
      outboundsForTrustedDevices: {},
      peersInbound: {
        [olmSid]: {
          inbound: inboundSession,
          peerkey: sessionKey,
          pubkey: this.publicKey,
          fingerprint: this.thisDevice.fingerprint,
        },
      },
      creatorIdentityPubkey: creatorPubkey ? creatorPubkey : this.publicKey,
    };
    this.inviteSessionMap.set(tempSid, newSession);

    return {
      messageIndex: outboundSession.message_index(),
      sessionId: outboundSession.session_id(),
      sessionKey: outboundSession.session_key(),
      pubkey: this.publicKey,
      deviceFingerprint: this.thisDevice.fingerprint,
      trustedDevices: [] as {
        deviceFingerprint: string;
        peerkey: string;
        registeredAt: number;
      }[],
    };
  }

  loadOlmInvite(inviteId: string, olmInvite: OlmInvite): void {
    const outboundSession = new olm.OutboundGroupSession();
    outboundSession.create();
    const inboundSession = new olm.InboundGroupSession();
    const sessionKey = outboundSession.session_key();
    inboundSession.create(sessionKey);
    const olmSid = inboundSession.session_id();

    const newSession: OlmGroup = {
      outbound: outboundSession,
      outboundsForTrustedDevices: {},
      peersInbound: {
        [olmSid]: {
          inbound: inboundSession,
          peerkey: sessionKey,
          pubkey: this.publicKey,
          fingerprint: olmInvite.deviceFingerprint,
        },
      },
      creatorIdentityPubkey: this.publicKey,
    };

    // this.sessionMap.set(inviteId, newSession);

    this.inviteSessionMap.set(inviteId, newSession);
  }

  /**
   * Creates invite for existing session
   * @param sessionId
   * @returns
   */
  inviteToPendingSession(sessionId: string): OlmInvite {
    const session = this.sessionMap.get(sessionId);
    if (!session) {
      throw new Error(
        `[OlmGroupService] inviteToPendingSession: Session ${sessionId} not found in OlmService sessionMap`
      );
    }

    const trustedDevicesKeys: { deviceFingerprint: string; peerkey: string }[] =
      [];
    const trustedOutbounds = {};
    // pre-enroll keys for trusted devices
    // TODO: we don't need to find index - it's a HashMap now

    this.trustedDevices
      .filter((x) => !!x)
      .forEach((device: Device) => {
        // const foundOutbound = session.outboundsForTrustedDevices[device.fingerprint];
        if (
          Object.values(session.outboundsForTrustedDevices).findIndex(
            (x) => x.fingerprint
          ) === -1
        ) {
          const outbound = new olm.OutboundGroupSession();
          outbound.create();

          trustedOutbounds[outbound.session_id()] = {
            outbound: outbound,
            fingerprint: device.fingerprint,
          };
          const inbound = new olm.InboundGroupSession();
          const key = outbound.session_key();
          const olmSid = outbound.session_id();
          inbound.create(key);
          if (device.fingerprint !== this.thisDevice.fingerprint) {
            trustedDevicesKeys.push({
              deviceFingerprint: device.fingerprint,
              peerkey: key,
            });

            session.peersInbound[olmSid] = {
              fingerprint: device.fingerprint,
              inbound: inbound,
              peerkey: key,
              pubkey: this.publicKey,
            };
          }
        }
      });

    session.outboundsForTrustedDevices = trustedOutbounds;

    return {
      messageIndex: session.outbound.message_index(),
      sessionId: session.outbound.session_id(),
      sessionKey: session.outbound.session_key(),
      pubkey: this.publicKey,
      deviceFingerprint: this.thisDevice.fingerprint,
      trustedDevices: trustedDevicesKeys as {
        deviceFingerprint: string;
        peerkey: string;
        registeredAt: number;
      }[],
    };
  }

  /**
   * Alice adds Bobs peerKeys to registry and creates InboundOlm.
   * @param sessionId
   * @param peersInvite
   * @returns
   */
  acceptInviteToSession(
    sessionId: string,
    newSessionId: string,
    peersInvite: OlmInvite
  ): OlmGroupDescriptor {
    let session = this.inviteSessionMap.get(sessionId);
    if (sessionId === newSessionId) {
      // already esist
      session = this.sessionMap.get(sessionId);
    }

    if (!session) {
      throw new Error(
        `[OlmGroupService] acceptInvitetoSession: Session ${sessionId} not found in OlmService sessionMap`
      );
    }
    const peersInbound = new olm.InboundGroupSession();
    peersInbound.create(peersInvite.sessionKey);
    session.creatorIdentityPubkey = peersInvite.pubkey;
    // TODO: add his trusted devices too
    session.peersInbound[peersInvite.sessionId] = {
      inbound: peersInbound,
      pubkey: peersInvite.pubkey,
      peerkey: peersInvite.sessionKey,
      fingerprint: peersInvite.deviceFingerprint,
    };
    peersInvite.trustedDevices.forEach((trustedDevice) => {
      const inbound = new olm.InboundGroupSession();
      inbound.create(trustedDevice.peerkey);
      const olmSid = inbound.session_id();

      session.peersInbound[olmSid] = {
        inbound: inbound,
        pubkey: peersInvite.pubkey,
        peerkey: trustedDevice.peerkey,
        fingerprint: peersInvite.deviceFingerprint,
      };
    });

    this.inviteSessionMap.delete(sessionId);
    this.sessionMap.set(newSessionId, session);

    return this.olmGroupToOlmGroupDescriptor(this.sessionMap.get(newSessionId));
  }

  /**
   * AliceB imports AliceA session and must replace masterSessions outbound with own. Also adds keys from old outbound as peer's keys
   * saves same pubkey as it was
   *
   * @param sessionId
   * @param olmGroup
   * @returns { OlmGroup, OlmInvite } - OlmGroup to store and OlmInvite for Session
   */
  importGroupAndEnrollMe(
    sessionId: string,
    olmGroup: OlmGroupDescriptor,
    unpickleNonce: string
  ): Record<string, { olmGroup: OlmGroupDescriptor; olmInvite: OlmInvite }> {
    if (!this.inited) {
      throw new Error("OlmService not initiated");
    }
    let session = this.sessionMap.get(sessionId);
    // create if not exists, unpickle by given nonce
    if (!session) {
      session = this.olmGroupDescriptorToOlmGroup(olmGroup, unpickleNonce);
    }
    
    const outbound = new olm.OutboundGroupSession();
    outbound.create();
    session.outbound = outbound;
    // creating own inbound
    const olmSid = outbound.session_id();
    const key = outbound.session_key();
    const inbound = new olm.InboundGroupSession();
    inbound.create(key);
    // console.log('inbound decrypt fn', inbound.decrypt);
    session.peersInbound[olmSid] = {
      inbound,
      peerkey: key,
      pubkey: olmGroup.creatorIdentityPubkey,
      fingerprint: this.thisDevice.fingerprint,
    };
    // Object.values(olmGroup.peersInbound).forEach((peerInbound, index) => {
    //   const olmSid = Object.keys(olmGroup.peersInbound)[index];

    // });

    this.sessionMap.set(sessionId, session);

    return {
      [sessionId]: {
        olmGroup: olmGroup,
        olmInvite: {
          messageIndex: outbound.message_index(),
          sessionId: outbound.session_id(),
          sessionKey: outbound.session_key(),
          pubkey: this.publicKey,
          deviceFingerprint: this.thisDevice.fingerprint,
          trustedDevices: [], // TODO: investigate if plausable. I don't know what to put here now
        },
      },
    };
  }

  /**
   * AliceA exports existing group for AliceB by extracting AliceA outbound to one of peerKeys
   * That inbound should be created with creating session. Outbound should be removed
   * @param sessionId
   */
  shareOlmGroup(sessionId: string): OlmGroup {
    const session = { ...this.sessionMap.get(sessionId) };
    delete session.outbound;
    return session;
  }

  loadOlmGroup(
    sessionId: string,
    olmGroup: OlmGroupDescriptor,
    nonce?: string
  ) {
    // add olmGroup go sessionMap
    if (!this.sessionMap.get(sessionId)) {
      
      this.sessionMap.set(
        sessionId,
        this.olmGroupDescriptorToOlmGroup(olmGroup, nonce)
      );
    } else {
      console.warn(
        `[OlmGroupService] loadOlmGroup ${sessionId} rejected - such group already exists`
      );
    }
  }

  addPeerToSession(
    sessionId: string,
    olmInvite: OlmInvite
  ): OlmGroupDescriptor {
    const session = this.sessionMap.get(sessionId);
    // add his key to peer and return OlmGroup
    const inbound = new olm.InboundGroupSession();
    inbound.create(olmInvite.sessionKey);

    let peersInbound = { ...session.peersInbound };
    // use peers pubkey
    peersInbound[olmInvite.sessionId] = {
      inbound,
      pubkey: olmInvite.pubkey,
      peerkey: olmInvite.sessionKey,
      fingerprint: olmInvite.deviceFingerprint,
    };
    this.sessionMap.set(sessionId, {
      ...session,
      ...{
        peersInbound,
      },
    });

    return this.olmGroupToOlmGroupDescriptor(session);
  }

  /**
   *
   * @param sessionId
   * @param anyMessage
   * @param type
   * @returns decrypted and encrypted form of envelope, and session key
   * if session was not exist. Send it to peer if not null in order for him to decrypt the message
   */
  encryptOlmMessage(
    sessionId: string,
    anyMessage: any,
    type: string
  ): Promise<{ decrypted: DecryptedOlmEnvelope; encrypted: string }> {
    return new Promise(async (resolve, reject) => {
      let message = anyMessage;
      if (typeof message !== "string") {
        message = JSON.stringify(message);
      }

      const session = this.sessionMap.get(sessionId);

      if (!session) {
        reject(
          `[OlmGroupService] encryptOlmMessage: Session ${sessionId} not found`
        );

        return;
      }

      if (!session.outbound) {
        reject(
          `[OlmGroupService] encryptOlmMessage: Could not send the message - no outboundSession for ${sessionId}.`
        );
        console.log("sessionMap", this.sessionMap);
        console.error("sid", session);
        return;
      }

      const randomId = await this.simpleCrypto.randomId();
      const outgoingEnvelope = {
        timestamp: new Date(Date.now()),
        sessionId,
        content: message,
        id: randomId,
        type,
      };
      const olmSid = session.outbound.session_id();
      console.log(
        "encrypting olm message, olmSid",
        olmSid,
        "sessionId",
        sessionId
      );
      const decrypted = {
        content: outgoingEnvelope.content,
        sessionId,
        olmSid,
        own: true,
        timestamp: outgoingEnvelope.timestamp,
        id: randomId,
        type,
      };

      const encrypted = await session.outbound.encrypt(
        JSON.stringify(outgoingEnvelope)
      );
      resolve({
        encrypted,
        decrypted,
      });
    });
  }

  decryptOlmMessage(
    sessionId: string,
    encryptedMessage: string,
    olmSid: string
  ) {
    return new Promise((resolve, reject) => {
      console.log(
        `[OlmGroupService] decrypt msg for session ${sessionId}, olmSid ${olmSid}`
      );
      const session = this.sessionMap.get(sessionId);
      if (!session) {
        reject(`[OlmGroupService] Session ${sessionId} not found`);
        return;
      }
      if (!session.peersInbound[olmSid]) {
        reject(
          `[OlmGroupService] OlmSid ${olmSid} of session ${sessionId} not found.`
        );
        console.error("[OlmGroupService] sessionMap", this.sessionMap);
        return;
      }
      
      if (!session.peersInbound[olmSid].inbound) {
        reject(
          `InboundOlm not found for OlmSid ${olmSid} in session ID ${sessionId}`
        );
        return;
      }
      try {
        const { message_index, plaintext } = session.peersInbound[olmSid].inbound.decrypt(encryptedMessage);
        const decryptedEnvelope = JSON.parse(plaintext) as DecryptedOlmEnvelope;
        const outgoingEnvelope = {
          id: decryptedEnvelope.id,
          content: decryptedEnvelope.content,
          sessionId,
          own: session.peersInbound[olmSid].pubkey === this.publicKey,
          timestamp: decryptedEnvelope.timestamp,
          type: decryptedEnvelope.type,
          messageIndex: message_index,
          olmSid,
        };

        resolve(outgoingEnvelope);
      } catch (e) {
        reject(e);
      }
    });
  }

  /**
   * Creates peer's inbound by key for receive and new outbound for sending. Outbound should be sent to peer
   * @param id
   * @param olmInvite
   * @returns
   */
  async joinGroupByInvite(
    id: string,
    olmInvite: OlmInvite
  ): Promise<OlmGroupDescriptor> {
    this.checkIfInited();
    const inboundSession = new olm.InboundGroupSession();
    inboundSession.create(olmInvite.sessionKey);

    const outboundSession = new olm.OutboundGroupSession();
    outboundSession.create();

    // TODO: generate olms for trustedDevices

    const myInboundSession = new olm.InboundGroupSession();
    const mySid = outboundSession.session_id();
    const myKey = outboundSession.session_key();
    myInboundSession.create(myKey);
    
    this.sessionMap.set(id, {
      peersInbound: {
        [olmInvite.sessionId]: {
          inbound: inboundSession,
          pubkey: olmInvite.pubkey,
          peerkey: olmInvite.sessionKey,
          fingerprint: olmInvite.deviceFingerprint,
        },
        [mySid]: {
          inbound: myInboundSession,
          pubkey: this.publicKey,
          peerkey: myKey,
          fingerprint: this.thisDevice.fingerprint,
        },
      },
      outbound: outboundSession,
      creatorIdentityPubkey: this.publicKey,
      outboundsForTrustedDevices: {},
    });

    return this.olmGroupToOlmGroupDescriptor(this.sessionMap.get(id));
  }

  createOutboundsForTrustedDevices(
    sessionId: string
  ): { outbound: string; fingerprint: string; key: string }[] {
    const session = this.sessionMap.get(sessionId);
    return Object.values(session.outboundsForTrustedDevices).map((outbound) => {
      const exported = outbound.outbound.pickle(this.nonce);
      return {
        outbound: exported,
        fingerprint: outbound.fingerprint,
        key: this.nonce,
      };
    });
  }

  checkIfInited(): void {
    if (!this.inited) {
      throw new Error("OlmGroupService not initialized");
    }
  }
}
