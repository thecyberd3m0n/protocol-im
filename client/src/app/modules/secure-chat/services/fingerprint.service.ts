import { Injectable } from "@angular/core";
import { Device } from "../models/device.model";
import { SimpleCryptoService } from "./simple-crypto.service";

@Injectable({
  providedIn: "root",
})
export class FingerprintService {
  constructor(private simpleCrypto: SimpleCryptoService) {}

  async generateDevice(): Promise<Device> {
    const fp = await this.generateFingerprint();
    return {
      name: this.getDeviceName(),
      fingerprint: fp,
      registeredAt: new Date()
    }
  }
  // TODO: generate just unique number
  public generateFingerprint(): Promise<string> {
    return new Promise(async (resolve, reject) => {
      const randomStr = await this.simpleCrypto.randomString(16);
      resolve(randomStr);
    });
  }

  public getDeviceName(): string {
    const userAgent = navigator.userAgent.toLowerCase();

    // Determine the browser name
    let browserName = "";
    if (userAgent.includes("chrome")) {
      browserName = "Google Chrome";
    } else if (userAgent.includes("firefox")) {
      browserName = "Firefox";
    } else if (userAgent.includes("safari")) {
      browserName = "Safari";
    } else if (userAgent.includes("applewebkit")) {
      browserName = "WebKit";
    }

    // Determine the device type
    const deviceType = /Mobile|Android|iPhone|iPod|iPad/i.test(userAgent)
      ? "Mobile"
      : "Desktop";

    // Concatenate the browser name and device type
    const deviceName = `${browserName} ${deviceType}`;

    return deviceName;
  }
}
