import { EntityAdapter, EntityState, createEntityAdapter } from '@ngrx/entity';
import { SerializedActiveSession } from './../models/activeSession.model';
export interface ActiveSessionsState extends EntityState<SerializedActiveSession> { }

export const activeSessionsAdapter: EntityAdapter<SerializedActiveSession> = createEntityAdapter({
  selectId: session => session.id
});
