import { createEntityAdapter, EntityAdapter, EntityState } from "@ngrx/entity";
import { Device } from "../models/device.model";

export interface ApprovedDevicesState extends EntityState<Partial<Device>> { }

export const approvedDevicesAdapter: EntityAdapter<Partial<Device>> = createEntityAdapter<Partial<Device>>({
    selectId: device => device.fingerprint
});
