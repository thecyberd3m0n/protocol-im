import { PreKey } from '@wireapp/proteus/src/main/keys';
import { EntityState, createEntityAdapter, EntityAdapter } from '@ngrx/entity';

export interface PreKeysState extends EntityState<PreKey> { }

export const preKeysStateAdapter: EntityAdapter<PreKey> = createEntityAdapter({
  selectId: preKey => preKey.key_id
});
