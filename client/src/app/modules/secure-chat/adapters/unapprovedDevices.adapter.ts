import { createEntityAdapter, EntityAdapter, EntityState } from "@ngrx/entity";
import { Device } from "../models/device.model";

export interface UnapprovedDevicesState extends EntityState<{ device: Device, pendingSessionId: string }> { }

export const unapprovedDevicesAdapter: EntityAdapter<{ device: Device, pendingSessionId: string }> = createEntityAdapter<{ device: Device, pendingSessionId: string }>({
    selectId: (device) => {
        return device?.device?.fingerprint;
    }
});
