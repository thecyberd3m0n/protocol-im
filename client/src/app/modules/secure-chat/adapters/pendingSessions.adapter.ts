import { EntityAdapter, EntityState, createEntityAdapter } from '@ngrx/entity';
import { SerializedInviteSession } from '../models/inviteSession.model';

export interface InviteSessionsState extends EntityState<SerializedInviteSession> { }

export const inviteSessionsAdapter: EntityAdapter<SerializedInviteSession> = createEntityAdapter<SerializedInviteSession>({
  selectId: session => session.id
});
