import { DecryptedOlmEnvelope } from '../models/decryptedOlmEnvelope.model';
import { EntityState, createEntityAdapter, EntityAdapter } from '@ngrx/entity';
export interface MessagesState extends EntityState<DecryptedOlmEnvelope> { }

export const messagesStateAdapter: EntityAdapter<DecryptedOlmEnvelope> = createEntityAdapter({
  selectId: messages => messages.id
});
