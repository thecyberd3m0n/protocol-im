import { createEntityAdapter, EntityAdapter, EntityState } from "@ngrx/entity";
import { Device } from "../models/device.model";

export interface LockedDevicesState extends EntityState<Partial<Device>> { }

export const lockedDevicesAdapter: EntityAdapter<Partial<Device>> = createEntityAdapter<Partial<Device>>({
    selectId: device => device.fingerprint
});
