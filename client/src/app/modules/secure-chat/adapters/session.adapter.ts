import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Session } from '@wireapp/proteus/src/main/session';

export interface SessionsState extends EntityState<Session> { }

export const sessionsAdapter: EntityAdapter<Session> = createEntityAdapter<Session>({
  selectId: session => session.session_tag.toString()
});
