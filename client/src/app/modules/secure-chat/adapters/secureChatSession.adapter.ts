import { EntityAdapter, createEntityAdapter, EntityState } from '@ngrx/entity';
import { SecureChatSession } from '../models/secureChatSession.model';

export interface SecureChatSessionsState extends EntityState<SecureChatSession> { }

export const secureChatSessionsAdapter: EntityAdapter<SecureChatSession> = createEntityAdapter<SecureChatSession>({
  selectId: session => session.id
});
