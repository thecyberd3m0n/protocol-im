import { EntityAdapter, EntityState, createEntityAdapter } from "@ngrx/entity";
import { SessionRequest } from "../store/reducers/secure-chat.reducer";

export interface SessionRequestsState extends EntityState<SessionRequest> { }

export const sessionRequestsAdapter: EntityAdapter<SessionRequest> = createEntityAdapter<SessionRequest>({
    selectId: sessionRequest => sessionRequest.inviteSessionId
})
