export interface DecryptedE2eEnvelope {
  id: string;
  sessionId: string;
  content: string;
  own: boolean;
  timestamp: Date;
  type: string;
  meta?: Map<string, any>;
}
