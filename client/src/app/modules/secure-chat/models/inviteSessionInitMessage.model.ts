import { OlmInvite } from "../services/olm-group.service";

export interface InviteSessionInitMessage {
    olmInvite: OlmInvite;
    userPublicKeyFingerprint: string;
}