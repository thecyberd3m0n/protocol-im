import { PreKey } from '@wireapp/proteus/src/main/keys/PreKey';
import { IdentityKey, PreKeyBundle } from '@wireapp/proteus/src/main/keys';
import { Session } from '@wireapp/proteus/src/main/session/Session';

export class SecureChatSession {
  id: string;
  preKeyBundle: PreKeyBundle;
  preKey?: PreKey;
  session?: Session;
  manualApprove: boolean;
  ownerIdentityPublicKey: string;

  serialize() {
    return {
      id: this.id,
      preKey: this.preKey.serialise(),
      preKeyBundle: this.preKeyBundle.serialise(),
      session: this.session.serialise(),
      manualApprove: this.manualApprove,
      ownerIdentityPublicKey: this.ownerIdentityPublicKey
    };
  }
}
