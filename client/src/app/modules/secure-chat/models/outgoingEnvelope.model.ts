/**
 * OutgoingMessage with metadata
 */
export interface OutgoingEnvelope {
  id: string;
  sessionId: string;
  content: string;
  timestamp: Date;
}
