export interface EncryptedMessage {
  id: string;
  cipherText: Uint8Array;
}
