import { OlmInvite } from "../services/olm-group.service";

export interface PreKeyInitMessage {
    olmInvite: OlmInvite;
    welcomeData?: string;
}
