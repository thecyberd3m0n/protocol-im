import { MetadataMap } from "./metadataMap.model";

export interface Profile {
  pubKeyFingerprint: string;
  meta: MetadataMap;
}
