export interface Device {
  name: string;
  fingerprint: string;
  registeredAt: Date;
}
