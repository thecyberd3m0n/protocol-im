import { keys, session } from '@wireapp/proteus';
import { ArrayBufferUtilsService } from '../services/arrayBufferUtils.service';
import { OlmInvite } from '../services/olm-group.service';

export interface SerializedInviteSession {
  id: string;
  peersPublicKey?: string;
  preKeyBundle: string;
  manualApprove: boolean;
  groupMode: boolean;
  oneTime: boolean;
  created: Date;
  activeSessionId?: string | null; // null if group mode is false
  e2eSession?: string;
  olmInvite?: OlmInvite;
}

export class InviteSession {
    constructor(
      public id: string,
      public manualApprove: boolean,
      public groupMode: boolean,
      public oneTime: boolean,
      public created: Date,
      public activeSessionId?: string,
      public peersPublicKey?: string,
      public e2eSession?: session.Session,
      public preKeyBundle?: keys.PreKeyBundle,
      public olmInvite?: OlmInvite
    ) {}

    static serialize(inviteSession: InviteSession): SerializedInviteSession {
      return {
        id: inviteSession.id,
        preKeyBundle: inviteSession.preKeyBundle ? ArrayBufferUtilsService.arraybufferToString(inviteSession.preKeyBundle.serialise()) : null,
        manualApprove: inviteSession.manualApprove,
        groupMode: inviteSession.groupMode,
        activeSessionId: inviteSession.activeSessionId,
        oneTime: inviteSession.oneTime,
        created: inviteSession.created,
        e2eSession: ArrayBufferUtilsService.arraybufferToString(inviteSession.e2eSession ? inviteSession.e2eSession.serialise() : null),
        peersPublicKey: inviteSession.peersPublicKey,
        olmInvite: inviteSession.olmInvite ? inviteSession.olmInvite : null,
      };
    }

    static deserialize(
      localIdentity: keys.IdentityKeyPair, 
      serializedPendingSession: SerializedInviteSession): InviteSession {
        // try deserialize e2eSession, but nullify on errors
        let e2eSession = null;
        try {
          e2eSession = session.Session.deserialise(localIdentity, ArrayBufferUtilsService.stringToArraybuffer(serializedPendingSession.e2eSession));
        } catch {
        }
        return new InviteSession(
        serializedPendingSession.id,
        serializedPendingSession.manualApprove,
        serializedPendingSession.groupMode,
        serializedPendingSession.oneTime,
        serializedPendingSession.created,
        serializedPendingSession.activeSessionId,
        serializedPendingSession.peersPublicKey,
        e2eSession,
        serializedPendingSession.preKeyBundle ? keys.PreKeyBundle.deserialise(ArrayBufferUtilsService.stringToArraybuffer(serializedPendingSession.preKeyBundle)) : null,
        serializedPendingSession.olmInvite ?? serializedPendingSession.olmInvite
      );
    }
}
