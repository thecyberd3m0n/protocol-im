import { keys } from "@wireapp/proteus";
import { ArrayBufferUtilsService } from "../services/arrayBufferUtils.service";
import { SecureSyncState } from "../store/reducers/secure-sync.reducer";
import { ActiveSession, SerializedActiveSession } from "./activeSession.model";
import { Device } from "./device.model";
import { InviteSession, SerializedInviteSession } from "./inviteSession.model";
import { MetadataMap } from "./metadataMap.model";
export interface SerializedMasterSession {
  identity: string;
  device: {
    fingerprint: string;
    name: string;
    registeredAt: number;
  };
  trustedDevices: {
    fingerprint: string;
    name: string;
    registeredAt: number;
  }[];
  activeSessions: SerializedActiveSession[];
  inviteSessions: SerializedInviteSession[];
  preKeys: string[];
  meta: string;
  syncStatus: SecureSyncState;
}

export class MasterSession {
  constructor(
    public identity: keys.IdentityKeyPair,
    public activeSessions: ActiveSession[] = [],
    public inviteSessions: InviteSession[] = [],
    public preKeys: keys.PreKey[] = [],
    public syncStatus: SecureSyncState,
    public device: Device,
    public trustedDevices: Device[],
    public meta?: MetadataMap
  ) {}
  static serialize(masterSession: MasterSession): SerializedMasterSession {
    const identity = ArrayBufferUtilsService.arraybufferToString(
      masterSession.identity.serialise()
    );
    let meta = null;
    if (masterSession.meta) {
      meta = JSON.stringify(masterSession.meta);
    }
    const activeSessions = masterSession.activeSessions.map((session) =>
      ActiveSession.serialize(session)
    );
    const inviteSessions = masterSession.inviteSessions.map((pendingSession) =>
      InviteSession.serialize(pendingSession)
    );

    const preKeys = masterSession.preKeys.map((preKey) => {
      console.log("ms serialize", preKey.serialise());
      return ArrayBufferUtilsService.arraybufferToString(preKey.serialise());
    });
    const syncStatus = masterSession.syncStatus;

    return {
      identity,
      meta,
      activeSessions,
      inviteSessions,
      preKeys,
      syncStatus,
      trustedDevices: masterSession.trustedDevices
        ? masterSession.trustedDevices.map((td) => {
            if (!td.registeredAt)
              console.warn(
                `MasterSession: no regsiteredAt parameter for device ${td.name} with fingerprint ${td.fingerprint}`
              );
            return {
              fingerprint: td.fingerprint,
              name: td.name,
              registeredAt: td.registeredAt ? td.registeredAt.getTime() : null,
            };
          })
        : [],
      device: {
        fingerprint: masterSession.device.fingerprint,
        name: masterSession.device.name,
        registeredAt: masterSession.device.registeredAt.getTime(),
      },
    };
  }

  static deserialize(
    serializedMasterSession: SerializedMasterSession
  ): MasterSession {
    const identity = keys.IdentityKeyPair.deserialise(
      ArrayBufferUtilsService.stringToArraybuffer(
        serializedMasterSession.identity
      )
    );
    const meta = serializedMasterSession.meta;

    const activeSessions = serializedMasterSession.activeSessions.map(
      (activeSession) => {
        const session = ActiveSession.deserialize(activeSession);
        return {
          id: activeSession.id,
          session,
        };
      }
    );

    const inviteSessions = serializedMasterSession.inviteSessions.map(
      (inviteSession) => InviteSession.deserialize(identity, inviteSession)
    );
    let preKeys = [];
    try {
      preKeys = serializedMasterSession.preKeys.map((preKey) => {
        console.log('ms deserialize', ArrayBufferUtilsService.stringToArraybuffer(preKey))
        return keys.PreKey.deserialise(
          ArrayBufferUtilsService.stringToArraybuffer(preKey)
        );
      });
    } catch (e) {
      console.error(e);
      console.error(e.stack);
    }
    

    let device = null;
    if (serializedMasterSession.device) {
      device = {
        fingerprint: serializedMasterSession.device.fingerprint,
        name: serializedMasterSession.device.name,
        registeredAt: new Date(serializedMasterSession.device.registeredAt),
      };
    }

    const trustedDevices = serializedMasterSession.trustedDevices
      ? serializedMasterSession.trustedDevices.map(
          (td) =>
            ({
              name: td.name,
              fingerprint: td.fingerprint,
              registeredAt: new Date(td.registeredAt),
            } as Device)
        )
      : [];

    return new MasterSession(
      identity,
      activeSessions.map((x) => x.session),
      inviteSessions,
      preKeys,
      serializedMasterSession.syncStatus,
      device,
      trustedDevices,
      JSON.parse(meta)
    );
  }
}
