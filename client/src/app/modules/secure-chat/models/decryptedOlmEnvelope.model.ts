export interface DecryptedOlmEnvelope {
  id: string;
  sessionId: string;
  olmSid: string;
  content: string;
  own: boolean;
  timestamp: Date;
  type: string;
}
