export interface OutgoingMessageModel {
  sessionId: string;
  content: string;
  type: string;
  noStore: boolean;
}
