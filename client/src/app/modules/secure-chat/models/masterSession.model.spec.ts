import * as Proteus from "@wireapp/proteus";
import { initialState } from "../store/reducers/secure-sync.reducer";
import { Device } from "./device.model";
import { InviteSession } from "./inviteSession.model";
import { MasterSession } from "./masterSession.model";

fdescribe("MasterSession", () => {
  it("should serialize and deserialize prekeys with pendingSession", async () => {
    const identity = await Proteus.keys.IdentityKeyPair.new();
    const preKeys = await Proteus.keys.PreKey.generate_prekeys(0, 1);
    const preKeyBundle = await new Proteus.keys.PreKeyBundle(
      identity.public_key,
      preKeys[0]
    );
    const meta = { foo: "bar" };
    const activeSessions = [];
    const pendingSessions = [
      new InviteSession(
        "testid",
        false,
        false,
        false,
        new Date(Date.now()),
        null,
        null,
        null,
        preKeyBundle,
        null
      ),
    ];
    const serialisedPkeyBundle = preKeyBundle.serialise();

    const secureSyncState = initialState;
    const device: Device = {
      fingerprint: "testfingerprint",
      name: "TestDevice",
      registeredAt: new Date(Date.now()),
    };
    const masterSession = new MasterSession(
      identity,
      activeSessions,
      pendingSessions,
      preKeys,
      secureSyncState,
      device,
      [],
      meta
    );
    const sms = MasterSession.serialize(masterSession);
    console.log(sms);
    const deserialized = await MasterSession.deserialize(sms);
    console.log(deserialized.preKeys);
    expect(deserialized.preKeys[0].serialise()).toEqual(preKeys[0].serialise());
    expect(deserialized.inviteSessions[0].preKeyBundle.serialise()).toEqual(
      serialisedPkeyBundle
    );
  });
});
