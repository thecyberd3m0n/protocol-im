export interface UnsignedProfile {
  passphrase: string;
  name?: string;
}
