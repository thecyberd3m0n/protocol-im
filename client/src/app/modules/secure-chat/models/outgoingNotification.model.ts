export interface OutgoingNotification {
    devTokens: string[];
    content: string;
}
