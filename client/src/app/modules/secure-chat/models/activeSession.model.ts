import { OlmGroupDescriptor } from "../services/olm-group.service";
import { MetadataMap } from "./metadataMap.model";

export interface SerializedActiveSession {
  id: string;
  meta?: MetadataMap;
  olmGroupDescriptor: OlmGroupDescriptor;
  inviteSessionId: string;
  peersIdentityPubkey: string;
}

export class ActiveSession {
  constructor(
    public id: string,
    public meta: MetadataMap,
    public olmGroupDescriptor: OlmGroupDescriptor,
    public peersIdentityPublicKey: string,
    public inviteSessionId: string
  ) {}


  static serialize(
    activeSession: ActiveSession
  ): SerializedActiveSession {
    return {
      id: activeSession.id,
      meta: activeSession.meta,
      olmGroupDescriptor: activeSession.olmGroupDescriptor,
      peersIdentityPubkey: activeSession.peersIdentityPublicKey,
      inviteSessionId: activeSession.inviteSessionId,
    };
  }

  static deserialize(
    serializedActiveSession: SerializedActiveSession
  ): ActiveSession {
 
    return new ActiveSession(
      serializedActiveSession.id,
      serializedActiveSession.meta,
      serializedActiveSession.olmGroupDescriptor,
      serializedActiveSession.peersIdentityPubkey,
      serializedActiveSession.inviteSessionId
    );
  }
}
