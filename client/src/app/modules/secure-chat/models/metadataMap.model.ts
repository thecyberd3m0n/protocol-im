export interface MetadataMap {
  [index: string]: any;
}
