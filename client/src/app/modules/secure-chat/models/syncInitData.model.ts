import { DBNonceCipher } from "../services/db-models/nonce-cipher.dbmodel";
import { Device } from "./device.model";
import { SerializedMasterSession } from "./masterSession.model";

export interface SyncInitData {
    masterSessionBundle: SerializedMasterSession;
    device: Device;
    nonceCipher: DBNonceCipher; //TODO: change to Nonce
    type: string;
}