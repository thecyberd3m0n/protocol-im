import { OlmInvite } from "../services/olm-group.service";

export class SessionTicket {
    olmInvite: OlmInvite;
    activeSessionId: string;
}
