import { Subscription } from "rxjs";
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import {
  SecureChatDispatchers,
  SecureChatMode,
  SecureChatSelectors,
} from "./modules/secure-chat";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
})
export class AppComponent implements OnInit {
  title = "client";
  modeSub: Subscription;
  routeSub: Subscription;
  constructor(
    private secureChatSelectors: SecureChatSelectors,
    private secureChatDispatchers: SecureChatDispatchers,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}
  ngOnInit(): void {
    this.modeSub = this.secureChatSelectors.mode$.subscribe(
      (mode: SecureChatMode) => {
        this.routeSub = this.activatedRoute.params.subscribe((params) => {
          if (mode === SecureChatMode.DISABLED) {
            this.secureChatDispatchers.init();
            this.routeSub.unsubscribe();
          } else if (
            mode === SecureChatMode.GUEST &&
            this.router.url.indexOf("join") < 0 &&
            this.router.url.indexOf("device") < 0
          ) {
            this.router.navigateByUrl("home");
            this.routeSub.unsubscribe();
          } else if (mode === SecureChatMode.AWAITING_MASTER_PASSPHRASE) {
            this.router.navigateByUrl(
              `unlock-master-session${
                this.router.url.indexOf("join") !== -1 ||
                this.router.url.indexOf("device") !== -1
                  ? `/?r=${encodeURIComponent(this.router.url)}`
                  : ""
              }`
            );
            this.routeSub.unsubscribe();
          }
        });
      }
    );
  }
}
