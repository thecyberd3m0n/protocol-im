import { ClipboardModule } from "@angular/cdk/clipboard";
import { LayoutModule } from "@angular/cdk/layout";
import { TextFieldModule } from "@angular/cdk/text-field";
import { ModuleWithProviders, NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { AngularFireModule } from "@angular/fire";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule, Routes } from "@angular/router";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { ZXingScannerModule } from "@zxing/ngx-scanner";
import { QRCodeModule } from "angularx-qrcode";
import { NgxFileDropModule } from "ngx-file-drop";
import { environment } from "../environments/environment";
import { AppComponent } from "./app.component";
import { ChatComponent } from "./components/chat/chat.component";
import { ConnectDeviceDeviceApproveStepComponent } from './components/connect-device-device-approve-step/connect-device-device-approve-step.component';
import { ConnectDeviceFirstStepComponent } from "./components/connect-device-first-step/connect-device-first-step.component";
import { ConnectDeviceStepperComponent } from "./components/connect-device-stepper/connect-device-stepper.component";
import { CreateProfileFirstStepComponent } from "./components/create-profile-first-step/create-profile-first-step.component";
import { CreateProfileSecondStepComponent } from "./components/create-profile-second-step/create-profile-second-step.component";
import { CreateProfileThirdStepComponent } from "./components/create-profile-third-step/create-profile-third-step.component";
import { DesktopLogoWrapperComponent } from "./components/desktop-logo-wrapper/desktop-logo-wrapper.component";
import { MessageComponent } from "./components/message/message.component";
import { PeerProfileComponent } from "./components/peer-profile/peer-profile.component";
import { SessionItemComponent } from "./components/session-item/session-item.component";
import { SessionsListComponent } from "./components/sessions-list/sessions-list.component";
import { ShareNewSessionComponent } from "./components/share-new-session/share-new-session.component";
import { StoreSessionStepperComponent } from "./components/store-session-stepper/store-session-stepper.component";
import { UnlockMasterSessionPassphraseComponent } from "./components/unlock-master-session-passphrase/unlock-master-session-passphrase.component";
import { UserProfileComponent } from "./components/user-profile/user-profile.component";
import { SecureChatModule } from "./modules/secure-chat";
import { UxModule } from "./modules/ux/ux.module";
import { ConnectDeviceComponent } from "./pages/connect-device/connect-device.component";
import { ConversationsComponent } from "./pages/conversations/conversations.component";
import { CreateProfileComponent } from "./pages/create-profile/create-profile.component";
import { ImportMasterSessionComponent } from "./pages/import-master-session/import-master-session.component";
import { JoinSessionComponent } from "./pages/join-session/join-session.component";
import { MainComponent } from "./pages/main/main.component";
import { NoSessionComponent } from "./pages/no-session/no-session.component";
import { SaveMasterSessionComponent } from "./pages/save-master-session/save-master-session.component";
import { SessionComponent } from "./pages/session/session.component";
import { UnlockMasterSessionComponent } from "./pages/unlock-master-session/unlock-master-session.component";
import { MessageInterpreterService } from "./services/message-interpreter.service";
import { ProfileMessageService } from "./services/profile-message.service";
import { TextMessageService } from "./services/text-message.service";
import { WasReadMessageService } from "./services/was-read-message.service";
import { WritingMessageService } from "./services/writing-message.service";
import { ReactionMessageService } from "./services/reaction-message.service";
import { LongPressDirective } from "./modules/ux/directives/long-press/long-press.directive";
import { EditUserProfileComponent } from "./components/edit-user-profile/edit-user-profile.component";
import { DialogComponent } from "./components/dialog/dialog.component";
import { ConnectDeviceLoadStepComponent } from './components/connect-device-load-step/connect-device-load-step.component';
import { NotifyMessageService } from "./services/notify-message.service";
import { FormComponent } from "./components/form/form.component";

export const routes: Routes = [
  {
    path: "",
    component: AppComponent,
    children: [
      { path: "", redirectTo: "home", pathMatch: 'full' },
      { path: "home", component: MainComponent },
      { path: "device/:syncId", component: ConnectDeviceComponent },
      { path: "import", component: ImportMasterSessionComponent },
      {
        component: CreateProfileComponent,
        data: { label: "Start your first converation", icon: "forward" },
        path: "start-session",
      },
      {
        component: UserProfileComponent,
        data: { label: "Profile" },
        path: "user-profile",
      },
      {
        component: EditUserProfileComponent,
        data: { label: "EditProfile" },
        path: "edit-user-profile",
      },
      {
        component: ConversationsComponent,
        data: { label: "Conversations", icon: "forward" },
        path: "conversations",
        children: [
          {
            path: "session/new",
            component: ShareNewSessionComponent,
          },
          {
            path: "session/new/:id",
            component: ShareNewSessionComponent,
          },
          {
            path: "session/join/:id",
            component: JoinSessionComponent,
          },
          {
            path: "session/active/:id",
            component: SessionComponent,
          },
        ],
      },
      {
        component: UnlockMasterSessionComponent,
        data: { label: "Unlock master session", icon: "forward" },
        path: "unlock-master-session"
      },
    ],
  },
];

const ModuleRouting: ModuleWithProviders<RouterModule> = RouterModule.forRoot(routes, {
  enableTracing: false,
});

@NgModule({
  declarations: [
    AppComponent,
    SessionsListComponent,
    SessionItemComponent,
    MainComponent,
    ConversationsComponent,
    EditUserProfileComponent,
    MessageComponent,
    UnlockMasterSessionComponent,
    UnlockMasterSessionPassphraseComponent,
    SaveMasterSessionComponent,
    ChatComponent,
    NoSessionComponent,
    SessionComponent,
    CreateProfileComponent,
    CreateProfileFirstStepComponent,
    CreateProfileSecondStepComponent,
    CreateProfileThirdStepComponent,
    ShareNewSessionComponent,
    JoinSessionComponent,
    DesktopLogoWrapperComponent,
    UserProfileComponent,
    StoreSessionStepperComponent,
    PeerProfileComponent,
    ImportMasterSessionComponent,
    ConnectDeviceComponent,
    ConnectDeviceStepperComponent,
    ConnectDeviceFirstStepComponent,
    ConnectDeviceDeviceApproveStepComponent,
    DialogComponent,
    ConnectDeviceLoadStepComponent,
    FormComponent
  ],
  imports: [
    MatDialogModule,
    MatSnackBarModule,
    LayoutModule,
    TextFieldModule,
    BrowserModule,
    QRCodeModule,
    FormsModule,
    ReactiveFormsModule,
    ClipboardModule,
    MatCheckboxModule,
    MatInputModule,
    MatSidenavModule,
    MatIconModule,
    MatButtonModule,
    NgxFileDropModule,
    MatExpansionModule,
    UxModule.forRoot({
      appName: "Protocol",
    }),
    FlexLayoutModule,
    ModuleRouting,
    AngularFireModule.initializeApp(environment.firebase, "ark.chat"),
    SecureChatModule.forRoot(environment),
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    BrowserAnimationsModule,
    ZXingScannerModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
  ],
  providers: [
    ProfileMessageService,
    MessageInterpreterService,
    TextMessageService,
    WasReadMessageService,
    WritingMessageService,
    ReactionMessageService,
    LongPressDirective,
    NotifyMessageService
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule {
  constructor(
    private profile: ProfileMessageService,
    private textMessageService: TextMessageService,
    private wasReadMessageService: WasReadMessageService,
    private writingMessageService: WritingMessageService,
    private reactionMessageService: ReactionMessageService,
    private notifyService: NotifyMessageService
  ) {
    this.profile.init();
    this.textMessageService.init();
    this.wasReadMessageService.init();
    this.writingMessageService.init();
    this.reactionMessageService.init();
    this.notifyService.init();
  }
}
