import { enableProdMode } from "@angular/core";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";

import { AppModule } from "./app/app.module";
import { environment } from "./environments/environment";

if (environment.production) {
  enableProdMode();
}

// BrowserFS.configure({
//   fs: 'IndexedDB',
//   options: {}
// }, function (err) {
//   if (err) {
//     console.log(err);
//     return;
//   }
//   // Expose the fs module globally on the window object
//   window.fs = BrowserFS.BFSRequire('fs');

//   // Bootstrap the app
//   platformBrowserDynamic().bootstrapModule(AppModule)
//     .catch(err => console.error(err));
// });

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .then(() => {
    if (!("serviceWorker" in navigator)) {
      console.warn(
        "Too bad, Service worker is not supported in current browser"
      );
      return;
    }
  });
