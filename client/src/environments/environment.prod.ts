export const environment = {
  production: true,
  baseUrl: 'https://thecyberd3m0n.gitlab.io/protocol-im/',
  subpath: 'protocol-im',
  appName: 'protocol-stg',
  firebase: {
    apiKey: 'AIzaSyCyLwUdWWtGGRyeV_O_kIMKbk-SkKbsb44a',
    authDomain: 'ohmsoftware.firebaseapp.com',
    databaseURL: 'https://ohmsoftware.firebaseio.com',
    projectId: 'ohmsoftware',
    storageBucket: 'ohmsoftware.appspot.com',
    messagingSenderId: '85533558057',
    appId: '1:85533558057:web:31976b83d0e3571c912338'
  },
};
