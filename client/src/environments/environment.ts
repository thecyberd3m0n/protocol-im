// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'http://10.0.0.2:4201',
  subpath: '',
  appName: 'protocol-local',
  firebase: {
    apiKey: 'AIzaSyCyLwUdWWtGGRyeV_O_kIMKbk-SkKbsb44',
    authDomain: 'ohmsoftware.firebaseapp.com',
    databaseURL: 'https://ohmsoftware.firebaseio.com',
    projectId: 'ohmsoftware',
    storageBucket: 'ohmsoftware.appspot.com',
    messagingSenderId: '85533558057',
    appId: '1:85533558057:web:31976b83d0e3571c912338'
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
