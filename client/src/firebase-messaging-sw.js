importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-app.js");
importScripts(
  "https://www.gstatic.com/firebasejs/8.10.0/firebase-messaging.js"
);
firebase.initializeApp({
  apiKey: "AIzaSyCyLwUdWWtGGRyeV_O_kIMKbk-SkKbsb44",
  authDomain: "ohmsoftware.firebaseapp.com",
  databaseURL: "https://ohmsoftware.firebaseio.com",
  projectId: "ohmsoftware",
  storageBucket: "ohmsoftware.appspot.com",
  messagingSenderId: "85533558057",
  appId: "1:85533558057:web:31976b83d0e3571c912338",
});
const messaging = firebase.messaging();
const sessionMap = {};
// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
messaging.setBackgroundMessageHandler(function (payload) {
  console.log(
    "[firebase-messaging-sw.js] Received background message ",
    payload
  );
  // Customize notification here
  const notificationTitle = "Protocol IM";
  const notificationOptions = {
    body: "You received new message",
  };

  return self.registration.showNotification(
    notificationTitle,
    notificationOptions
  );
});

// register handler for channel registration
// register session by posting { op: 'regroom', sessionId, peerKey }
self.addEventListener("message", (ev) => {
  console.log('received Service Worker postmessage', ev);
  if (op === "regroom") {
    sessionMap[ev.sessionId] = {
      [inboundKey]: ev.peerKey,
    };
  }
});
