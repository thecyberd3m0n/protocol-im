### protocall - e2e and locally encrypted, web-based messenger.

- No phone numbers, no emails, only anonymous sessions. Way of sign-up is more convenient and reminds crypto wallets.
- Guest sessions - invite someone to session - he'll be able to talk with you just after clicking the link
- E2E axolotl (Proteus) transport encryption. Cloud service admins cannot read your messages.
- Local database encryption (TweetNaCl). Physical access don't make things much easier
- metadata protection - they're sent as just another encrypted messages.

## WORK IN PROGRESS

# What works?

- sending, receiving messages
- guest sessions
- locking/unlocking session
- emojis
- more metadata - message was read, message was deleted
- sync between devices

# What's left

- devices management
- Push notificatons
- calls
- PWAization
- files transfer
- monetization/premium accounts
- groups
- just gradually making it more feature-full and powerful
- better ux
- Web3 integration, executing smart-contracts between peers, monetization

# Technologies used

- Axolotl/Proteus - https://github.com/wireapp/proteus
- IndexedDb/Dexie/Dexie encrypted
- Google Firebase (Realtime Database and Storage)
- Angular
- Angular Material
- ngrx/store
- Matrix.org/Olm - https://gitlab.matrix.org/matrix-org/olm
- libsodium-sumo - https://www.npmjs.com/package/libsodium-wrappers-sumo

Donate:

BTC: bc1q53gev40vcdth34d75ll68js0sqala7a6ha37jf

ETH: 0x27A28ba6Ca0f88163C9641682A64516F66c767Cd

XMR: 46WJUEBjEah13xi9UPuziE4oz1d1TeSYchdNmV13qfbp74X6bBsER4oGDboCMZysKaEEvxTwwuMULBdWN9Jtqycm8DpUtu7


More donations = more motivation = more features

